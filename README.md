# Practice Suite

Introducing the Swiss army knife of musicians - Practice Suite! This all-in-one app is perfect for musicians of all levels, from beginners to professionals.

Practice Suite is the ultimate music practice tool for musicians of all levels.   
With its comprehensive features, you can stay on beat, in tune, and track your progress in one convenient app.

The Metronome feature allows you to set your desired tempo and time signature, ensuring that you stay on beat and in time while practicing.   
The Tuner feature uses your device's microphone to accurately tune your instrument, helping you to play in perfect pitch.  
The Recorder feature allows you to record your practice sessions and play them back, so you can listen to your progress and identify areas for improvement.   
With the built-in Log feature, you can keep track of your practice sessions, set goals, and monitor your progress over time.   
The app also provides detailed statistics, so you can see how much time you've spent practicing and how you've improved over time.

The app is designed to be user-friendly and easy to navigate, so you can focus on your practice and not on figuring out the app.   
It's available for both iOS and Android devices.

Whether you're just starting to learn an instrument or you're a seasoned professional looking to fine-tune your skills,  
Practice Suite is the perfect tool to help you achieve your goals. Download now and see your progress soar!

<br/>

[<img src="https://img.shields.io/badge/F_Droid-1976D2?style=for-the-badge&logo=f-droid&logoColor=white"  
alt="Get it on F-Droid"  
height="40">](https://f-droid.org/packages/com.berkersen.practicesuite) &nbsp;&nbsp; [<img src="https://img.shields.io/badge/Google_Play-414141?style=for-the-badge&logo=google-play&logoColor=white"    
alt="Get it on Google Play"    
height="40">](https://play.google.com/store/apps/details?id=com.berkersen.practicesuite)  &nbsp;&nbsp; [<img src="https://img.shields.io/badge/App_Store-0D96F6?style=for-the-badge&logo=app-store&logoColor=white"    
alt="Get it on App Store"    
height="40">](https://apps.apple.com/us/app/practice-suite/id1668677376?itsct=apps_box_link&itscg=30200)

<br/>

![screenshots](screenshots/Screenshots.png)

