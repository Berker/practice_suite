# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.0] - 2024-08-16
### Added
- Keep screen awake for metronome, tuner and recorder
- Persist metronome values on restart
- Themable adaptive launcher icons
### Changed
- Background color of on/off buttons
- Updated packages
- Updated Flutter to 3.24

## [2.0.0] - 2024-08-06
### Added
- Play progress in recording tiles
### Changed
- Updated to Material 3 design
- Updated packages
- Updated Flutter to 3.22.3
### Fixed
- Lots of bugs
- Translations

## [1.2.1] - 2023-05-24
### Fixed
- Fix tempo digits not changing with 'tap to set tempo' button when metronome is on

## [1.2.0] - 2023-05-23
### Added
- New button on metronome which lets user tap to set the tempo
### Changed
- Updated packages
- Updated Flutter to 3.10.1
### Fixed
- Breaking changes caused by package updates
- Translations

## [1.1.1] -2023-05-02
### Fixed
- Fixed play button not showing up right after recording is stopped.
- Fixed metronome stability on triplets.
- Fixed range errors.
### Changed
- Removed tempo restriction on triplets.
### Updated
- Flutter updated to 3.7.12

## [1.1.0] - 2023-04-08
### Changed
- Increased pitch range
- Flutter to 3.7.10
### Added
- Metronome speed and pitch value is now saved across restarts

## [1.0.9] - 2023-02-19
### Changed
- Recording codec to wav for better sound quality and compatibility

## [1.0.8] - 2ß23-02-17
### Added
- Source code, issue tracker and social media links to settings
### Changes
- Updated packages

## [1.0.7] - 2023-02-16
### Fixed
- Translations
- Accent sound initialization bug
- Pitch change not being picked up
### Added
- More tuning steps thus making tuner more sensitive

## [1.0.6] - 2023-02-16
- Initial release