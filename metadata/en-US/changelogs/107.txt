## [1.0.7]
- Translation fixes
- Fixed accent sound initialization bug
- Fixed pitch change not being picked up
- Added more tuning steps thus making tuner more sensitive