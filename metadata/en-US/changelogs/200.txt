Added
- Play progress in recording tiles
Changed
- Updated to Material 3 design
- Updated packages
- Updated Flutter to 3.22.3
Fixed
- Lots of bugs
- Translations