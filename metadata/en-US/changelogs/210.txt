Added
- Keep screen awake for metronome, tuner and recorder
- Persist metronome values on restart
- Themable adaptive launcher icons
Changed
- Background color of on/off buttons
- Updated packages
- Updated Flutter to 3.24