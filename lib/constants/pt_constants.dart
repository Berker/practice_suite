/// KEYS
const String themeModeKey = 'themeMode';
const String languageKey = 'language';
const String firstLaunchKey = 'firstLaunch';
const String pitchKey = 'pitch';
const String metronomeKey = 'metronome';
const String notesKey = 'notes';
const String normalSoundKey = 'normalSound';
const String accentSoundKey = 'accentSound';

/// VALUES
const String darkModeValue = 'darkMode';
const String lightModeValue = 'lightMode';
const String systemModeValue = 'systemMode';
const String englishValue = 'en_US';
const String germanValue = 'de_DE';
const String turkishValue = 'tr_TR';
const String systemLanguageValue = 'systemLanguage';