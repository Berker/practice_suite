class PracticeLogEntry {
  String id;
  String title;
  String duration;
  String metronome;
  String description;
  List<String> tags;
  String date;

  PracticeLogEntry({
    required this.id,
    required this.title,
    required this.duration,
    this.metronome = "",
    required this.date,
    this.description = "",
    required this.tags,
  });
}
