class PracticeDuration {
  int minute;
  int hour;
  int day;

  PracticeDuration({
    required this.minute,
    required this.hour,
    required this.day,
  });

  factory PracticeDuration.fromMinutes(int totalMinutes) {
    var duration = Duration(minutes: totalMinutes);

    var minute = duration.inMinutes.remainder(60);
    var hour = duration.inHours.remainder(24);
    var day = duration.inDays;

    return PracticeDuration(minute: minute, hour: hour, day: day);
  }

  factory PracticeDuration.fromMinutesReturnInHoursAndMinutes(int totalMinutes) {
    var duration = Duration(minutes: totalMinutes);

    var minute = duration.inMinutes.remainder(60);
    var hour = duration.inHours;

    return PracticeDuration(minute: minute, hour: hour, day: 0);
  }
}
