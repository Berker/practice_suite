import 'package:flutter/material.dart';

class StatisticCharts {
  Widget thisWeek;
  Widget lastWeek;
  Widget thisYear;
  Widget thisMonth;

  StatisticCharts(
      {required this.thisWeek,
      required this.lastWeek,
      required this.thisYear,
      required this.thisMonth});
}
