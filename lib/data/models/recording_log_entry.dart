class RecordingLogEntry {
  String id;
  String date;
  String time;
  String title;
  String duration;
  String fileName;

  RecordingLogEntry({
    required this.id,
    required this.date,
    required this.time,
    this.title = "",
    required this.duration,
    required this.fileName,
  });
}
