class TempoMarking {
  String marking;
  String translation;
  String tempoRange;
  int tempo;

  TempoMarking({
    required this.marking,
    required this.translation,
    required this.tempoRange,
    required this.tempo,
  });
}
