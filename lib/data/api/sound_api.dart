import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:flutter_sound_platform_interface/flutter_sound_recorder_platform_interface.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:practice_suite/logic/blocs/recording_log/recording_log_bloc.dart';

import '../../generated/l10n.dart';
import '../../services/locator_service.dart';

String _pathToSaveAudio = "/sdcard/Practice_Suite/Recordings/";

class SoundApi {
  FlutterSoundRecorder? _audioRecorder;
  FlutterSoundPlayer? _audioPlayer;
  StreamSubscription? _recorderSubscription;
  final Codec _codec = Codec.pcm16WAV;
  bool _isSoundApiInitialized = false;
  bool _isAudioPlayerInitialized = false;
  ValueNotifier<String> getRecordingProgress = ValueNotifier("00:00:00");

  bool get isRecording => _audioRecorder!.isRecording;

  bool get isPlaying => _audioPlayer!.isPlaying;

  String get pathToSaveAudio => _pathToSaveAudio;

  set pathToSaveAudio(String path) {
    _pathToSaveAudio = path;
  }

  Future init() async {
    _audioRecorder = FlutterSoundRecorder();
    _audioPlayer = FlutterSoundPlayer();

    print(_codec.name);
    print(_codec.toString());

    final microphonePermissionStatus = await Permission.microphone.request();
    final storagePermissionStatus = await Permission.storage.request();

    if (microphonePermissionStatus != PermissionStatus.granted) {
      getIt<RecordingLogBloc>().add(RecordingLogFailure(errorMessage: getIt<S>().recording_log_check_permissions));
      //throw RecordingPermissionException("Microphone permission is denied!");
    }
    if (storagePermissionStatus != PermissionStatus.granted) {
      getIt<RecordingLogBloc>().add(RecordingLogFailure(errorMessage: getIt<S>().recording_log_check_permissions));
      //throw RecordingPermissionException("Microphone permission is denied!");
    }

    await _audioRecorder!.openRecorder();
    await _audioPlayer!.openPlayer();

    _isSoundApiInitialized = true;
  }

  Future initPlayerOnly() async {
    _audioPlayer = FlutterSoundPlayer();
    await _audioPlayer!.openPlayer();
    _isAudioPlayerInitialized = true;
  }

  void disposePlayerOnly() {
    if (!_isAudioPlayerInitialized) return;
    _audioPlayer!.closePlayer();
    _audioPlayer = null;
    _isAudioPlayerInitialized = false;
  }

  void dispose() {
    if (!_isSoundApiInitialized) return;
    _audioRecorder!.closeRecorder();
    _audioRecorder = null;
    _audioPlayer!.closePlayer();
    _audioPlayer = null;
    _isSoundApiInitialized = false;
  }

  Future record(String fileName) async {
    if (!_isSoundApiInitialized) return;

    if (!File("$pathToSaveAudio$fileName.wav").existsSync()) {
      File("$pathToSaveAudio$fileName.wav").create(recursive: true);
    }

    await _audioRecorder!.startRecorder(
      toFile: "$pathToSaveAudio$fileName.wav",
      codec: _codec,
      audioSource: AudioSource.microphone,
    );
    setRecorderSubscription();
  }

  Future stopRecording() async {
    if (!_isSoundApiInitialized) return;

    await _audioRecorder!.stopRecorder();
    if (_recorderSubscription != null) {
      _recorderSubscription!.cancel();
    }
  }

  Future play(String fileName, TWhenFinished setState) async {
    if (_isSoundApiInitialized || _isAudioPlayerInitialized) {
      await _audioPlayer!.startPlayer(
        fromURI: "$pathToSaveAudio$fileName.wav",
        codec: _codec, whenFinished: setState,
      );
    } else {
      return;
    }
  }

  Future stopPlaying() async {
    if (_isSoundApiInitialized || _isAudioPlayerInitialized) {
      await _audioPlayer!.stopPlayer();
    } else {
      return;
    }
  }

  Future setRecorderSubscription() async {
    await _audioRecorder!
        .setSubscriptionDuration(const Duration(milliseconds: 10));
    _recorderSubscription = _audioRecorder!.onProgress!.listen((event) {
      var date = DateTime.fromMillisecondsSinceEpoch(
          event.duration.inMilliseconds,
          isUtc: true);

      var txt = DateFormat("mm:ss:SS", "en_GB").format(date);
      getRecordingProgress.value = txt.substring(0, 8);
    });
  }
/*
  Future toggleRecording(String fileName) async {
    if (_audioRecorder!.isStopped) {
      await _record(fileName);
    } else {
      await _stopRecording();
    }
  }

  Future togglePlaying(String fileName) async {
    if (_audioPlayer!.isStopped) {
      await _play(fileName);
    } else {
      await _stopPlaying();
    }
  } */
}
