import 'dart:io';
import 'package:flutter/material.dart';
import 'package:practice_suite/constants/enums.dart';
import 'package:practice_suite/constants/pt_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../services/metronome_service.dart';

class SharedPrefsApi {
  static late SharedPreferences sharedPreferences;

  static init() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  static Locale getLanguage() {
    List<Locale> supportedLanguages = [
      PTLanguage.deutsch.locale,
      PTLanguage.english.locale,
      PTLanguage.turkce.locale,
    ];
    final String systemLocaleName = Platform.localeName;
    Locale systemLocale = PTLanguage.english.locale;
    String appLanguage =
        sharedPreferences.getString(languageKey) ?? systemLanguageValue;
    switch (appLanguage) {
      case englishValue:
        systemLocale = PTLanguage.english.locale;
        break;
      case germanValue:
        systemLocale = PTLanguage.deutsch.locale;
        break;
      case turkishValue:
        systemLocale = PTLanguage.turkce.locale;
        break;
      case systemLanguageValue:
        String languageName = systemLocaleName.substring(0, 2);
        String? countryCode = systemLocaleName.length > 2 ? systemLocaleName.substring(3, 5) : null;
        systemLocale = Locale(languageName, countryCode);
        break;
    }
    Locale? selectedLocale;
    for (var locale in supportedLanguages) { if(locale.languageCode == systemLocale.languageCode) {
      selectedLocale = locale;
    } }
    return selectedLocale ?? PTLanguage.english.locale;
  }

  static bool getFirstLaunchInfo() {
    return sharedPreferences.getBool(firstLaunchKey) ?? true;
  }

  static void setFirstLaunch() {
    sharedPreferences.setBool(firstLaunchKey, false);
  }

  static Future<bool> setLanguage(String language) async {
    String languageValue = englishValue;

    switch (language) {
      case englishValue:
        languageValue = englishValue;
        break;
      case germanValue:
        languageValue = germanValue;
        break;
      case systemLanguageValue:
        languageValue = systemLanguageValue;
    }
    return await sharedPreferences.setString(languageKey, languageValue);
  }

  static ThemeMode getThemeMode() {
    String themeMode =
        sharedPreferences.getString(themeModeKey) ?? systemModeValue;
    switch (themeMode) {
      case darkModeValue:
        return ThemeMode.dark;
      case lightModeValue:
        return ThemeMode.light;
      default:
        return ThemeMode.system;
    }
  }

  static Future<bool> setThemeMode(ThemeMode themeMode) async {
    String themeModeValue = systemModeValue;

    switch (themeMode) {
      case ThemeMode.dark:
        themeModeValue = darkModeValue;
        break;
      case ThemeMode.light:
        themeModeValue = lightModeValue;
        break;
      default:
        themeModeValue = systemModeValue;
    }

    return await sharedPreferences.setString(themeModeKey, themeModeValue);
  }

  static double getPitchValue() {
    return sharedPreferences.getDouble(pitchKey) ?? 440.0;
  }

  static void setPitchValue(double pitchValue) {
    sharedPreferences.setDouble(pitchKey, pitchValue);
  }

  static int getMetronomeSpeedValue() {
    return sharedPreferences.getInt(metronomeKey) ?? 100;
  }

  static void setMetronomeSpeedValue(int metronomeSpeedValue) {
    sharedPreferences.setInt(metronomeKey, metronomeSpeedValue);
  }

  static Notes getNotesValue() {
    Notes notesValue = Notes.values.byName(sharedPreferences.getString(notesKey) ?? 'fourth');
    return notesValue;
  }

  static void setNotesValue(Notes notesValue) {
    sharedPreferences.setString(notesKey, notesValue.name);
  }

  static String getAccentSoundValue() {
    return sharedPreferences.getString(accentSoundKey) ?? 'sound-1.wav';
  }

  static void setAccentSoundValue(String accentSound) {
    sharedPreferences.setString(accentSoundKey, accentSound);
  }

  static String getNormalSoundValue() {
    return sharedPreferences.getString(normalSoundKey) ?? 'sound-1.wav';
  }

  static void setNormalSoundValue(String normalSound) {
    sharedPreferences.setString(normalSoundKey, normalSound);
  }
}
