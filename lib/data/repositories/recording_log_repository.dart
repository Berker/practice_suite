import 'dart:io';

import 'package:practice_suite/data/api/database_api.dart';
import 'package:practice_suite/data/models/recording_log_entry.dart';

class RecordingLogRepository {
  Future<List<RecordingLogEntry>> fetchAndSetLogEntries() async {
    final entryData = await DatabaseApi.getData("recording_log_entries");
    List<RecordingLogEntry> recordingLog = entryData
        .map((entry) => RecordingLogEntry(
              id: entry["id"],
              date: entry["date"],
              time: entry["time"],
              title: entry["title"],
              duration: entry["duration"],
              fileName: entry["fileName"],
            ))
        .toList();
    return recordingLog;
  }

  RecordingLogEntry getLogEntryById(
      String id, List<RecordingLogEntry> recordingLog) {
    return recordingLog.firstWhere((element) => element.id == id);
  }

  void addLogEntry(RecordingLogEntry newEntry) {
    DatabaseApi.insert("recording_log_entries", {
      "id": newEntry.id,
      "date": newEntry.date,
      "time": newEntry.time,
      "title": newEntry.title,
      "duration": newEntry.duration,
      "fileName": newEntry.fileName,
    });
  }

  void removeLogEntry(String entryId, String filePath) {
    deleteRecordingFile(filePath);
    DatabaseApi.delete(entryId, "recording_log_entries");
  }

  void deleteRecordingFile(String filePath) {
    const pathToDeleteAudio = "/sdcard/Practice_Suite/Recordings/";
    var fileToDelete = File("$pathToDeleteAudio$filePath.mp4");
    if (fileToDelete.existsSync()) {
      fileToDelete.deleteSync();
    }
  }

  bool updateLogEntry(RecordingLogEntry editedEntry) {
    try {
      DatabaseApi.update(editedEntry.id, "recording_log_entries", {
        "id": editedEntry.id,
        "date": editedEntry.date,
        "time": editedEntry.time,
        "title": editedEntry.title,
        "duration": editedEntry.duration,
        "fileName": editedEntry.fileName,
      });
      return true;
    } catch (error) {
      return false;
    }
  }
}
