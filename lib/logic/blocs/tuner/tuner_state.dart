part of 'tuner_bloc.dart';

abstract class TunerState extends Equatable {
  const TunerState();

  @override
  List<Object> get props => [];
}

class UpdateNoteTunerState extends TunerState {
  final String note;
  final TuningStatus pitchStatus;

  const UpdateNoteTunerState({required this.note, required this.pitchStatus});

  @override
  List<Object> get props => [note, pitchStatus];

  @override
  String toString() {
    return 'UpdateNoteTunerState{note: $note, pitchStatus: $pitchStatus}';
  }
}

class LoadingStateTunerState extends TunerState {}

class ChangePitchTunerState extends TunerState {
  final double pitch;

  const ChangePitchTunerState({required this.pitch});
}
