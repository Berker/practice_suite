import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/data/api/shared_prefs_api.dart';
import 'package:practice_suite/services/tuner/tuner_service.dart';

import '../../../services/locator_service.dart';
import '../../../services/tuner/tuning_status.dart';

part 'tuner_event.dart';

part 'tuner_state.dart';

class TunerBloc extends Bloc<TunerEvent, TunerState> {
  double pitch = SharedPrefsApi.getPitchValue();

  TunerBloc()
      : super(const UpdateNoteTunerState(
            note: "-", pitchStatus: TuningStatus.undefined)) {
    on<InitTunerEvent>((event, emit) async {
      await getIt<TunerService>().init();
    });

    on<UpdateNoteTunerEvent>((event, emit) {

        emit(LoadingStateTunerState());
        emit(UpdateNoteTunerState(
            note: event.note, pitchStatus: event.pitchStatus));

    });
    on<ToggleTunerEvent>((event, emit) {
      getIt<TunerService>().toggleTuner();
    });
    on<DisposeTunerEvent>((event, emit) {
      emit(LoadingStateTunerState());
      getIt<TunerService>().dispose();
    });
    on<ChangePitchTunerEvent>((event, emit) {
      emit(LoadingStateTunerState());
      pitch = event.pitch;
      getIt<TunerService>().changePitch(pitch);
      SharedPrefsApi.setPitchValue(pitch);
      emit(ChangePitchTunerState(pitch: pitch));
    });
  }
}
