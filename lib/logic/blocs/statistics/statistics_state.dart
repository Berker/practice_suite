part of 'statistics_bloc.dart';

abstract class StatisticsState extends Equatable {
  const StatisticsState();
}

class StatisticsInitial extends StatisticsState {
  @override
  List<Object> get props => [];
}

class FetchAndBuildSuccessState extends StatisticsState {
  final List<Widget> statistics;
  final StatisticCharts charts;

  const FetchAndBuildSuccessState({required this.statistics, required this.charts});

  @override
  List<Object> get props => [statistics, charts];
}

class FetchAndBuildFailedState extends StatisticsState {
  @override
  List<Object> get props => [];
}

class LoadingStatisticsState extends StatisticsState {
  @override
  List<Object> get props => [];
}
