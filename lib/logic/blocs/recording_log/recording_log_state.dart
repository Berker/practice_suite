part of 'recording_log_bloc.dart';

abstract class RecordingLogState extends Equatable {
  const RecordingLogState();

  @override
  List<Object?> get props => [];
}

class RecordingLogEntries extends RecordingLogState {
  final List<RecordingLogEntry> recordingLog;
  const RecordingLogEntries({required this.recordingLog});

  @override
  List<Object> get props => [recordingLog];
  @override
  String toString() {
    return "RecordingLogEntries { Log: $recordingLog }";
  }
}

class LoadingRecordingLogState extends RecordingLogState {}

class AddedEntry extends RecordingLogState {}
class RemovedEntry extends RecordingLogState {}
class UpdatedEntry extends RecordingLogState {}

class RecordingLogFailureState extends RecordingLogState {
  final String errorMessage;

  const RecordingLogFailureState({required this.errorMessage});
}

class RecordingAudioState extends RecordingLogState {}
class RecorderIdleState extends RecordingLogState {}
class PlayingAudioState extends RecordingLogState {}
class PlayerIdleState extends RecordingLogState {}
class RecordingAvailableState extends RecordingLogState {}