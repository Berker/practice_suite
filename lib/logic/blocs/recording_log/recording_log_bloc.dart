import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/data/models/recording_log_entry.dart';
import 'package:practice_suite/data/repositories/recording_log_repository.dart';

part 'recording_log_event.dart';
part 'recording_log_state.dart';

class RecordingLogBloc extends Bloc<RecordingLogEvent, RecordingLogState> {
  List<RecordingLogEntry> _recordingLogItems = [];

  RecordingLogBloc() : super(const RecordingLogEntries(recordingLog: [])) {
    on<FetchRecordingLog>((event, emit) async {
      emit(LoadingRecordingLogState());
      RecordingLogRepository repository = RecordingLogRepository();
      _recordingLogItems = await repository.fetchAndSetLogEntries();
      emit(RecordingLogEntries(recordingLog: _recordingLogItems));
    });

    on<AddRecordingLogEntry>((event, emit) {
      emit(LoadingRecordingLogState());
      _recordingLogItems.add(event.newRecordingLogEntry);
      RecordingLogRepository repository = RecordingLogRepository();
      repository.addLogEntry(event.newRecordingLogEntry);
      emit(AddedEntry());
      emit(RecordingLogEntries(recordingLog: _recordingLogItems));
    });

    on<RemoveRecordingLogEntry>((event, emit) {
      emit(LoadingRecordingLogState());
      _recordingLogItems.removeWhere((entry) => entry.id == event.recordingLogEntryID);
      RecordingLogRepository repository = RecordingLogRepository();
      repository.removeLogEntry(event.recordingLogEntryID, event.filePath);
      emit(RemovedEntry());
      emit(RecordingLogEntries(recordingLog: _recordingLogItems));
    });

    on<UpdateRecordingLogEntry>((event, emit) {
      emit(LoadingRecordingLogState());
      _recordingLogItems.removeWhere(
              (entry) => entry.id == event.updatedRecordingLogEntry.id);
      _recordingLogItems.add(event.updatedRecordingLogEntry);
      RecordingLogRepository repository = RecordingLogRepository();
      repository.updateLogEntry(event.updatedRecordingLogEntry);
      emit(UpdatedEntry());
      emit(RecordingLogEntries(recordingLog: _recordingLogItems));
    });
    on<RecordingLogFailure>((event, emit) {
      emit(LoadingRecordingLogState());
      emit(RecordingLogFailureState(errorMessage: event.errorMessage));
    });
  }
}
