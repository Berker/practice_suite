part of 'app_theme_bloc.dart';


abstract class AppThemeState extends Equatable {
  const AppThemeState();
}

class ChangeThemeState extends AppThemeState {
  final ThemeMode themeMode;

  const ChangeThemeState({required this.themeMode});

  @override
  List<Object?> get props => [];
}

class ThemeLoadingState extends AppThemeState {
  @override
  List<Object?> get props => [];
}