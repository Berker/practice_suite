import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/data/api/shared_prefs_api.dart';
import 'package:practice_suite/presentation/styles/app_theme.dart';

part 'app_theme_event.dart';

part 'app_theme_state.dart';

class AppThemeBloc extends Bloc<AppThemeEvent, AppThemeState> {
  AppThemeBloc() : super(ChangeThemeState(themeMode: AppTheme.initialTheme)) {
    ThemeMode appTheme = AppTheme.initialTheme;

    on<ChangeThemeEvent>((event, emit) {
      emit(ThemeLoadingState());
      appTheme = event.themeMode;
      SharedPrefsApi.setThemeMode(appTheme);
      emit(ChangeThemeState(themeMode: appTheme));
    });
  }
}
