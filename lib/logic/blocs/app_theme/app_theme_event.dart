part of 'app_theme_bloc.dart';

abstract class AppThemeEvent extends Equatable {
  const AppThemeEvent();
}

class ChangeThemeEvent extends AppThemeEvent {
  final ThemeMode themeMode;

  const ChangeThemeEvent({required this.themeMode});

  @override
  List<Object?> get props => [];
}