import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/data/models/practice_log_entry.dart';
import 'package:practice_suite/data/repositories/practice_log_repository.dart';

part 'practice_log_event.dart';

part 'practice_log_state.dart';

class PanelItem {
  PracticeLogEntry logEntry;
  String headerValue;
  bool isExpanded;

  PanelItem({
    required this.logEntry,
    required this.headerValue,
    this.isExpanded = false,
  });
}

class PracticeLogBloc extends Bloc<PracticeLogEvent, PracticeLogState> {
  List<PanelItem> practiceLogItems = [];
  List<PanelItem> filteredLogItems = [];
  Map<String, bool> availableTags = {};
  bool showAllEntries = false;
  PracticeLogRepository repository = PracticeLogRepository();
  List<PracticeLogEntry> logEntries = [];

  DateTime filterEndDate = DateTime.now();
  DateTime filterStartDate = DateTime.now().subtract(const Duration(days: 30));

  void _filterLogItems() {
    filteredLogItems = [];
    DateTime filterEndDateOnly = DateTime(filterEndDate.year, filterEndDate.month, filterEndDate.day);
    DateTime filterStartDateOnly = DateTime(filterStartDate.year, filterStartDate.month, filterStartDate.day);

    for (PanelItem entry in practiceLogItems) {
      DateTime entryDate = DateTime.parse(entry.logEntry.date);
      DateTime entryDateOnly = DateTime(entryDate.year, entryDate.month, entryDate.day);

      if (((entryDateOnly.isBefore(filterEndDateOnly) || entryDateOnly == filterEndDateOnly) &&
          (entryDateOnly.isAfter(filterStartDateOnly) || entryDateOnly == filterStartDateOnly))) {
        if (isAnyTagSelected) {
          for (var tag in entry.logEntry.tags) {
            if (availableTags.containsKey(tag) && availableTags[tag] == true) {
              if (!filteredLogItems.contains(entry)) {
                filteredLogItems.add(entry);
              }
            }
          }
        }
      }
    }
  }

  bool get isAnyTagSelected => availableTags.containsValue(true);

  void setTags() {
    List<String> tags = repository.getAllTags(logEntries);
    for (var tag in tags) {
      availableTags.putIfAbsent(tag, () => true);
    }
  }

  void removeTagsWithoutEntry() {
    List<String> tags = repository.getAllTags(logEntries);
    availableTags.removeWhere((tag, isSelected) => !tags.contains(tag));
  }


  PracticeLogBloc() : super(PracticeLogEntries(practiceLog: const [])) {
    on<FetchPracticeLog>((event, emit) async {
      emit(LoadingPracticeLogState());
      logEntries =
          await repository.fetchAndSetLogEntries();
     setTags();
      practiceLogItems = logEntries
          .map((e) => PanelItem(logEntry: e, headerValue: e.title))
          .toList();
      _filterLogItems();
      emit(PracticeLogEntries(practiceLog: filteredLogItems));
    });

    on<AddPracticeLogEntry>((event, emit) {
      emit(LoadingPracticeLogState());
      practiceLogItems.add(PanelItem(
          logEntry: event.newPracticeLogEntry,
          headerValue: event.newPracticeLogEntry.title));
      repository.addLogEntry(event.newPracticeLogEntry);
      _filterLogItems();
      emit(AddedEntry());
      emit(PracticeLogEntries(practiceLog: filteredLogItems));
    });

    on<RemovePracticeLogEntry>((event, emit) {
      emit(LoadingPracticeLogState());
      practiceLogItems.removeWhere(
          (entry) => entry.logEntry.id == event.practiceLogEntryID);
      logEntries.removeWhere((entry) => entry.id == event.practiceLogEntryID);
      repository.removeLogEntry(event.practiceLogEntryID);
      removeTagsWithoutEntry();
      _filterLogItems();
      emit(RemovedEntry());
      emit(PracticeLogEntries(practiceLog: filteredLogItems));
    });

    on<UpdatePracticeLogEntry>((event, emit) {
      emit(LoadingPracticeLogState());
      practiceLogItems.removeWhere(
          (entry) => entry.logEntry.id == event.updatedPracticeLogEntry.id);
      practiceLogItems.add(PanelItem(
          logEntry: event.updatedPracticeLogEntry,
          headerValue: event.updatedPracticeLogEntry.title));
      logEntries.removeWhere((entry) => entry.id == event.updatedPracticeLogEntry.id);
      logEntries.add(event.updatedPracticeLogEntry);
      repository.updateLogEntry(event.updatedPracticeLogEntry);
      removeTagsWithoutEntry();
      _filterLogItems();
      emit(UpdatedEntry());
      emit(PracticeLogEntries(practiceLog: filteredLogItems));
    });

    on<FilterEntriesInDateRange>((event, emit) {
      emit(LoadingPracticeLogState());
      filterStartDate = event.startDate;
      filterEndDate = event.endDate;
      showAllEntries = event.showAllEntries;
      _filterLogItems();
      emit(PracticeLogEntries(practiceLog: filteredLogItems));
    });

    on<FilterEntriesWithTags>((event, emit) {
      emit(LoadingPracticeLogState());
      availableTags = event.selectedTags;
      _filterLogItems();
      emit(PracticeLogEntries(practiceLog: filteredLogItems));
    });
  }
}
