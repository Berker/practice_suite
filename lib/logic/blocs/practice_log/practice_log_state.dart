part of 'practice_log_bloc.dart';

abstract class PracticeLogState extends Equatable {

  @override
  List<Object> get props => [];
}

class PracticeLogEntries extends PracticeLogState {
  final List<PanelItem> practiceLog;
  PracticeLogEntries({required this.practiceLog});

  @override
  List<Object> get props => [practiceLog];
  @override
  String toString() {
    return "PracticeLogEntries { Log: $practiceLog }";
  }
}

class LoadingPracticeLogState extends PracticeLogState {}

// These are the states without any state (to show the snackbar in practice_log_entry_list)
class AddedEntry extends PracticeLogState {}
class RemovedEntry extends PracticeLogState {}
class UpdatedEntry extends PracticeLogState {}


