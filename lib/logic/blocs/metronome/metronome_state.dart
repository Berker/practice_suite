part of 'metronome_bloc.dart';

abstract class MetronomeState extends Equatable {

  @override
  List<Object> get props => [];
}

class MetronomeInitial extends MetronomeState {
}

class ToggleOnOffMetronomeState extends MetronomeState {
  final bool isMetronomeOn;
  ToggleOnOffMetronomeState({required this.isMetronomeOn});
  @override

  List<Object> get props => [isMetronomeOn];
}

class MetronomeTempoState extends MetronomeState {
  final int tempo;
  MetronomeTempoState({required this.tempo});
}

class MetronomeSoundState extends MetronomeState {
  final String normalSound;
  final String accentSound;
  MetronomeSoundState({required this.normalSound, required this.accentSound});
}

class AccentSoundState extends MetronomeState {
  final String accentSound;
  AccentSoundState({required this.accentSound});
}

class IncreaseTempoMetronomeState extends MetronomeState {}

class DecreaseTempoMetronomeState extends MetronomeState {}

class LoadingMetronomeState extends MetronomeState {}
