import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/data/api/shared_prefs_api.dart';
import 'package:practice_suite/services/locator_service.dart';
import 'package:practice_suite/services/metronome_service.dart';
import 'package:practice_suite/utils/debouncer.dart';

part 'metronome_event.dart';

part 'metronome_state.dart';

class MetronomeBloc extends Bloc<MetronomeEvent, MetronomeState> {
  final _metronome = getIt<MetronomeService>();
  int tempo = SharedPrefsApi.getMetronomeSpeedValue();
  Notes note = SharedPrefsApi.getNotesValue();
  bool tempoAdjustedWhenOn = false;
  String normalSound = SharedPrefsApi.getNormalSoundValue();
  String accentSound = SharedPrefsApi.getAccentSoundValue();
  final Debouncer _debouncer = Debouncer(milliseconds: 500);

  MetronomeBloc() : super(MetronomeInitial()) {
    int setTempoToNoteSelected(Notes selectedNote, int selectedTempo) {
      switch (selectedNote) {
        case Notes.fourth:
          return selectedTempo;
        case Notes.eighth:
          return selectedTempo * 2;
        case Notes.triplet:
          return selectedTempo * 3;
        default:
          return selectedTempo;
      }
    }

    on<InitMetronomeEvent>((event, emit) async {
      await _metronome.init();
      emit(MetronomeTempoState(tempo: setTempoToNoteSelected(note, tempo)));
      emit(MetronomeSoundState(
          normalSound: normalSound, accentSound: accentSound));
    });

    on<DisposeMetronomeEvent>((event, emit) async {
      await _metronome.dispose();
    });

    on<ToggleOnOffMetronomeEvent>((event, emit) async {
      emit(LoadingMetronomeState());
      if (_metronome.isMetronomeOn) {
        await _metronome.off();
      } else {
        await _metronome.on(setTempoToNoteSelected(note, tempo), note);
      }
      emit(ToggleOnOffMetronomeState(isMetronomeOn: _metronome.isMetronomeOn));
      debugPrint(
          "TOGGLED METRONOME ${_metronome.isMetronomeOn ? "ON" : "OFF"}");
    });

    on<IncreaseTempoMetronomeEvent>((event, emit) async {
      emit(LoadingMetronomeState());

      if (tempo == 230) {
        tempo = 24;
      } else {
        tempo++;
      }

      _debouncer.run(
          () async {
            if (_metronome.isMetronomeOn) {
              tempoAdjustedWhenOn = true;
              await _metronome.off();
            }
            if (tempoAdjustedWhenOn) {
              await _metronome.on(setTempoToNoteSelected(note, tempo), note);
              tempoAdjustedWhenOn = false;
            }
          }
      );

      SharedPrefsApi.setMetronomeSpeedValue(tempo);
      emit(MetronomeTempoState(tempo: tempo));
    });

    on<DecreaseTempoMetronomeEvent>((event, emit) async {
      emit(LoadingMetronomeState());

      if (tempo == 24) {
        tempo = 230;
      } else {
        tempo--;
      }
      _debouncer.run( () async {
        if (_metronome.isMetronomeOn) {
          tempoAdjustedWhenOn = true;
          await _metronome.off();
        }
        if (tempoAdjustedWhenOn) {
          await _metronome.on(setTempoToNoteSelected(note, tempo), note);
          tempoAdjustedWhenOn = false;
        }
      });
      SharedPrefsApi.setMetronomeSpeedValue(tempo);
      emit(MetronomeTempoState(tempo: tempo));
    });

    on<ChangeTempoMetronomeEvent>((event, emit) async {
      emit(LoadingMetronomeState());
      tempo = event.givenTempo;
      if (_metronome.isMetronomeOn) {
        await _metronome.off();
        await _metronome.on(setTempoToNoteSelected(note, tempo), note);
      }
      SharedPrefsApi.setMetronomeSpeedValue(tempo);
      emit(MetronomeTempoState(tempo: tempo));
    });

    on<ChangeMetronomeSoundMetronomeEvent>((event, emit) async {
      emit(LoadingMetronomeState());
      normalSound = event.soundName;
      accentSound = event.accentSoundName;
      SharedPrefsApi.setAccentSoundValue(accentSound);
      SharedPrefsApi.setNormalSoundValue(normalSound);
      await _metronome.changeMetronomeSound(
          normalSound, accentSound, setTempoToNoteSelected(note, tempo));
      emit(MetronomeSoundState(
          normalSound: normalSound, accentSound: accentSound));
    });

    on<ChangeNotesEvent>((event, emit) async {
      emit(LoadingMetronomeState());
      note = event.note;
      getIt<MetronomeService>().selectedNote = note;
      SharedPrefsApi.setNotesValue(note);
      if (note != Notes.fourth) {
        emit(MetronomeSoundState(
            normalSound: getIt<MetronomeService>().normalSound,
            accentSound: getIt<MetronomeService>().accentSound));
      }
      if (_metronome.isMetronomeOn) {
        await _metronome.off();
        await _metronome.on(setTempoToNoteSelected(note, tempo), note);
      }
    });
  }
}
