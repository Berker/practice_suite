import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:practice_suite/constants/enums.dart';
import 'package:practice_suite/data/api/sound_api.dart';
import 'package:practice_suite/data/models/recording_log_entry.dart';
import 'package:practice_suite/generated/l10n.dart';
import 'package:practice_suite/logic/blocs/recording_log/recording_log_bloc.dart';
import 'package:practice_suite/presentation/widgets/shared/delete_confirmation_dialog.dart';
import 'package:share_plus/share_plus.dart';
import 'package:wakelock_plus/wakelock_plus.dart';
import 'dart:async';

import '../shared/pt_snackbar.dart';

class RecordingLogEntrylist extends StatefulWidget {
  const RecordingLogEntrylist({Key? key}) : super(key: key);

  @override
  _RecordingLogEntrylistState createState() => _RecordingLogEntrylistState();
}

class _RecordingLogEntrylistState extends State<RecordingLogEntrylist> {
  late SoundApi audioPlayer;
  bool _isPlaying = false;
  String _fileNamePlaying = "";
  int _currentProgress = 0; // in milliseconds
  Timer? _timer;

  @override
  void initState() {
    WakelockPlus.enable();
    audioPlayer = SoundApi();
    audioPlayer.initPlayerOnly();
    BlocProvider.of<RecordingLogBloc>(context).add(FetchRecordingLog());
    super.initState();
  }

  @override
  void dispose() {
    WakelockPlus.disable();
    _timer?.cancel();
    audioPlayer.disposePlayerOnly();
    super.dispose();
  }

  TWhenFinished? _whenFinishedPlaying() {
    _isPlaying = false;
    _currentProgress = 0;
    _timer?.cancel();
    setState(() {});
    return null;
  }

  void _startCountdown(int duration) {
    _currentProgress = 0;
    _timer = Timer.periodic(const Duration(milliseconds: 10), (timer) {
      setState(() {
        if (_currentProgress < duration) {
          _currentProgress += 10;
        } else {
          _timer?.cancel();
        }
      });
    });
  }

  int _parseDuration(String duration) {
    List<String> parts = duration.split(':');
    int hours = int.parse(parts[0]);
    int minutes = int.parse(parts[1]);
    int seconds = int.parse(parts[2]);
    return (hours * 3600 + minutes * 60 + seconds) * 1000;
  }

  String _formatDuration(int milliseconds) {
    int minutes = (milliseconds ~/ 60000);
    int seconds = (milliseconds % 60000) ~/ 1000;
    int millis = (milliseconds % 1000) ~/ 10; // Convert to two digits
    return '${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}:${millis.toString().padLeft(2, '0')}';
  }

  void _onShareRecording(
      BuildContext context, String filePath, String fileName) async {
    final box = context.findRenderObject() as RenderBox?;
    final data = XFile('$filePath.wav', mimeType: 'audio/wav', name: fileName);
    final shareResult = await Share.shareXFiles(
      [data],
      fileNameOverrides: [fileName],
      sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RecordingLogBloc, RecordingLogState>(
        builder: (context, state) {
      List<RecordingLogEntry> logEntries = [];

      if (state is LoadingRecordingLogState) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
      if (state is RecordingLogEntries) {
        logEntries = state.recordingLog;

        return logEntries.isEmpty
            ? Center(
                child: Text(S.of(context).youHaventRecordedAnythingYet),
              )
            : SingleChildScrollView(
                child: Column(
                  children: logEntries
                      .map((RecordingLogEntry entry) => ListTile(
                            visualDensity: VisualDensity.compact,
                            contentPadding: const EdgeInsets.only(left: 10),
                            title: Text(entry.title),
                            subtitle: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${entry.date} - ${entry.time}"),
                                Text(_isPlaying &&
                                        _fileNamePlaying == entry.fileName
                                    ? _formatDuration(_currentProgress)
                                    : entry.duration),
                              ],
                            ),
                            trailing: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                IconButton(
                                    visualDensity: VisualDensity.compact,
                                    onPressed: () async {
                                      if (_isPlaying) {
                                        _isPlaying = false;
                                        _timer?.cancel();
                                        await audioPlayer.stopPlaying();
                                        setState(() {});
                                      } else {
                                        _isPlaying = true;
                                        _fileNamePlaying = entry.fileName;
                                        setState(() {});
                                        _startCountdown(
                                            _parseDuration(entry.duration));
                                        await audioPlayer.play(entry.fileName,
                                            _whenFinishedPlaying);
                                      }
                                    },
                                    icon: Icon(
                                        _isPlaying &&
                                                _fileNamePlaying ==
                                                    entry.fileName
                                            ? Icons.stop
                                            : Icons.arrow_right,
                                        size: 30)),
                                IconButton(
                                    visualDensity: VisualDensity.compact,
                                    onPressed: () async {
                                      _onShareRecording(
                                          context,
                                          '${audioPlayer.pathToSaveAudio}${entry.fileName}',
                                          entry.fileName);
                                    },
                                    icon: const Icon(Icons.share)),
                                IconButton(
                                    visualDensity: VisualDensity.compact,
                                    onPressed: () {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return DeleteConfirmationDialog(
                                              id: entry.id,
                                              title: entry.title,
                                              entryType: EntryType.recordingLog,
                                              filePath: entry.fileName,
                                            );
                                          });
                                    },
                                    icon: const Icon(
                                      Icons.delete,
                                      color: Colors.red,
                                    ))
                              ],
                            ),
                          ))
                      .toList(),
                ),
              );
      }
      return Container();
    }, listener: (context, state) {
      if (state is AddedEntry) {
        PTSnackBar.showSnackBar(context, S.of(context).entryAddedSuccessfully);
      }
      if (state is RemovedEntry) {
        PTSnackBar.showSnackBar(
            context, S.of(context).entryRemovedSuccessfully);
      }
      if (state is UpdatedEntry) {
        PTSnackBar.showSnackBar(
            context, S.of(context).entryUpdatedSuccessfully);
      }
      if (state is RecordingLogFailureState) {
        PTSnackBar.showSnackBar(context, state.errorMessage);
      }
    });
  }
}
