import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sound/public/flutter_sound_player.dart';
import 'package:intl/intl.dart';
import 'package:practice_suite/data/api/sound_api.dart';
import 'package:practice_suite/data/models/recording_log_entry.dart';
import 'package:practice_suite/data/repositories/recording_log_repository.dart';
import 'package:practice_suite/generated/l10n.dart';
import 'package:practice_suite/logic/blocs/recording_log/recording_log_bloc.dart';
import 'package:uuid/uuid.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

class RecorderSheet extends StatefulWidget {
  const RecorderSheet({Key? key}) : super(key: key);

  @override
  _RecorderSheetState createState() => _RecorderSheetState();
}

class _RecorderSheetState extends State<RecorderSheet> {
  late SoundApi soundApi;
  final _form = GlobalKey<FormState>();
  String finalRecordingDuration = "";
  bool _isRecordingAvailable = false;
  bool _isPlaying = false;
  bool _isRecording = false;
  final bool _isIOS = Platform.isIOS;

  RecordingLogEntry _newRecording = RecordingLogEntry(
      id: const Uuid().v4(),
      date: DateFormat.yMd().format(DateTime.now()),
      time: DateFormat.Hm().format(DateTime.now()),
      title: "",
      duration: "",
      fileName: "");

  bool _saveNewRecordingEntry() {
    final isValid = _form.currentState!.validate();
    if (isValid) {
      _form.currentState!.save();
      return true;
    }
    return false;
  }

  TWhenFinished? _whenFinishedPlaying() {
    _isPlaying = false;
    setState(() {});
    return null;
  }

  @override
  void initState() {
    WakelockPlus.enable();
    soundApi = SoundApi();
    soundApi.init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    soundApi.dispose();
  }

  void _deleteTemporaryRecording() {
    RecordingLogRepository().deleteRecordingFile(_newRecording.id);
  }

  @override
  Widget build(BuildContext context) {
    // If user doesn't wanna save the recording and dismisses the recorder sheet, recording gets deleted
    return WillPopScope(
      onWillPop: () async {
        _deleteTemporaryRecording();
        Navigator.of(context).pop();
        return true;
      },
      child: Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                    padding: const EdgeInsets.only(top: 12, bottom: 10),
                    child: Text(S.of(context).newRecording,
                        style: Theme.of(context)
                            .textTheme.headlineMedium
                            ?.copyWith(fontSize: 20))),
                ValueListenableBuilder(
                    valueListenable: soundApi.getRecordingProgress,
                    builder:
                        (BuildContext context, String progress, Widget? child) {
                      finalRecordingDuration = progress;
                      return Text(
                        progress,
                        style: const TextStyle(fontSize: 30),
                      );
                    }),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                        iconSize: 40,
                        color: Colors.red,
                        onPressed: () {
                          if (!soundApi.isPlaying && !soundApi.isRecording) {
                            _isRecording = true;
                            soundApi.record(_newRecording.id);
                            _isRecordingAvailable = false;
                          } else {
                            _isRecording = false;
                            soundApi.stopRecording();
                            _isRecordingAvailable = true;
                          }
                          setState(() {});
                        },
                        icon: _isRecording
                            ? const Icon(
                                Icons.stop,
                              )
                            : const Icon(Icons.mic)),
                    const SizedBox(width: 5, height: 5),
                    if (_isRecordingAvailable)
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: IconButton(
                            iconSize: 50,
                            onPressed: () {
                              if (!soundApi.isPlaying &&
                                  !soundApi.isRecording &&
                                  _isRecordingAvailable) {
                                _isPlaying = true;
                                setState(() {});
                                soundApi.play(
                                    _newRecording.id, _whenFinishedPlaying);
                              } else {
                                _isPlaying = false;
                                setState(() {});
                                soundApi.stopPlaying();
                              }
                            },
                            icon: Icon(
                                _isPlaying ? Icons.stop : Icons.arrow_right)),
                      ),
                  ],
                ),
                if (_isRecordingAvailable)
                  Row(
                      children: [
                        const SizedBox(width: 8.0),
                        Expanded(
                          child: Form(
                            key: _form,
                            child: TextFormField(
                              decoration: InputDecoration(
                                isDense: true,
                                labelText: S.of(context).title,
                                border: const OutlineInputBorder(),
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return S.of(context).titleValidation;
                                }
                                return null;
                              },
                              onSaved: (value) {
                                _newRecording.title = value.toString();
                                _newRecording.duration = finalRecordingDuration;
                                _newRecording.fileName = _newRecording.id;
                              },
                            ),
                          ),
                        ),
                        const SizedBox(width: 8.0),
                        TextButton.icon(
                            onPressed: () {
                              if (_saveNewRecordingEntry()) {
                                BlocProvider.of<RecordingLogBloc>(context)
                                    .add(AddRecordingLogEntry(_newRecording));
                                Navigator.of(context).pop();
                              }
                            },
                            icon: const Icon(
                              Icons.save,
                              size: 30,
                            ),
                            label: Text(
                              S.of(context).save,
                              style: const TextStyle(fontSize: 16),
                            )),
                        const SizedBox(width: 8.0),
                      ],
                    ),
                Padding(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: const SizedBox(
                    height: 10,
                  ),
                )
              ],
            ),
            if (_isIOS) const SizedBox(height: 30),
          ],
        ),
      ),
    );
  }
}
