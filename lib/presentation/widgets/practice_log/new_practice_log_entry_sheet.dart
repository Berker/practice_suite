import 'dart:io';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:omni_datetime_picker/omni_datetime_picker.dart';
import 'package:practice_suite/data/models/practice_log_entry.dart';
import 'package:practice_suite/logic/blocs/practice_log/practice_log_bloc.dart';
import 'package:practice_suite/presentation/widgets/shared/pt_container.dart';
import 'package:practice_suite/presentation/widgets/shared/pt_snackbar.dart';
import 'package:uuid/uuid.dart';
import 'package:practice_suite/generated/l10n.dart';

import '../../../services/locator_service.dart';

class NewPracticeLogEntrySheet extends StatefulWidget {
  final PracticeLogEntry? newLogEntry;

  const NewPracticeLogEntrySheet({Key? key, this.newLogEntry})
      : super(key: key);

  @override
  State<NewPracticeLogEntrySheet> createState() =>
      _NewPracticeLogEntrySheetState();
}

class _NewPracticeLogEntrySheetState extends State<NewPracticeLogEntrySheet> {
  final _durationFocusNode = FocusNode();
  final _descFocusNode = FocusNode();
  final _dateFocusNode = FocusNode();
  final _metronomeFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  final _tagFocusNode = FocusNode();
  bool _isEditing = false;
  bool _isAddingTag = false;
  TextEditingController tagTextController = TextEditingController();
  final bool _isIOS = Platform.isIOS;
  late Map<String, bool> availableTags;
  bool _isTagValidated = false;
  final DateFormat dateFormat = DateFormat("d MMM, yyyy");

  PracticeLogEntry _newEntry = PracticeLogEntry(
      id: const Uuid().v4(),
      title: "",
      duration: "",
      date: DateTime.now().toIso8601String(),
      tags: []);

  bool _saveNewLogEntry() {
    final isAnyTagSelected = _isAnyTagSelected();
    if (!isAnyTagSelected) {
      PTSnackBar.showOverlaySnackBar(
          context, S.of(context).practice_log_error_no_tag_selected);
    }
    final isValid = _form.currentState!.validate() && isAnyTagSelected;
    if (isValid) {
      _newEntry.tags = [];
      availableTags.forEach((tag, isSelected) {
        if (isSelected) {
          _newEntry.tags.add(tag);
        }
      });
      _form.currentState!.save();
      return true;
    }
    return false;
  }

  String? validateTag(String tagText) {
    _isTagValidated = false;
    if (tagText.trim() == "") {
      _isTagValidated = false;
      tagTextController.clear();
      return S.of(context).practice_log_error_empty_tag;
    }
    _isTagValidated = true;
    return null;
  }

  bool _isAnyTagSelected() {
    if (availableTags.isEmpty) {
      return false;
    }
    bool isAnyTagSelected = false;
    availableTags.forEach((tag, isSelected) {
      if (isSelected) {
        isAnyTagSelected = true;
      }
    });
    return isAnyTagSelected;
  }

  void saveTag() {
    if (_isTagValidated) {
      if (availableTags.keys.contains(tagTextController.text)) {
        availableTags[tagTextController.text] = true;
      } else {
        availableTags.putIfAbsent(tagTextController.text, () => true);
      }
      tagTextController.clear();
    }
  }

  Future<DateTime?> _showDateTimePickerDialog(
      BuildContext context, DateTime initialDate) async {
    return await showDialog<DateTime>(
      context: context,
      builder: (BuildContext context) {
        DateTime selectedDate = initialDate;
        return Dialog(
            child: Stack(children: [
          OmniDateTimePicker(
            type: OmniDateTimePickerType.date,
            lastDate: DateTime.now(),
            initialDate: initialDate,
            onDateTimeChanged: (DateTime value) {
              selectedDate = value;
            },
          ),
          Positioned(
            right: 10,
            bottom: 5,
            child: TextButton(
                onPressed: () => Navigator.of(context).pop(selectedDate),
                child: Text(S.of(context).done)),
          ),
        ]));
      },
    );
  }

  @override
  void initState() {
    availableTags = getIt<PracticeLogBloc>().availableTags;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.newLogEntry != null) {
      _newEntry = widget.newLogEntry!;
      for (var tag in _newEntry.tags) {
        if (availableTags.containsKey(tag)) {
          availableTags[tag] = true;
        }
      }
      _isEditing = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(25))),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 12, bottom: 10),
                child: Text(
                    _isEditing
                        ? S.of(context).editLogEntry
                        : S.of(context).newLogDialogTitle,
                    style: Theme.of(context).textTheme.displayLarge),
              ),
              Form(
                key: _form,
                child: Column(
                  children: [
                    TextFormField(
                      initialValue: _newEntry.title,
                      decoration: InputDecoration(
                        labelText: S.of(context).title,
                        border: const OutlineInputBorder(),
                      ),
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(_durationFocusNode);
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return S.of(context).titleValidation;
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _newEntry.title = value.toString();
                      },
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Expanded(
                          child: !_isAddingTag
                              ? PTContainer(
                                  padding: EdgeInsets.zero,
                                  borderColor: Theme.of(context).dividerColor,
                                  child: SizedBox(
                                    width: double.infinity,
                                    height: 57,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0),
                                      child: availableTags.isEmpty
                                          ? Align(
                                              alignment: Alignment.centerLeft,
                                              child: Text(
                                                S.of(context).tagsEmpty,
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .dividerColor),
                                              ))
                                          : ListView(
                                              scrollDirection: Axis.horizontal,
                                              children:
                                                  availableTags.keys.map((tag) {
                                                return Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 8.0),
                                                  child: FilterChip(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                      ),
                                                      label: Text(tag,
                                                          style: TextStyle(
                                                              color: availableTags[
                                                                      tag]!
                                                                  ? Theme.of(context)
                                                                      .floatingActionButtonTheme
                                                                      .foregroundColor
                                                                  : Theme.of(
                                                                          context)
                                                                      .listTileTheme
                                                                      .selectedColor)),
                                                      checkmarkColor:
                                                          Theme.of(context)
                                                              .listTileTheme
                                                              .textColor,
                                                      selectedColor: Theme
                                                              .of(context)
                                                          .highlightColor,
                                                      backgroundColor:
                                                          Theme.of(context)
                                                              .cardColor,
                                                      selected:
                                                          availableTags[tag]!,
                                                      onSelected: (bool value) {
                                                        setState(() {
                                                          availableTags[tag] =
                                                              value;
                                                        });
                                                      }),
                                                );
                                              }).toList()),
                                    ),
                                  ))
                              : TextField(
                                  onChanged: (value) {
                                    setState(() {});
                                  },
                                  focusNode: _tagFocusNode,
                                  controller: tagTextController,
                                  decoration: InputDecoration(
                                    labelText: S.of(context).newTag,
                                    border: const OutlineInputBorder(),
                                    errorText: _tagFocusNode.hasFocus
                                        ? validateTag(tagTextController.text)
                                        : null,
                                  ),
                                ),
                        ),
                        const SizedBox(width: 8),
                        SizedBox(
                          height: 59,
                          width: 50,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(5.0),
                            onTap: () {
                              if (!_isAddingTag) {
                                _tagFocusNode.requestFocus();
                              }
                              validateTag(tagTextController.text);
                              if (_isTagValidated) {
                                saveTag();
                                setState(() {
                                  _isAddingTag = !_isAddingTag;
                                });
                              } else {
                                setState(() {
                                  _isAddingTag = !_isAddingTag;
                                });
                              }
                            },
                            child: PTContainer(
                                padding: EdgeInsets.zero,
                                borderColor: Theme.of(context).dividerColor,
                                child: Icon(
                                  _isAddingTag ? Icons.check : Icons.add,
                                  color: tagTextController.text.isEmpty
                                      ? Theme.of(context).dividerColor
                                      : Theme.of(context).colorScheme.primary,
                                )),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    PTContainer(
                        padding: EdgeInsets.zero,
                        borderColor: Theme.of(context).dividerColor,
                        child: SizedBox(
                            width: double.infinity,
                            height: 57,
                            child: InkWell(
                              onTap: () {
                                _showDateTimePickerDialog(
                                        context, DateTime.parse(_newEntry.date))
                                    .then((value) {
                                  if (value != null) {
                                    setState(() {
                                      _newEntry.date = value.toIso8601String();
                                    });
                                  }
                                });
                              },
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    child: Text(
                                      dateFormat.format(
                                          DateTime.parse(_newEntry.date)),
                                      style: TextStyle(
                                          color:
                                              Theme.of(context).dividerColor),
                                    ),
                                  )),
                            ))),
                    Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: TextFormField(
                              initialValue: _newEntry.duration,
                              decoration: InputDecoration(
                                errorMaxLines: 2,
                                labelText: S.of(context).duration,
                                border: const OutlineInputBorder(),
                              ),
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.next,
                              onFieldSubmitted: (_) {
                                FocusScope.of(context)
                                    .requestFocus(_metronomeFocusNode);
                              },
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return S.of(context).durationValidation;
                                }
                                if (int.tryParse(value) == null) {
                                  return S
                                      .of(context)
                                      .duration_format_validation;
                                }
                                if (int.tryParse(value) != null &&
                                        int.tryParse(value)! > 720 ||
                                    int.tryParse(value)! < 1) {
                                  return S
                                      .of(context)
                                      .duration_amount_validation;
                                }
                                return null;
                              },
                              onSaved: (value) {
                                _newEntry.duration = value.toString();
                              },
                            ),
                          ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: TextFormField(
                              initialValue: _newEntry.metronome,
                              decoration: InputDecoration(
                                errorMaxLines: 2,
                                labelText: S.of(context).navBarMetronome,
                                border: const OutlineInputBorder(),
                              ),
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.next,
                              onFieldSubmitted: (_) {
                                FocusScope.of(context)
                                    .requestFocus(_dateFocusNode);
                              },
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              validator: (value) {
                                if (int.tryParse(value!) == null) {
                                  return S
                                      .of(context)
                                      .metronome_format_validation;
                                }
                                if (int.tryParse(value) != null &&
                                        int.tryParse(value)! > 230 ||
                                    int.tryParse(value)! < 24) {
                                  return S
                                      .of(context)
                                      .metronome_limit_validation;
                                }
                                return null;
                              },
                              onSaved: (value) {
                                _newEntry.metronome = value.toString();
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: TextFormField(
                        initialValue: _newEntry.description,
                        maxLines: 3,
                        decoration: InputDecoration(
                          labelText: S.of(context).description,
                          border: const OutlineInputBorder(),
                        ),
                        textInputAction: TextInputAction.done,
                        onSaved: (value) {
                          if (value!.isEmpty) {
                            _newEntry.description =
                                S.of(context).optionalDescriptionText;
                          } else {
                            _newEntry.description = value.toString();
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(S.of(context).cancel),
                    ),
                    TextButton(
                      onPressed: () {
                        if (_saveNewLogEntry()) {
                          getIt<PracticeLogBloc>().add(_isEditing
                              ? UpdatePracticeLogEntry(_newEntry)
                              : AddPracticeLogEntry(_newEntry));
                          Navigator.of(context).pop();
                        }
                      },
                      child: Text(S.of(context).done),
                    )
                  ],
                ),
              ),
              if (_isIOS) const SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }
}
