import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/logic/blocs/practice_log/practice_log_bloc.dart';

import '../../../generated/l10n.dart';
import '../../../services/locator_service.dart';

class PracticeLogTags extends StatefulWidget {
  const PracticeLogTags({Key? key}) : super(key: key);

  @override
  State<PracticeLogTags> createState() => _PracticeLogTagsState();
}

class _PracticeLogTagsState extends State<PracticeLogTags> {
  late Map<String, bool> availableTags;
  late bool showAllTags;

  @override
  void initState() {
    availableTags = getIt<PracticeLogBloc>().availableTags;
    showAllTags = !availableTags.containsValue(false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 50,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: BlocConsumer<PracticeLogBloc, PracticeLogState>(
          listener: (context, state) {
            if (state is PracticeLogEntries) {
              availableTags = getIt<PracticeLogBloc>().availableTags;
            }
          },
          builder: (context, state) {
            return Row(
              children: [
                Row(
                  children: [
                    RotatedBox(
                      quarterTurns: 3,
                      child: FittedBox(
                        child: Text(
                          S.of(context).tagsTitle,
                          style: Theme.of(context).textTheme.displayLarge,
                        ),
                      ),
                    ),
                    VerticalDivider(color: Theme.of(context).colorScheme.primary,
                      thickness: 1,
                      endIndent: 8,
                      indent: 8,
                    ),
                  ],
                ),
                const SizedBox(width: 3),
                Expanded(
                  child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: availableTags.keys.map((tag) {
                        return Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: FilterChip(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                 ),
                              label: Text(
                                tag,
                                style:  TextStyle(
                                    color: availableTags[tag]! ? Theme.of(context)
                                        .floatingActionButtonTheme
                                        .foregroundColor : Theme.of(context).listTileTheme.selectedColor),
                              ),
                              selectedColor: Theme.of(context).highlightColor,
                              backgroundColor: Theme.of(context).cardColor,
                              selected: availableTags[tag]!,
                              onSelected: (bool value) {
                                setState(() {
                                  availableTags[tag] = value;
                                  if (!availableTags.containsValue(false)) {
                                    showAllTags = !showAllTags;
                                  } else {
                                    showAllTags = false;
                                  }
                                });
                                getIt<PracticeLogBloc>().add(
                                    FilterEntriesWithTags(
                                        selectedTags: availableTags));
                              }),
                        );
                      }).toList()),
                ),
                IconButton(
                    onPressed: () {
                      showAllTags = !showAllTags;
                      if (!availableTags.containsValue(false)) {
                        setState(() {
                          availableTags
                              .updateAll((key, value) => value = false);
                        });
                      } else {
                        setState(() {
                          availableTags.updateAll((key, value) => value = true);
                        });
                      }
                      getIt<PracticeLogBloc>().add(
                          FilterEntriesWithTags(selectedTags: availableTags));
                    },
                    icon:
                        Icon(showAllTags ? Icons.check_box : Icons.select_all)),
              ],
            );
          },
        ),
      ),
    );
  }
}
