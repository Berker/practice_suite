import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:practice_suite/logic/blocs/practice_log/practice_log_bloc.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../../generated/l10n.dart';
import '../../../logic/blocs/language/language_bloc.dart';
import '../../../services/locator_service.dart';

class PracticeLogListFilter extends StatefulWidget {
  const PracticeLogListFilter({Key? key}) : super(key: key);

  @override
  _PracticeLogListFilterState createState() => _PracticeLogListFilterState();
}

class _PracticeLogListFilterState extends State<PracticeLogListFilter> {
  late DateTime _startDate;
  late DateTime _endDate;
  late bool _showAllEntries;
  final bool _isIOS = Platform.isIOS;

  @override
  void initState() {
    super.initState();
    _endDate = getIt<PracticeLogBloc>().filterEndDate;
    _startDate = getIt<PracticeLogBloc>().filterStartDate;
    _showAllEntries = getIt<PracticeLogBloc>().showAllEntries;
  }

  void _showDateRangePicker() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (_) {
        return _dateRangePicker();
      },
    );
  }

  Widget _dateRangePicker() {
    return Card(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(25)),
      ),
      child: Padding(
        padding: EdgeInsets.only(
            right: 10, left: 10, top: 15, bottom: _isIOS ? 30 : 0),
        child: Stack(children: [
          SfDateRangePicker(
            backgroundColor: Colors.transparent,
            cancelText: S.of(context).cancel,
            confirmText: S.of(context).done,
            selectionMode: DateRangePickerSelectionMode.range,
            showActionButtons: true,
            view: DateRangePickerView.month,
            maxDate: DateTime.now(),
            selectionShape: DateRangePickerSelectionShape.rectangle,
            onSubmit: (value) {
              if (value != null) {
                PickerDateRange range = value as PickerDateRange;
                setState(() {
                  _startDate = (range.startDate as DateTime);
                  _endDate = (range.endDate ?? range.startDate as DateTime);
                  _showAllEntries = false;
                });
                getIt<PracticeLogBloc>().add(FilterEntriesInDateRange(
                    startDate: _startDate,
                    endDate: _endDate,
                    showAllEntries: false));
              }
              Navigator.of(context).pop();
            },
            onCancel: () {
              Navigator.of(context).pop();
            },
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: TextButton(
                onPressed: () {
                  getIt<PracticeLogBloc>().add(FilterEntriesInDateRange(
                      startDate: DateTime(1800, 1, 1),
                      endDate: DateTime.now(),
                      showAllEntries: true));
                  setState(() {
                    _showAllEntries = true;
                  });
                  Navigator.of(context).pop();
                },
                child: Text(S.of(context).practice_log_filter_allEntries),
              ),
            ),
          )
        ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: () => _showDateRangePicker(),
        child: Row(
          children: [
            IntrinsicHeight(
              child: Row(
                children: [
                  const Icon(Icons.sort_outlined),
                  const SizedBox(width: 8),
                  Text(
                    S.of(context).practice_log_filter_title,
                    style: Theme.of(context).textTheme.displayLarge,
                  ),
                  VerticalDivider(
                    color: Theme.of(context).colorScheme.primary,
                    thickness: 1,
                  ),
                ],
              ),
            ),
            const Expanded(child: SizedBox()),
            BlocBuilder<LanguageBloc, LanguageState>(
              bloc: getIt<LanguageBloc>(),
              buildWhen: (previousState, currentState) =>
                  currentState is ChangeLanguageState,
              builder: (context, state) {
                return Text(_showAllEntries
                    ? S.of(context).practice_log_filter_allEntries
                    : _startDate == _endDate
                        ? DateFormat("dd MMM yyyy").format(_startDate)
                        : "${DateFormat("dd MMM yyyy").format(_startDate)} - ${DateFormat("dd MMM yyyy").format(_endDate)}");
              },
            ),
          ],
        ),
      ),
    );
  }
}
