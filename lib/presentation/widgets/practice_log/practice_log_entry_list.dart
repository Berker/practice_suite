import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:practice_suite/constants/enums.dart';
import 'package:practice_suite/generated/l10n.dart';
import 'package:practice_suite/logic/blocs/practice_log/practice_log_bloc.dart';
import 'package:practice_suite/presentation/widgets/practice_log/new_practice_log_entry_sheet.dart';
import 'package:practice_suite/presentation/widgets/shared/pt_snackbar.dart';

import '../../../services/locator_service.dart';
import '../shared/delete_confirmation_dialog.dart';

class PracticeLogEntryList extends StatefulWidget {
  const PracticeLogEntryList({Key? key}) : super(key: key);

  @override
  _PracticeLogEntryListState createState() => _PracticeLogEntryListState();
}

class _PracticeLogEntryListState extends State<PracticeLogEntryList> {
  @override
  void initState() {
    super.initState();
    getIt<PracticeLogBloc>().add(FetchPracticeLog());
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PracticeLogBloc, PracticeLogState>(
        builder: (context, state) {
      List<PanelItem> logEntries = [];

      if (state is LoadingPracticeLogState) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
      if (state is PracticeLogEntries) {
        logEntries = state.practiceLog;

        return logEntries.isEmpty
            ? Center(
                child: Text(S.of(context).noLogEntryWarning),
              )
            : SingleChildScrollView(
                child: ExpansionPanelList(
                  elevation: 3,
                  animationDuration: const Duration(milliseconds: 600),
                  expansionCallback: (int index, bool isExpanded) {
                    setState(() {
                      logEntries[index].isExpanded = !logEntries[index].isExpanded;
                    });
                  },
                  children: logEntries.map<ExpansionPanel>((PanelItem item) {
                    return ExpansionPanel(
                      canTapOnHeader: true,
                      headerBuilder: (BuildContext context, bool isExpanded) {
                        return ListTile(
                          visualDensity: VisualDensity.compact,
                          title: Text(item.logEntry.title),
                          subtitle: Text(DateFormat('d MMM, yyyy')
                              .format(DateTime.parse(item.logEntry.date))),
                        );
                      },
                      body: ListTile(
                        dense: true,
                        title: Text(
                            "${item.logEntry.duration} ${S.of(context).minutesPracticed}"),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 5),
                            Text(item.logEntry.description),
                            const SizedBox(height: 5),
                            if (item.logEntry.metronome != "")
                              Text(
                                  "${S.of(context).navBarMetronome}: ${item.logEntry.metronome}")
                          ],
                        ),
                        trailing: SizedBox(
                          width: 100,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              IconButton(
                                onPressed: () {
                                  showModalBottomSheet(
                                    backgroundColor: Colors.transparent,
                                    context: context,
                                    isScrollControlled: true,
                                    builder: (_) {
                                      // Makes the AddNewTransactions slide from the bottom
                                      return NewPracticeLogEntrySheet(
                                          newLogEntry: item.logEntry);
                                    },
                                  );
                                },
                                icon: const Icon(
                                  Icons.edit,
                                  color: Colors.greenAccent,
                                ),
                              ),
                              IconButton(
                                color: Colors.red,
                                icon: const Icon(Icons.delete),
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return DeleteConfirmationDialog(
                                            id: item.logEntry.id,
                                            title: item.headerValue,
                                        entryType: EntryType.practiceLog,
                                        );
                                      });
                                  setState(() {});
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      isExpanded: item.isExpanded,
                    );
                  }).toList(),
                ),
              );
      }
      return Container();
    }, listener: (context, state) {

      if (state is AddedEntry) {
        PTSnackBar.showSnackBar(context,S.of(context).entryAddedSuccessfully);
      }
      if (state is RemovedEntry) {
        PTSnackBar.showSnackBar(context,S.of(context).entryRemovedSuccessfully);
      }
      if (state is UpdatedEntry) {
        PTSnackBar.showSnackBar(context,S.of(context).entryUpdatedSuccessfully);
      }
    });
  }
}
