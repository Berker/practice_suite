import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/constants/enums.dart';
import 'package:practice_suite/generated/l10n.dart';
import 'package:practice_suite/logic/blocs/practice_log/practice_log_bloc.dart';
import 'package:practice_suite/logic/blocs/recording_log/recording_log_bloc.dart';

import '../../../services/locator_service.dart';

class DeleteConfirmationDialog extends StatelessWidget {
  final String id;
  final String title;
  final EntryType entryType;
  final String? filePath;

  const DeleteConfirmationDialog({
    Key? key,
    this.filePath,
    required this.id,
    required this.title,
    required this.entryType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(S.of(context).areYouSure),
      content: Text("${S.of(context).wannaRemoveEntry} '$title' ?"),
      titleTextStyle: Theme.of(context).textTheme.titleMedium,
      actions: [
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(S.of(context).no)),
        TextButton(
            onPressed: () {
              if (entryType == EntryType.recordingLog) {
                BlocProvider.of<RecordingLogBloc>(context)
                    .add(RemoveRecordingLogEntry(id, filePath!));
              } else {
                getIt<PracticeLogBloc>().add(RemovePracticeLogEntry(id));
              }
              Navigator.of(context).pop();
            },
            child: Text(S.of(context).yes)),
      ],
    );
  }
}
