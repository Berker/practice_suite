import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:practice_suite/data/models/practice_duration.dart';
import 'package:practice_suite/utils/color_extensions.dart';

import '../../../generated/l10n.dart';

class WeeklyStatisticsChart extends StatefulWidget {
  final String title;
  final List<double> stats;
  final PracticeDuration totalHoursMinutes;

  const WeeklyStatisticsChart(
      {super.key,
      required this.title,
      required this.stats,
      required this.totalHoursMinutes});

  @override
  State<StatefulWidget> createState() => WeeklyStatisticsChartState();
}

class WeeklyStatisticsChartState extends State<WeeklyStatisticsChart> {
  int touchedIndex = -1;

  bool isPlaying = false;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        color: Colors.transparent,
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 5, left: 5, right: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    widget.title,
                    style: TextStyle(
                      color: Theme.of(context).brightness == Brightness.light
                          ? Colors.black
                          : Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    "${S.of(context).statistics_txt_total}: ${S.of(context).statistics_hours_and_minutes(widget.totalHoursMinutes.hour, widget.totalHoursMinutes.minute)}",
                    style: TextStyle(
                      color: Theme.of(context).brightness == Brightness.light
                          ? Colors.black54
                          : Colors.white54,
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Expanded(
                    child: BarChart(
                      mainBarData(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  BarChartGroupData makeGroupData(
    int x,
    double y, {
    bool isTouched = false,
    double width = 12,
    List<int> showTooltips = const [],
  }) {
    return BarChartGroupData(
      x: x,
      barRods: [
        BarChartRodData(
          toY: isTouched ? y + 1 : y,
          color: isTouched ? Colors.yellow : Colors.deepOrangeAccent,
          width: width,
          borderSide: isTouched
              ? BorderSide(color: Colors.yellow.darken())
              : const BorderSide(color: Colors.white, width: 0),
          backDrawRodData: BackgroundBarChartRodData(
            show: true,
            toY: 12,
            color: Theme.of(context).brightness == Brightness.light
                ? Colors.lightBlue
                : const Color(0xff72d8bf),
          ),
        ),
      ],
      showingTooltipIndicators: showTooltips,
    );
  }

  List<BarChartGroupData> showingGroups() => List.generate(7, (i) {
        switch (i) {
          case 0:
            return makeGroupData(0, widget.stats[0],
                isTouched: i == touchedIndex);
          case 1:
            return makeGroupData(1, widget.stats[1],
                isTouched: i == touchedIndex);
          case 2:
            return makeGroupData(2, widget.stats[2],
                isTouched: i == touchedIndex);
          case 3:
            return makeGroupData(3, widget.stats[3],
                isTouched: i == touchedIndex);
          case 4:
            return makeGroupData(4, widget.stats[4],
                isTouched: i == touchedIndex);
          case 5:
            return makeGroupData(5, widget.stats[5],
                isTouched: i == touchedIndex);
          case 6:
            return makeGroupData(6, widget.stats[6],
                isTouched: i == touchedIndex);
          default:
            return throw Error();
        }
      });

  BarChartData mainBarData() {
    return BarChartData(
      barTouchData: BarTouchData(
        touchTooltipData: BarTouchTooltipData(
          getTooltipItem: (group, groupIndex, rod, rodIndex) {
            String weekDay;
            switch (group.x) {
              case 0:
                weekDay = S.of(context).statistics_day_monday;
                break;
              case 1:
                weekDay = S.of(context).statistics_day_tuesday;
                break;
              case 2:
                weekDay = S.of(context).statistics_day_wednesday;
                break;
              case 3:
                weekDay = S.of(context).statistics_day_thursday;
                break;
              case 4:
                weekDay = S.of(context).statistics_day_friday;
                break;
              case 5:
                weekDay = S.of(context).statistics_day_saturday;
                break;
              case 6:
                weekDay = S.of(context).statistics_day_sunday;
                break;
              default:
                throw Error();
            }
            return BarTooltipItem(
              '$weekDay\n',
              TextStyle(
                color: Theme.of(context).brightness == Brightness.light
                    ? Colors.black
                    : Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
              children: <TextSpan>[
                TextSpan(
                  text: rod.toY < 2.0
                      ? "${rod.toY.toString().substring(2)} m"
                      : "${(rod.toY - 1).toStringAsFixed(2).split(".")[0]} h, ${(rod.toY - 1).toStringAsFixed(2).split(".")[1]} m",
                  style: const TextStyle(
                    color: Colors.yellow,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            );
          },
        ),
        touchCallback: (FlTouchEvent event, barTouchResponse) {
          setState(() {
            if (!event.isInterestedForInteractions ||
                barTouchResponse == null ||
                barTouchResponse.spot == null) {
              touchedIndex = -1;
              return;
            }
            touchedIndex = barTouchResponse.spot!.touchedBarGroupIndex;
          });
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        rightTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        bottomTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            getTitlesWidget: getTitles,
            reservedSize: 38,
          ),
        ),
        leftTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: false,
          ),
        ),
      ),
      borderData: FlBorderData(
        show: false,
      ),
      barGroups: showingGroups(),
      gridData: FlGridData(show: false),
    );
  }

  Widget getTitles(double value, TitleMeta meta) {
    var style = TextStyle(
      color: Theme.of(context).brightness == Brightness.light
          ? Colors.black
          : Colors.white,
      fontWeight: FontWeight.bold,
      fontSize: 14,
    );
    Widget text;
    switch (value.toInt()) {
      case 0:
        text = Text(S.of(context).statistics_day_monday.substring(0, 1),
            style: style);
        break;
      case 1:
        text = Text(S.of(context).statistics_day_tuesday.substring(0, 1),
            style: style);
        break;
      case 2:
        text = Text(S.of(context).statistics_day_wednesday.substring(0, 1),
            style: style);
        break;
      case 3:
        text = Text(S.of(context).statistics_day_thursday.substring(0, 1),
            style: style);
        break;
      case 4:
        text = Text(S.of(context).statistics_day_friday.substring(0, 1),
            style: style);
        break;
      case 5:
        text = Text(S.of(context).statistics_day_saturday.substring(0, 1),
            style: style);
        break;
      case 6:
        text = Text(S.of(context).statistics_day_sunday.substring(0, 1),
            style: style);
        break;
      default:
        text = Text('', style: style);
        break;
    }
    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 12,
      child: text,
    );
  }
}
