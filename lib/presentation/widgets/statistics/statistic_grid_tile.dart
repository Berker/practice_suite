import 'package:flutter/material.dart';
import 'package:practice_suite/presentation/widgets/shared/pt_container.dart';

class StatisticGridTile extends StatelessWidget {
  final String title;
  final String value;

  const StatisticGridTile({Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PTContainer(
      child: Column(
        children: [
          Flexible(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 3, right: 3),
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  maxLines: 5,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
          ),
          Divider(
            indent: 20,
            endIndent: 20,
            color: Theme.of(context).colorScheme.primary,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Center(
              child: Text(value),
            ),
          ),
        ],
      ),
    );
  }
}