import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

import '../../../generated/l10n.dart';
import '../../../logic/blocs/tuner/tuner_bloc.dart';
import '../../../services/locator_service.dart';
import '../../../services/tuner/tuning_status.dart';

class TunerWidget extends StatefulWidget {
  const TunerWidget({Key? key}) : super(key: key);

  @override
  State<TunerWidget> createState() => _TunerWidgetState();
}

class _TunerWidgetState extends State<TunerWidget> {
  String note = "-";
  bool _isTunerOn = false;
  double dialValue = 50;
  late double pitch;

  @override
  void initState() {
    WakelockPlus.enable();
    pitch = getIt<TunerBloc>().pitch;
    getIt<TunerBloc>().add(InitTunerEvent());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    WakelockPlus.disable();
    note = "-";
    if (_isTunerOn) {
      getIt<TunerBloc>().add(ToggleTunerEvent());
      _isTunerOn = false;
    }
    getIt<TunerBloc>().add(DisposeTunerEvent());
  }

  void increasePitch() {
    if (pitch < 450) {
      pitch = pitch + 1;
      getIt<TunerBloc>().add(ChangePitchTunerEvent(pitch: pitch));
      setState(() {});
    }
  }

  void decreasePitch() {
    if (pitch > 400) {
      pitch = pitch - 1;
      getIt<TunerBloc>().add(ChangePitchTunerEvent(pitch: pitch));
      setState(() {});
    }
  }

  Widget buildTuningIndicators() {
    return BlocBuilder<TunerBloc, TunerState>(
      bloc: getIt<TunerBloc>(),
      builder: (context, state) {
        if (state is UpdateNoteTunerState) {
          note = state.note;

          switch (state.pitchStatus) {
            case TuningStatus.tunedMinus:
              dialValue = 45;
              break;
            case TuningStatus.tuned:
              dialValue = 50;
              break;
            case TuningStatus.tunedPlus:
              dialValue = 55;
              break;
            case TuningStatus.lowMinus:
              dialValue = 25;
              break;
            case TuningStatus.low:
              dialValue = 30;
              break;
            case TuningStatus.lowPlus:
              dialValue = 35;
              break;
            case TuningStatus.highMinus:
              dialValue = 65;
              break;
            case TuningStatus.high:
              dialValue = 70;
              break;
            case TuningStatus.highPlus:
              dialValue = 75;
              break;
            case TuningStatus.tooLowMinus:
              dialValue = 5;
              break;
            case TuningStatus.tooLow:
              dialValue = 10;
              break;
            case TuningStatus.tooLowPlus:
              dialValue = 15;
              break;
            case TuningStatus.tooHighMinus:
              dialValue = 85;
              break;
            case TuningStatus.tooHigh:
              dialValue = 90;
              break;
            case TuningStatus.tooHighPlus:
              dialValue = 95;
              break;
            case TuningStatus.undefined:
              dialValue = 50;
              note = "-";
              break;
          }
        }

        return SingleChildScrollView(
          child: Column(
            children: [
              SfRadialGauge(
                axes: <RadialAxis>[
                  RadialAxis(
                      showLabels: false,
                      showAxisLine: false,
                      showTicks: false,
                      minimum: 0,
                      maximum: 99,
                      ranges: <GaugeRange>[
                        GaugeRange(
                            startValue: 0,
                            endValue: 20,
                            color: const Color(0xFFFE2A25),
                            label: S.of(context).tuner_status_too_low,
                            sizeUnit: GaugeSizeUnit.factor,
                            labelStyle: GaugeTextStyle(
                                fontFamily: 'Times',
                                fontSize: 20,
                                color: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .color),
                            startWidth: 0.65,
                            endWidth: 0.65),
                        GaugeRange(
                            startValue: 20,
                            endValue: 40,
                            color: const Color(0xFFFFBA00),
                            label: S.of(context).tuner_status_low,
                            sizeUnit: GaugeSizeUnit.factor,
                            labelStyle: GaugeTextStyle(
                                fontFamily: 'Times',
                                fontSize: 20,
                                color: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .color),
                            startWidth: 0.65,
                            endWidth: 0.65),
                        GaugeRange(
                            startValue: 40,
                            endValue: 60,
                            color: const Color(0xFF00AB47),
                            label: S.of(context).tuner_status_in_tune,
                            sizeUnit: GaugeSizeUnit.factor,
                            labelStyle: GaugeTextStyle(
                                fontFamily: 'Times',
                                fontSize: 20,
                                color: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .color),
                            startWidth: 0.65,
                            endWidth: 0.65),
                        GaugeRange(
                          startValue: 60,
                          endValue: 80,
                          color: const Color(0xFFFFBA00),
                          label: S.of(context).tuner_status_high,
                          labelStyle: GaugeTextStyle(
                              fontFamily: 'Times',
                              fontSize: 20,
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .color),
                          startWidth: 0.65,
                          endWidth: 0.65,
                          sizeUnit: GaugeSizeUnit.factor,
                        ),
                        GaugeRange(
                          startValue: 80,
                          endValue: 99,
                          color: const Color(0xFFFE2A25),
                          label: S.of(context).tuner_status_too_high,
                          labelStyle: GaugeTextStyle(
                              fontFamily: 'Times',
                              fontSize: 20,
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .color),
                          sizeUnit: GaugeSizeUnit.factor,
                          startWidth: 0.65,
                          endWidth: 0.65,
                        ),
                      ],
                      pointers: <GaugePointer>[
                        MarkerPointer(
                            value: dialValue,
                            color: note == "-"
                                ? Colors.transparent
                                : const Color(0xFFDF5F2D),
                            enableAnimation: false,
                            animationDuration: 1500,
                            markerOffset: 0.71,
                            offsetUnit: GaugeSizeUnit.factor,
                            markerType: MarkerType.triangle,
                            markerHeight: 10,
                            markerWidth: 15)
                      ],
                      annotations: <GaugeAnnotation>[
                        GaugeAnnotation(
                            angle: 270,
                            positionFactor: 0.025,
                            widget: Text(
                              note,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 40,
                              ),
                            ))
                      ])
                ],
              ),
              SizedBox(
                height: 40,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (!_isTunerOn)
                      FloatingActionButton.small(
                        heroTag: 'tunerPitchDecrease',
                        onPressed: () => decreasePitch(),
                        child: const Icon(Icons.remove),
                      ),
                    const SizedBox(width: 5),
                    Text(
                      pitch.toString().substring(0, 3),
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                      ),
                    ),
                    const SizedBox(width: 5),
                    if (!_isTunerOn)
                      FloatingActionButton.small(
                        heroTag: 'tunerPitchIncrease',
                        onPressed: () => increasePitch(),
                        child: const Icon(Icons.add),
                      ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
                child: Center(
                  child: Text(
                    S.of(context).tuner_pitch_txt,
                    style: Theme.of(context).textTheme.titleSmall,
                  ),
                ),
              ),
            ],
          ),
        );

        /* Column(
          children: [
            Column(
              children: [
                Icon(Icons.add,
                    size: 30, color: tooHigh ? Colors.red : Colors.grey),
                Icon(Icons.arrow_drop_up,
                    size: 100, color: high ? Colors.red : Colors.grey),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    child: Divider(
                  color: Theme.of(context).colorScheme.primary,
                )),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 110,
                    height: 110,
                    child: Align(
                      alignment: Alignment.center,
                      child: FittedBox(
                        child: Text(
                          note,
                          style: TextStyle(
                              fontSize: 100,
                              color: tuned
                                  ? Colors.greenAccent
                                  : Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      ?.color),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                    child: Divider(
                  color: Theme.of(context).colorScheme.primary,
                )),
              ],
            ),
            Column(
              children: [
                Icon(Icons.arrow_drop_down,
                    size: 100, color: low ? Colors.red : Colors.grey),
                Icon(Icons.add,
                    size: 30, color: tooLow ? Colors.red : Colors.grey),
              ],
            ),
          ],
        ); */
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [buildTuningIndicators()],
      )),
      Positioned(
          bottom: 8,
          right: 8,
          child: FloatingActionButton(
            backgroundColor: _isTunerOn
                ? Colors.red
                : Theme.of(context).floatingActionButtonTheme.backgroundColor,
            onPressed: () {
              getIt<TunerBloc>().add(ToggleTunerEvent());
              if (_isTunerOn) {
                note = "-";
              }
              getIt<TunerBloc>().add(const UpdateNoteTunerEvent(
                  note: "-", pitchStatus: TuningStatus.undefined));
              setState(() {
                _isTunerOn = !_isTunerOn;
              });
            },
            child: const Icon(
              Icons.power_settings_new,
            ),
          ))
    ]);
  }
}
