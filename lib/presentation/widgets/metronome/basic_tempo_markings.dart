import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/generated/l10n.dart';
import 'package:practice_suite/logic/blocs/language/language_bloc.dart';
import 'package:practice_suite/logic/blocs/metronome/metronome_bloc.dart';
import 'package:practice_suite/services/locator_service.dart';

import '../../../data/models/tempo_marking.dart';

class BasicTempoMarkings extends StatefulWidget {
  const BasicTempoMarkings({Key? key}) : super(key: key);

  @override
  State<BasicTempoMarkings> createState() => _BasicTempoMarkingsState();
}

class _BasicTempoMarkingsState extends State<BasicTempoMarkings> {
  late List<TempoMarking> markings;

  @override
  void initState() {
    markings = initTempoMarkings();
    super.initState();
  }

  List<TempoMarking> initTempoMarkings() {
    return [
      TempoMarking(
          marking: "Larghissimo",
          translation: getIt<S>().metronome_larghissimo,
          tempoRange: "0-24 bpm",
          tempo: 24),
      TempoMarking(
          marking: "Grave",
          translation: getIt<S>().metronome_grave,
          tempoRange: "25-45 bpm",
          tempo: 25),
      TempoMarking(
          marking: "Largo",
          translation: getIt<S>().metronome_largo,
          tempoRange: "40-60 bpm",
          tempo: 40),
      TempoMarking(
          marking: "Lento",
          translation: getIt<S>().metronome_lento,
          tempoRange: "45-60 bpm",
          tempo: 45),
      TempoMarking(
          marking: "Larghetto",
          translation: getIt<S>().metronome_larghetto,
          tempoRange: "60-66 bpm",
          tempo: 60),
      TempoMarking(
          marking: "Adagio",
          translation: getIt<S>().metronome_adagio,
          tempoRange: "66-76 bpm",
          tempo: 66),
      TempoMarking(
          marking: "Adagietto",
          translation: getIt<S>().metronome_adagietto,
          tempoRange: "72-76 bpm",
          tempo: 72),
      TempoMarking(
          marking: "Andante",
          translation: getIt<S>().metronome_andante,
          tempoRange: "76-108 bpm",
          tempo: 76),
      TempoMarking(
          marking: "Andantino",
          translation: getIt<S>().metronome_andantino,
          tempoRange: "80-108 bpm",
          tempo: 80),
      TempoMarking(
          marking: "Marcia moderato",
          translation: getIt<S>().metronome_marcia_moderato,
          tempoRange: "83-85 bpm",
          tempo: 83),
      TempoMarking(
          marking: "Andante Moderato",
          translation: getIt<S>().metronome_andante_moderato,
          tempoRange: "92-112 bpm",
          tempo: 92),
      TempoMarking(
          marking: "Moderato",
          translation: getIt<S>().metronome_moderato,
          tempoRange: "108-120 bpm",
          tempo: 108),
      TempoMarking(
          marking: "Allegretto",
          translation: getIt<S>().metronome_allegretto,
          tempoRange: "112-120 bpm",
          tempo: 112),
      TempoMarking(
          marking: "Allegro moderato",
          translation: getIt<S>().metronome_allegro_moderato,
          tempoRange: "116-120 bpm",
          tempo: 116),
      TempoMarking(
          marking: "Allegro",
          translation: getIt<S>().metronome_allegro,
          tempoRange: "120-168 bpm",
          tempo: 120),
      TempoMarking(
          marking: "Vivace",
          translation: getIt<S>().metronome_vivace,
          tempoRange: "168-176 bpm",
          tempo: 168),
      TempoMarking(
          marking: "Vivacissimo",
          translation: getIt<S>().metronome_vivacissimo,
          tempoRange: "172-176 bpm",
          tempo: 172),
      TempoMarking(
          marking: "Allegro vivace",
          translation: getIt<S>().metronome_allegro_vivace,
          tempoRange: "172-176 bpm",
          tempo: 172),
      TempoMarking(
          marking: "Presto",
          translation: getIt<S>().metronome_presto,
          tempoRange: "168-200 bpm",
          tempo: 168),
      TempoMarking(
          marking: "Prestissimo",
          translation: getIt<S>().metronome_prestissimo,
          tempoRange: "200 bpm and over",
          tempo: 200),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(S.of(context).metronome_markings_title,
            style: Theme.of(context).textTheme.displayLarge),
        const SizedBox(height: 5),
        Expanded(
          child: BlocBuilder<LanguageBloc, LanguageState>(
            bloc: getIt<LanguageBloc>(),
            builder: (context, state) {
              if (state is ChangeLanguageState) {
                markings = initTempoMarkings();
              }
              return ListView(
                children: markings
                    .map((marking) => InkWell(
                          onTap: () {
                            getIt<MetronomeBloc>().add(
                                ChangeTempoMetronomeEvent(
                                    givenTempo: marking.tempo));
                          },
                          child: ListTile(
                            visualDensity: VisualDensity.compact,
                            titleTextStyle:
                                Theme.of(context).textTheme.bodyMedium,
                            subtitleTextStyle:
                                Theme.of(context).textTheme.bodySmall,
                            title: Text(marking.marking),
                            subtitle: Text(marking.translation),
                            contentPadding:
                                const EdgeInsets.only(left: 10, right: 10),
                            trailing: Text(marking.tempoRange,
                                style: Theme.of(context)
                                    .textTheme
                                    .labelLarge
                                    ?.copyWith(
                                        color:
                                            Theme.of(context).highlightColor)),
                          ),
                        ))
                    .toList(),
              );
            },
          ),
        ),
      ],
    );
  }
}
