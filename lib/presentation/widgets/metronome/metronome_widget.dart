import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_arc_text/flutter_arc_text.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/logic/blocs/metronome/metronome_bloc.dart';
import 'package:practice_suite/presentation/widgets/metronome/sound_picker.dart';
import 'package:practice_suite/services/locator_service.dart';
import 'package:practice_suite/services/metronome_service.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

import '../../../generated/l10n.dart';

class MetronomeWidget extends StatefulWidget {
  const MetronomeWidget({Key? key}) : super(key: key);

  @override
  MetronomeWidgetState createState() => MetronomeWidgetState();
}

class MetronomeWidgetState extends State<MetronomeWidget> {
  Timer? timer;
  late int tempo;
  bool _isMetronomeOn = false;
  late MetronomeBloc _bloc;
  Notes notes = Notes.fourth;
  bool _isFourthNote = true;
  bool _isEighthNote = false;
  bool _isTripletNote = false;
  Stopwatch stopwatch = Stopwatch();

  @override
  void initState() {
    super.initState();
    WakelockPlus.enable();
    _bloc = getIt<MetronomeBloc>();
    _bloc.add(InitMetronomeEvent());
    tempo = _bloc.tempo;
    notes = _bloc.note;
    _selectNotes();
  }

  @override
  void dispose() {
    WakelockPlus.disable();
    _bloc.add(DisposeMetronomeEvent());
    _isMetronomeOn = false;
    _disposeStopwatch();
    super.dispose();
  }

  void _disposeStopwatch() {
    if (stopwatch.isRunning) {
      stopwatch.stop();
      stopwatch.reset();
    }
  }

  void _selectNotes() {
    switch (notes) {
      case Notes.fourth:
        _isFourthNote = true;
        _isEighthNote = false;
        _isTripletNote = false;
        break;
      case Notes.eighth:
        _isFourthNote = false;
        _isEighthNote = true;
        _isTripletNote = false;
        break;
      case Notes.triplet:
        _isFourthNote = false;
        _isEighthNote = false;
        _isTripletNote = true;
        break;
      default:
        _isFourthNote = true;
        _isEighthNote = false;
        _isTripletNote = false;
        break;
    }
    getIt<MetronomeBloc>().add(ChangeNotesEvent(note: notes));
    getIt<MetronomeBloc>().add(ChangeMetronomeSoundMetronomeEvent(soundName: _bloc.normalSound, accentSoundName: _bloc.accentSound));
  }

  Widget _buildTapToSetTempoButton() {
    return Stack(
      children: [
        Positioned(
          left: 24,
          bottom: 24,
          child: RotatedBox(
              quarterTurns: 3,
              child: ArcText(
                placement: Placement.inside,
                text: getIt<S>().metronome_tap_to_set_tempo,
                radius: 55,
                textStyle: Theme.of(context).textTheme.displayLarge!,
                startAngleAlignment: StartAngleAlignment.center,
              )),
        ),
        FloatingActionButton.small(
            heroTag: 'tapToSetTempoButton',
            onPressed: () {
              if (!stopwatch.isRunning) {
                stopwatch.start();
              } else {
                stopwatch.stop();

                int tappedTempo =
                    (60000 / stopwatch.elapsedMilliseconds).ceil();
                int tempoWithLimits = tappedTempo >= 230
                    ? 230
                    : tappedTempo <= 24
                        ? 24
                        : tappedTempo;
                getIt<MetronomeBloc>().add(
                    ChangeTempoMetronomeEvent(givenTempo: tempoWithLimits));

                stopwatch.reset();
                stopwatch.start();
              }
            },
            child: const Icon(Icons.watch_later_outlined)),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MetronomeBloc, MetronomeState>(
      bloc: _bloc,
      builder: (context, state) {
        if (state is ToggleOnOffMetronomeState) {
          _isMetronomeOn = state.isMetronomeOn;
        }
        if (state is MetronomeTempoState) {
          tempo = state.tempo;
        }

        return Stack(children: [
          Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ChoiceChip(
                        selected: _isFourthNote,
                        onSelected: (value) {
                          setState(() {
                            notes = Notes.fourth;
                            _selectNotes();
                          });
                        },
                        label: Image.asset(
                          "assets/icons/fourth_icon.png",
                          height: 20,
                          width: 50,
                          color: _isFourthNote
                              ? Theme.of(context)
                                  .floatingActionButtonTheme
                                  .foregroundColor
                              : Theme.of(context).textTheme.bodyMedium!.color,
                        ),
                      ),
                      const SizedBox(width: 10),
                      ChoiceChip(
                        selected: _isEighthNote,
                        onSelected: (value) {
                          setState(() {
                            notes = Notes.eighth;
                            _selectNotes();
                          });
                        },
                        label: Image.asset(
                          "assets/icons/eighth_icon.png",
                          height: 20,
                          width: 50,
                          color: _isEighthNote
                              ? Theme.of(context)
                                  .floatingActionButtonTheme
                                  .foregroundColor
                              : Theme.of(context).textTheme.bodyMedium!.color,
                        ),
                      ),
                      const SizedBox(width: 10),
                      ChoiceChip(
                        selected: _isTripletNote,
                        onSelected: (value) {
                          setState(() {
                            notes = Notes.triplet;
                            _selectNotes();
                          });
                        },
                        label: Image.asset(
                          "assets/icons/triplet_icon.png",
                          height: 20,
                          width: 50,
                          color: _isTripletNote
                              ? Theme.of(context)
                                  .floatingActionButtonTheme
                                  .foregroundColor
                              : Theme.of(context).textTheme.bodyMedium!.color,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 15),
                  SoundPicker(isAccentSoundEnabled: !_isFourthNote),
                  const SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          GestureDetector(
                              onTapDown: (TapDownDetails details) {
                                timer = Timer.periodic(
                                    const Duration(milliseconds: 100), (t) {
                                  _bloc.add(IncreaseTempoMetronomeEvent());
                                });
                              },
                              onTapUp: (TapUpDetails details) {
                                timer?.cancel();
                              },
                              onTapCancel: () {
                                timer?.cancel();
                              },
                              child: FloatingActionButton.small(
                                  heroTag: 'plusButton',
                                  onPressed: () {
                                    _bloc.add(IncreaseTempoMetronomeEvent());
                                  },
                                  child: const Icon(Icons.add))),
                          const SizedBox(height: 15),
                          Text(tempo.toString(),
                              style: const TextStyle(fontSize: 20)),
                          const SizedBox(height: 15),
                          GestureDetector(
                              onTapDown: (TapDownDetails details) {
                                timer = Timer.periodic(
                                    const Duration(milliseconds: 100), (t) {
                                  _bloc.add(DecreaseTempoMetronomeEvent());
                                });
                              },
                              onTapUp: (TapUpDetails details) {
                                timer?.cancel();
                              },
                              onTapCancel: () {
                                timer?.cancel();
                              },
                              child: FloatingActionButton.small(
                                  heroTag: 'minusButton',
                                  onPressed: () {
                                    _bloc.add(DecreaseTempoMetronomeEvent());
                                  },
                                  child: const Icon(Icons.remove))),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
              bottom: 16,
              right: 16,
              child: FloatingActionButton(
                backgroundColor: _isMetronomeOn
                    ? Colors.red
                    : Theme.of(context).floatingActionButtonTheme.backgroundColor,
                onPressed: () {
                  _disposeStopwatch();
                  _bloc.add(ToggleOnOffMetronomeEvent());
                },
                child: const Icon(Icons.power_settings_new),
              )),
          Positioned(
            bottom: 87,
            left: 57,
            child: _buildTapToSetTempoButton(),
          ),
        ]);
      },
    );
  }
}
