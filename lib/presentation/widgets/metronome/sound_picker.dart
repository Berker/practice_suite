import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/presentation/widgets/shared/pt_container.dart';
import 'package:practice_suite/services/metronome_service.dart';

import '../../../generated/l10n.dart';
import '../../../logic/blocs/metronome/metronome_bloc.dart';
import '../../../services/locator_service.dart';

class SoundPicker extends StatefulWidget {
  final bool isAccentSoundEnabled;

  const SoundPicker({Key? key, required this.isAccentSoundEnabled})
      : super(key: key);

  @override
  _SoundPickerState createState() => _SoundPickerState();
}

class _SoundPickerState extends State<SoundPicker> {
  String normalSound = getIt<MetronomeService>().normalSound;
  String accentSound = getIt<MetronomeService>().accentSound;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MetronomeBloc, MetronomeState>(
      builder: (context, state) {
        if (state is MetronomeSoundState) {
          normalSound = state.normalSound;
          accentSound = state.accentSound;
        }
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 30,
              width: 120,
              child: PTContainer(
                child: Center(
                  child: DropdownButton(icon: const Icon(Icons.music_note),
                      underline: Container(),
                      borderRadius: BorderRadius.circular(5.0),
                      value: normalSound,
                      items: metronomeSounds.map((String sound) {
                        return DropdownMenuItem(
                            value: sound,
                            alignment: Alignment.center,
                            child: Text(S.of(context).metronome_sound_name + sound.substring(5, sound.length - 4)));
                      }).toList(),
                      onChanged: (selected) {
                        setState(() {
                          normalSound = selected.toString();
                          getIt<MetronomeBloc>().add(
                              ChangeMetronomeSoundMetronomeEvent(
                                  soundName: normalSound,
                                  accentSoundName: accentSound));
                        });
                      }),
                ),
              ),
            ),
            if (widget.isAccentSoundEnabled) ...[
              const SizedBox(width: 8),
              SizedBox(
                height: 30,
                width: 120,
                child: PTContainer(
                  child: Center(
                    child: DropdownButton(
                        icon: const Icon(Icons.music_note_outlined),
                        underline: Container(),
                        borderRadius: BorderRadius.circular(5.0),
                        value: accentSound,
                        items: metronomeSounds.map((String sound) {
                          return DropdownMenuItem(
                              value: sound,
                              alignment: Alignment.center,
                              child:
                              Text(S.of(context).metronome_sound_name + sound.substring(5, sound.length - 4)));
                        }).toList(),
                        onChanged: (selected) {
                          setState(() {
                            accentSound = selected.toString();
                            getIt<MetronomeBloc>().add(
                                ChangeMetronomeSoundMetronomeEvent(
                                    soundName: normalSound,
                                    accentSoundName: accentSound));
                          });
                        }),
                  ),
                ),
              ),
            ],
          ],
        );
      },
    );
  }
}
