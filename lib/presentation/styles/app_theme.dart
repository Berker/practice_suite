import 'package:flutter/material.dart';
import 'package:practice_suite/data/api/shared_prefs_api.dart';

/// Theming class that provides custom application themes.
class AppTheme {
  const AppTheme._();

  static late final ThemeData light;
  static late final ThemeData dark;

  static void initialize() {
    light = lightTheme();
    dark = darkTheme();
  }

  static ThemeMode initialTheme = SharedPrefsApi.getThemeMode();

  /// Generated light theme for the app.
  static ThemeData lightTheme() {
    final theme = ThemeData.light();

    return theme.copyWith(
      brightness: Brightness.light,
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
        elevation: 15,
        backgroundColor: Colors.blue,
        foregroundColor: Colors.black,
      ),
      chipTheme: ChipThemeData(
        selectedColor: Colors.blue,
        showCheckmark: false,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
          side: const BorderSide(
            color: Colors.blue,
          ),
        ),
      ),
      appBarTheme: const AppBarTheme(
          backgroundColor: Colors.transparent,
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.black54)),
      textTheme: theme.textTheme.copyWith(
        // Title from new practice log entry
        displayLarge: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.bold,
          color: Colors.blue,
        ),
        titleLarge: const TextStyle(
          fontSize: 14.5,
          fontWeight: FontWeight.w600,
          color: Colors.white,
          letterSpacing: 0.1,
        ),
        titleSmall: theme.textTheme.bodyLarge?.copyWith(
          fontSize: 12,
          fontWeight: FontWeight.w400,
          color: Colors.black,
          letterSpacing: 0.1,
        ),
        bodyLarge: theme.textTheme.bodyLarge?.copyWith(
          fontSize: 12,
          fontWeight: FontWeight.w400,
          color: Colors.black54,
          letterSpacing: 0.1,
        ),
      ),
      cardTheme: CardTheme(
        elevation: 8,
        margin: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
      ),
      colorScheme: theme.colorScheme.copyWith(
        primary: Colors.blue,
      ),
      highlightColor: Colors.blue,
    );
  }

  /// Generated dark theme for the app.
  static ThemeData darkTheme() {
    final theme = ThemeData.dark();

    return theme.copyWith(
      brightness: Brightness.dark,
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
        elevation: 15,
        backgroundColor: Colors.greenAccent,
        foregroundColor: Colors.black,
      ),
      chipTheme: ChipThemeData(
        selectedColor: Colors.greenAccent,
        showCheckmark: false,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
          side: const BorderSide(
            color: Colors.greenAccent,
          ),
        ),
      ),
      appBarTheme: const AppBarTheme(
          backgroundColor: Colors.transparent,
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.white70)),
      textTheme: theme.textTheme.copyWith(
        // Title from new practice log entry
        displayLarge: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.bold,
          color: Colors.greenAccent,
        ),
        titleLarge: const TextStyle(
          fontSize: 14.5,
          fontWeight: FontWeight.w600,
          color: Colors.black,
          letterSpacing: 0.1,
        ),
        titleSmall: theme.textTheme.bodyLarge?.copyWith(
          fontSize: 12,
          fontWeight: FontWeight.w400,
          color: Colors.white,
          letterSpacing: 0.1,
        ),
        bodyLarge: theme.textTheme.bodyLarge?.copyWith(
          fontSize: 12,
          fontWeight: FontWeight.w400,
          color: Colors.white54,
          letterSpacing: 0.1,
        ),
      ),
      cardTheme: CardTheme(
        elevation: 0,
        margin: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
      ),
      highlightColor: Colors.greenAccent,
      colorScheme: theme.colorScheme.copyWith(
        primary: Colors.greenAccent,
      ),
    );
  }
}
