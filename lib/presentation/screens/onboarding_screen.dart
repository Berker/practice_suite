import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_onboarding_slider/flutter_onboarding_slider.dart';
import 'package:practice_suite/data/api/shared_prefs_api.dart';
import 'package:practice_suite/main.dart';

import '../../generated/l10n.dart';

class OnboardingScreen extends StatelessWidget {
  const OnboardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double sizedBoxHeight = screenHeight < 900 ? 260 : 400;

    return CupertinoApp(
      home: OnBoardingSlider(
        totalPage: 6,
        headerBackgroundColor: Theme.of(context).scaffoldBackgroundColor,
        speed: 1.8,
        finishButtonText: S.of(context).done,
        skipTextButton: Text(S.of(context).onboarding_skip),
        onFinish: () {
          SharedPrefsApi.setFirstLaunch();
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => const MyApp()));
        },
        pageBodies: [
          Container(
            padding: const EdgeInsets.only(left: 30, right: 30, bottom: 40),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: sizedBoxHeight,
                ),
                const Spacer(),
                Text(
                  S.of(context).onboarding_swiss_army_knife,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  S.of(context).practice_suite_intro_name,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  S.of(context).onboarding_app_intro,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Theme.of(context).hintColor,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 40, right: 40, bottom: 40),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: sizedBoxHeight,
                ),
                const Spacer(),
                Text(
                  S.of(context).onboarding_log_title,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  S.of(context).onboarding_log_desc,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Theme.of(context).hintColor,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 40, right: 40, bottom: 40),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: sizedBoxHeight,
                ),
                const Spacer(),
                Text(
                  S.of(context).navBarMetronome,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  S.of(context).onboarding_metronome_desc,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Theme.of(context).hintColor,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 40, right: 40, bottom: 40),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: sizedBoxHeight,
                ),
                Spacer(),
                Text(
                  S.of(context).navBarTuner,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  S.of(context).onboarding_tuner_desc,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Theme.of(context).hintColor,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 40, right: 40, bottom: 40),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: sizedBoxHeight,
                ),
                const Spacer(),
                Text(
                  S.of(context).navBarRecording,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  S.of(context).onboarding_recorder_desc,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Theme.of(context).hintColor,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 40, right: 40, bottom: 40),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: sizedBoxHeight,
                ),
                const Spacer(),
                Text(
                  S.of(context).statistics_title,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  S.of(context).onboarding_stats_desc,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Theme.of(context).hintColor,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
              ],
            ),
          ),
        ],
        pageBackgroundColor: Theme.of(context).scaffoldBackgroundColor,
        controllerColor: Theme.of(context).colorScheme.primary,
        finishButtonStyle: FinishButtonStyle(
            backgroundColor: Theme.of(context).colorScheme.primary),
        background: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: SizedBox(
              height: sizedBoxHeight,
              width: screenWidth,
              child: Image.asset(
                'assets/icons/p_t_icon_rounded.png',
                fit: BoxFit.contain,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: SizedBox(
                height: sizedBoxHeight,
                width: screenWidth,
                child: Image.asset(
                  'assets/onboarding/slide_1.png',
                  fit: BoxFit.contain,
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: SizedBox(
                height: sizedBoxHeight,
                width: screenWidth,
                child: Image.asset(
                  'assets/onboarding/slide_2.png',
                  fit: BoxFit.contain,
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: SizedBox(
                height: sizedBoxHeight,
                width: screenWidth,
                child: Image.asset(
                  'assets/onboarding/slide_3.png',
                  fit: BoxFit.cover,
                )),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: SizedBox(
                  height: sizedBoxHeight,
                  width: screenWidth,
                  child: Image.asset(
                    'assets/onboarding/slide_4.png',
                    fit: BoxFit.contain,
                  ))),
          Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: SizedBox(
                  height: sizedBoxHeight,
                  width: screenWidth,
                  child: Image.asset(
                    'assets/onboarding/slide_5.png',
                    fit: BoxFit.contain,
                  ))),
        ],
      ),
    );
  }
}
