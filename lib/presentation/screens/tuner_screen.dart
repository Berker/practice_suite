import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/logic/blocs/recording_log/recording_log_bloc.dart';
import 'package:practice_suite/presentation/widgets/tuner/tuner_widget.dart';

import '../../generated/l10n.dart';
import '../../services/locator_service.dart';
import '../widgets/shared/pt_snackbar.dart';

class TunerScreen extends StatelessWidget {
  const TunerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<RecordingLogBloc, RecordingLogState>(
      listener: (context, state) {
        if (state is RecordingLogFailureState) {
          PTSnackBar.showSnackBar(
              context, getIt<S>().recording_log_check_permissions);
        }
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(
                style: BorderStyle.solid,
                color: Theme.of(context).colorScheme.primary),
          ),
          child: const Padding(
            padding: EdgeInsets.all(8.0),
            child: TunerWidget(),
          ),
        ),
      ),
    );
  }
}
