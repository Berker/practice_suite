import 'package:flutter/material.dart';
import 'package:practice_suite/presentation/widgets/metronome/basic_tempo_markings.dart';
import 'package:practice_suite/presentation/widgets/metronome/metronome_widget.dart';

class MetronomeScreen extends StatelessWidget {
  const MetronomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                border: Border.all(
                    style: BorderStyle.solid,
                    color: Theme.of(context).colorScheme.primary),
              ),
              child: const BasicTempoMarkings(),
            ),
          ),
        ),
        const SizedBox(height: 8,),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                border: Border.all(
                    style: BorderStyle.solid,
                    color: Theme.of(context).colorScheme.primary),
              ),
              child: const MetronomeWidget(),
              ),
            ),
          ),
      ],
    );
  }
}
