import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/data/models/statistic_charts.dart';
import 'package:practice_suite/logic/blocs/practice_log/practice_log_bloc.dart';
import 'package:practice_suite/logic/blocs/statistics/statistics_bloc.dart';
import 'package:practice_suite/presentation/widgets/shared/pt_container.dart';

import '../../generated/l10n.dart';
import '../../services/locator_service.dart';

class StatisticsScreen extends StatefulWidget {
  const StatisticsScreen({Key? key}) : super(key: key);

  @override
  State<StatisticsScreen> createState() => _StatisticsScreenState();
}

class _StatisticsScreenState extends State<StatisticsScreen> {
  List<Widget> _statistics = [];
  StatisticCharts _charts = StatisticCharts(
      thisWeek: const Placeholder(),
      lastWeek: const Placeholder(),
      thisYear: const Placeholder(),
      thisMonth: const Placeholder());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text(
                S.of(context).statistics_title,
                style: Theme.of(context).textTheme.headlineSmall,
            )),
        body: BlocBuilder<PracticeLogBloc, PracticeLogState>(
            bloc: getIt<PracticeLogBloc>(),
            builder: (context, state) {
              if (state is PracticeLogEntries) {
                getIt<StatisticsBloc>().add(FetchAndBuildStatisticsEvent());
              }

              return BlocBuilder<StatisticsBloc, StatisticsState>(
                bloc: getIt<StatisticsBloc>(),
                builder: (context, state) {
                  if (state is FetchAndBuildSuccessState) {
                    _statistics = state.statistics;
                    _charts = state.charts;
                  }
                  return _statistics.isEmpty
                      ? Center(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 30.0),
                            child: Text(
                              S.of(context).statistics_empty_warning,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )
                      : SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: double.infinity,
                                height: 150,
                                child: Row(
                                  children: [
                                    Expanded(child: _statistics[0]),
                                    Expanded(child: _statistics[1]),
                                    Expanded(child: _statistics[2]),
                                  ],
                                ),
                              ),
                              const SizedBox(height: 8),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Expanded(
                                      child:
                                          PTContainer(child: _charts.thisWeek)),
                                  const SizedBox(height: 8),
                                  Expanded(
                                      child:
                                          PTContainer(child: _charts.lastWeek)),
                                ],
                              ),
                              const SizedBox(height: 8),
                              PTContainer(child: _charts.thisYear),
                            ],
                          ),
                        );
                },
              );
            }));
  }
}
