import 'package:flutter/material.dart';
import 'package:practice_suite/presentation/widgets/recording/recording_log_entry_list.dart';

import '../widgets/recording/recorder_sheet.dart';

class RecordingLogScreen extends StatelessWidget {
  const RecordingLogScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(
                      style: BorderStyle.solid,
                      color: Theme.of(context).colorScheme.primary),
                ),
                child: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: RecordingLogEntrylist(),
                ),
              ),
            ),
          )
        ],
      ),
      Positioned(
          right: 25,
          bottom: 17,
          child: FloatingActionButton(
            onPressed: () {
              showModalBottomSheet(
                isScrollControlled: true,
                backgroundColor: Colors.transparent,
                context: context,
                builder: (_) {
                  return const RecorderSheet();
                },
              );
            },
            child: const Icon(Icons.mic_rounded),
          ))
    ]);
  }
}
