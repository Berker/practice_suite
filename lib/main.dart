import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:path_provider/path_provider.dart';
import 'package:practice_suite/data/api/license_api.dart';
import 'package:practice_suite/data/api/sound_api.dart';
import 'package:practice_suite/logic/blocs/app_theme/app_theme_bloc.dart';
import 'package:practice_suite/logic/blocs/metronome/metronome_bloc.dart';
import 'package:practice_suite/logic/blocs/practice_log/practice_log_bloc.dart';
import 'package:practice_suite/logic/blocs/recording_log/recording_log_bloc.dart';
import 'package:practice_suite/presentation/screens/metronome_screen.dart';
import 'package:practice_suite/presentation/screens/onboarding_screen.dart';
import 'package:practice_suite/presentation/screens/practice_log_screen.dart';
import 'package:practice_suite/presentation/screens/recording_log_screen.dart';
import 'package:practice_suite/presentation/screens/tuner_screen.dart';
import 'package:practice_suite/presentation/styles/app_theme.dart';
import 'package:practice_suite/presentation/widgets/shared/app_bar_pt.dart';
import 'package:practice_suite/services/locator_service.dart';

import 'data/api/shared_prefs_api.dart';
import 'generated/l10n.dart';
import 'logic/blocs/bloc_observer_pt.dart';
import 'logic/blocs/language/language_bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPrefsApi.init();
  AppTheme.initialize();
  Bloc.observer = BlocObserverPT();
  setupLocatorService();
  await setFileSavePath();
  LicenseApi().initMyLicense();
  runApp(const PracticeTools());
}

Future<void> setFileSavePath() async {
  Directory? dir = Platform.isIOS
      ? await getApplicationDocumentsDirectory()
      : await getExternalStorageDirectory();
  getIt<SoundApi>().pathToSaveAudio = "${dir?.path}/Practice_Suite/Recordings/";
  debugPrint("File save path: ${getIt<SoundApi>().pathToSaveAudio}");
}

class PracticeTools extends StatefulWidget {
  const PracticeTools({Key? key}) : super(key: key);

  @override
  _PracticeToolsState createState() => _PracticeToolsState();
}

class _PracticeToolsState extends State<PracticeTools> {
  final ThemeMode _initialTheme = AppTheme.initialTheme;
  final Locale _initialLanguage = SharedPrefsApi.getLanguage();
  final bool _isFirstLaunch = SharedPrefsApi.getFirstLaunchInfo();

  ThemeMode appTheme(AppThemeState state) {
    if (state is ChangeThemeState) {
      return state.themeMode;
    } else {
      return _initialTheme;
    }
  }

  Locale appLanguage(LanguageState state) {
    if (state is ChangeLanguageState) {
      return state.appLanguage;
    } else {
      return _initialLanguage;
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => getIt<AppThemeBloc>()),
          BlocProvider(create: (context) => getIt<PracticeLogBloc>()),
          BlocProvider(create: (context) => getIt<RecordingLogBloc>()),
          BlocProvider(create: (context) => getIt<MetronomeBloc>()),
        ],
        child: BlocBuilder<LanguageBloc, LanguageState>(
          bloc: getIt<LanguageBloc>(),
          builder: (context, languageState) {
            return BlocBuilder<AppThemeBloc, AppThemeState>(
              builder: (context, themeState) {
                return MaterialApp(
                  debugShowCheckedModeBanner: false,
                  title: 'Practice Suite',
                  themeMode: appTheme(themeState),
                  theme: AppTheme.light,
                  darkTheme: AppTheme.dark,
                  localizationsDelegates: const [
                    S.delegate,
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate,
                  ],
                  locale: appLanguage(languageState),
                  supportedLocales: S.delegate.supportedLocales,
                  home: _isFirstLaunch ? OnboardingScreen() : MyApp(),
                );
              },
            );
          },
        ));
  }
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final List<Widget> _pages = [
    const PracticeLogScreen(),
    const MetronomeScreen(),
    const TunerScreen(),
    const RecordingLogScreen(),
  ];
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBarPT(),
      body: _pages[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor:
            Theme.of(context).floatingActionButtonTheme.backgroundColor,
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
              icon: const Icon(Icons.note_alt), label: S.of(context).navBarlog),
          BottomNavigationBarItem(
              icon: const Icon(Icons.speed),
              label: S.of(context).navBarMetronome),
          BottomNavigationBarItem(
              icon: const Icon(Icons.hearing),
              label: S.of(context).navBarTuner),
          BottomNavigationBarItem(
              icon: const Icon(Icons.mic),
              label: S.of(context).navBarRecording),
        ],
      ),
    );
  }
}
