// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a tr_TR locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'tr_TR';

  static String m0(day, hour, minute) => "${day} G, ${hour} S, ${minute} D";

  static String m1(hour, minute) => "${hour} S, ${minute} D";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "app_name": MessageLookupByLibrary.simpleMessage("Practice Suite"),
        "areYouSure": MessageLookupByLibrary.simpleMessage("Emin misin?"),
        "cancel": MessageLookupByLibrary.simpleMessage("İptal"),
        "date": MessageLookupByLibrary.simpleMessage("Tarih"),
        "days": MessageLookupByLibrary.simpleMessage("Günler:"),
        "description":
            MessageLookupByLibrary.simpleMessage("Açıklama (opsiyonel)"),
        "done": MessageLookupByLibrary.simpleMessage("Tamam"),
        "duration":
            MessageLookupByLibrary.simpleMessage("Süre (dakika olarak)"),
        "durationValidation":
            MessageLookupByLibrary.simpleMessage("Lütfen bir süre girin"),
        "duration_amount_validation": MessageLookupByLibrary.simpleMessage(
            "Yalnızca 1 ile 720 arasında bir süreye izin verilir."),
        "duration_format_validation": MessageLookupByLibrary.simpleMessage(
            "Süre rakamlardan oluşmalıdır"),
        "edit": MessageLookupByLibrary.simpleMessage("Düzenle"),
        "editLogEntry":
            MessageLookupByLibrary.simpleMessage("Günlük girdisini düzenle"),
        "entryAddedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Girdi başarıyla eklendi!"),
        "entryRemovedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Girdi başarıyla silindi!"),
        "entryUpdatedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Girdi başarıyla düzenlendi!"),
        "error_message_microphone_denied":
            MessageLookupByLibrary.simpleMessage("Mikrofon izni reddedildi!"),
        "hours": MessageLookupByLibrary.simpleMessage("Saatler:"),
        "metronome_adagietto": MessageLookupByLibrary.simpleMessage(
            "oldukça yavaş, adagio\'dan biraz daha hızlı"),
        "metronome_adagio": MessageLookupByLibrary.simpleMessage("yavaş"),
        "metronome_allegretto": MessageLookupByLibrary.simpleMessage(
            "orta derecede hızlı, allegro\'dan biraz daha yavaş"),
        "metronome_allegro": MessageLookupByLibrary.simpleMessage("hızlı"),
        "metronome_allegro_moderato": MessageLookupByLibrary.simpleMessage(
            "orta derecede hızlı, allegro\'dan biraz daha yavaş"),
        "metronome_allegro_vivace":
            MessageLookupByLibrary.simpleMessage("çok hızlı"),
        "metronome_andante":
            MessageLookupByLibrary.simpleMessage("sakince, yavaşça"),
        "metronome_andante_moderato": MessageLookupByLibrary.simpleMessage(
            "orta derecede sakin, andante\'den daha hızlı"),
        "metronome_andantino": MessageLookupByLibrary.simpleMessage(
            "orta tempo, andante\'den biraz daha hızlı"),
        "metronome_format_validation": MessageLookupByLibrary.simpleMessage(
            "Metronom hızı rakamlardan oluşmalıdır."),
        "metronome_grave": MessageLookupByLibrary.simpleMessage("çok yavaş"),
        "metronome_larghetto": MessageLookupByLibrary.simpleMessage(
            "oldukça yavaş ve geniş, largo\'dan biraz daha hızlı"),
        "metronome_larghissimo": MessageLookupByLibrary.simpleMessage(
            "son derece yavaş, largodan daha yavaş"),
        "metronome_largo":
            MessageLookupByLibrary.simpleMessage("çok yavaş ve geniş"),
        "metronome_lento": MessageLookupByLibrary.simpleMessage("yavaş"),
        "metronome_limit_validation": MessageLookupByLibrary.simpleMessage(
            "Lütfen 24 ile 230 arasında bir hız seçin"),
        "metronome_marcia_moderato":
            MessageLookupByLibrary.simpleMessage("orta, marş temposunda"),
        "metronome_markings_title":
            MessageLookupByLibrary.simpleMessage("temel tempo terimleri"),
        "metronome_moderato":
            MessageLookupByLibrary.simpleMessage("ılımlı, orta"),
        "metronome_prestissimo": MessageLookupByLibrary.simpleMessage(
            "son derece hızlı, presto\'dan daha hızlı"),
        "metronome_presto":
            MessageLookupByLibrary.simpleMessage("çok, çok hızlı"),
        "metronome_sound_name": MessageLookupByLibrary.simpleMessage("Ses"),
        "metronome_tap_to_set_tempo":
            MessageLookupByLibrary.simpleMessage("Tempo ayarı için dokunun"),
        "metronome_triplet_warning": MessageLookupByLibrary.simpleMessage(
            "Cihazınızın sağlığı için triolelerde maksimum hız 197 bpm\'ye düşürülmüştür.\n"),
        "metronome_vivace":
            MessageLookupByLibrary.simpleMessage("hızlı ve canlı"),
        "metronome_vivacissimo": MessageLookupByLibrary.simpleMessage(
            "çok hızlı ve canlı, vivace’den daha hızlı"),
        "minutes": MessageLookupByLibrary.simpleMessage("Dakikalar:"),
        "minutesPracticed":
            MessageLookupByLibrary.simpleMessage("Çalışılan dakikalar"),
        "navBarMetronome": MessageLookupByLibrary.simpleMessage("Metronom"),
        "navBarRecording": MessageLookupByLibrary.simpleMessage("Kayıt"),
        "navBarTuner": MessageLookupByLibrary.simpleMessage("Akort"),
        "navBarlog": MessageLookupByLibrary.simpleMessage("Günlük"),
        "newLogDialogTitle":
            MessageLookupByLibrary.simpleMessage("Yeni günlük girişi"),
        "newRecording": MessageLookupByLibrary.simpleMessage("Yeni kayıt"),
        "newTag": MessageLookupByLibrary.simpleMessage("Yeni etiket"),
        "no": MessageLookupByLibrary.simpleMessage("Hayır"),
        "noLogEntryWarning": MessageLookupByLibrary.simpleMessage(
            "Henüz kayıt yok! Pratik yapma zamanı!"),
        "note_text_a": MessageLookupByLibrary.simpleMessage("LA"),
        "note_text_a_sharp": MessageLookupByLibrary.simpleMessage("LA#"),
        "note_text_b": MessageLookupByLibrary.simpleMessage("Sİ"),
        "note_text_c": MessageLookupByLibrary.simpleMessage("DO"),
        "note_text_c_sharp": MessageLookupByLibrary.simpleMessage("DO#"),
        "note_text_d": MessageLookupByLibrary.simpleMessage("RE"),
        "note_text_d_sharp": MessageLookupByLibrary.simpleMessage("RE#"),
        "note_text_e": MessageLookupByLibrary.simpleMessage("Mİ"),
        "note_text_f": MessageLookupByLibrary.simpleMessage("FA"),
        "note_text_f_sharp": MessageLookupByLibrary.simpleMessage("FA#"),
        "note_text_g": MessageLookupByLibrary.simpleMessage("SOL"),
        "note_text_g_sharp": MessageLookupByLibrary.simpleMessage("SOL#"),
        "onboarding_app_intro": MessageLookupByLibrary.simpleMessage(
            "Practice Suite, her seviyeden müzisyen için mükemmel bir müzik pratik aracıdır.Kapsamlı özellikleri sayesinde ritminizi ve akordunuzu koruyabilir ve ilerlemenizi tek bir kullanışlı uygulamada takip edebilirsiniz."),
        "onboarding_log_desc": MessageLookupByLibrary.simpleMessage(
            "Günlük tutma özelliği ile alıştırma seanslarınızı takip edebilir, hedefler belirleyebilir ve zaman içindeki ilerlemenizi izleyebilirsiniz."),
        "onboarding_log_title":
            MessageLookupByLibrary.simpleMessage("Egzersiz Tutanağı"),
        "onboarding_metronome_desc": MessageLookupByLibrary.simpleMessage(
            "Metronom özelliği, istediğiniz tempoyu ve zaman dilimini ayarlamanıza olanak tanıyarak pratik yaparken ritminizi ve ölçünüzü korumanızı sağlar."),
        "onboarding_recorder_desc": MessageLookupByLibrary.simpleMessage(
            "Kayıt özelliği, alıştırma seanslarınızı kaydetmenize ve tekrar çalmanıza olanak tanır, böylece ilerlemenizi dinleyebilir ve geliştirmeniz gereken alanları belirleyebilirsiniz."),
        "onboarding_skip": MessageLookupByLibrary.simpleMessage("Atla"),
        "onboarding_stats_desc": MessageLookupByLibrary.simpleMessage(
            "Uygulama aynı zamanda ayrıntılı istatistikler sunar, böylece pratik yapmak için ne kadar zaman harcadığınızı ve zaman içinde nasıl geliştiğinizi görebilirsiniz."),
        "onboarding_swiss_army_knife": MessageLookupByLibrary.simpleMessage(
            "Karşınızda müzisyenlerin İsviçre çakısı"),
        "onboarding_tuner_desc": MessageLookupByLibrary.simpleMessage(
            "Akort özelliği, enstrümanınızı doğru bir şekilde akort etmek için cihazınızın mikrofonunu kullanır ve mükemmel perdede çalmanıza yardımcı olur."),
        "optionalDescriptionText":
            MessageLookupByLibrary.simpleMessage("Açıklama yapılmadı"),
        "practice_log_error_empty_tag":
            MessageLookupByLibrary.simpleMessage("Etiket boş bırakılamaz"),
        "practice_log_error_no_tag_selected":
            MessageLookupByLibrary.simpleMessage(
                "Lütfen en az bir etiket ekleyin veya seçin"),
        "practice_log_filter_allEntries":
            MessageLookupByLibrary.simpleMessage("Tüm girdiler"),
        "practice_log_filter_showAllEntries":
            MessageLookupByLibrary.simpleMessage("Tüm girdileri göster"),
        "practice_log_filter_title":
            MessageLookupByLibrary.simpleMessage("Filtre"),
        "practice_suite_intro_name":
            MessageLookupByLibrary.simpleMessage("Practice Suite!"),
        "recording_log_check_permissions": MessageLookupByLibrary.simpleMessage(
            "Lütfen sistem ayarlarından uygulamanın izinlerini kontrol edin"),
        "save": MessageLookupByLibrary.simpleMessage("kaydet"),
        "settings_btn_licences":
            MessageLookupByLibrary.simpleMessage("Lisanslar"),
        "settings_built_with_love":
            MessageLookupByLibrary.simpleMessage("💙 ile kodlandı, "),
        "settings_donation_txt": MessageLookupByLibrary.simpleMessage(
            "Uygulamaların geliştirilmesine sayısız saatler harcanıyor. Uygulamayı beğendiyseniz ve geliştirilmesini desteklemek istiyorsanız, lütfen bağış yapmayı düşünün. Teşekkürler!"),
        "settings_follow_on":
            MessageLookupByLibrary.simpleMessage("Takip için "),
        "settings_language_english":
            MessageLookupByLibrary.simpleMessage("İngilizce"),
        "settings_language_german":
            MessageLookupByLibrary.simpleMessage("Almanca"),
        "settings_language_system":
            MessageLookupByLibrary.simpleMessage("Sistem"),
        "settings_language_turkish":
            MessageLookupByLibrary.simpleMessage("Türkçe"),
        "settings_theme_mode": MessageLookupByLibrary.simpleMessage("Tema"),
        "settings_title_about":
            MessageLookupByLibrary.simpleMessage("Hakkında"),
        "settings_title_donate": MessageLookupByLibrary.simpleMessage("Bağış"),
        "settings_title_email": MessageLookupByLibrary.simpleMessage("E-posta"),
        "settings_title_help": MessageLookupByLibrary.simpleMessage("Yardım"),
        "settings_title_issues":
            MessageLookupByLibrary.simpleMessage("Hatalar"),
        "settings_title_language": MessageLookupByLibrary.simpleMessage("Dil"),
        "settings_title_matrix_room":
            MessageLookupByLibrary.simpleMessage("Matris odası"),
        "settings_title_source": MessageLookupByLibrary.simpleMessage("Kaynak"),
        "settings_title_txt": MessageLookupByLibrary.simpleMessage("Ayarlar"),
        "share_recording_text":
            MessageLookupByLibrary.simpleMessage("Şu kayda göz atın!"),
        "share_recording_title":
            MessageLookupByLibrary.simpleMessage("Kayıt paylaş"),
        "statistics_day_friday": MessageLookupByLibrary.simpleMessage("Cuma"),
        "statistics_day_monday":
            MessageLookupByLibrary.simpleMessage("Pazartesi"),
        "statistics_day_saturday":
            MessageLookupByLibrary.simpleMessage("Cumartesi"),
        "statistics_day_sunday": MessageLookupByLibrary.simpleMessage("Pazar"),
        "statistics_day_thursday":
            MessageLookupByLibrary.simpleMessage("Perşembe"),
        "statistics_day_tuesday": MessageLookupByLibrary.simpleMessage("Salı"),
        "statistics_day_wednesday":
            MessageLookupByLibrary.simpleMessage("Çarşamba"),
        "statistics_days_hours_minutes": m0,
        "statistics_empty_warning": MessageLookupByLibrary.simpleMessage(
            "İstatistikleri görmek için pratik yapın ve günlük girdileri ekleyin!"),
        "statistics_hours_and_minutes": m1,
        "statistics_hours_practiced_last_week":
            MessageLookupByLibrary.simpleMessage("Geçen hafta çalışılan süre"),
        "statistics_hours_practiced_this_week":
            MessageLookupByLibrary.simpleMessage("Bu hafta çalışılan süre"),
        "statistics_month_april": MessageLookupByLibrary.simpleMessage("Nisan"),
        "statistics_month_august":
            MessageLookupByLibrary.simpleMessage("Ağustos"),
        "statistics_month_december":
            MessageLookupByLibrary.simpleMessage("Aralık"),
        "statistics_month_february":
            MessageLookupByLibrary.simpleMessage("Şubat"),
        "statistics_month_january":
            MessageLookupByLibrary.simpleMessage("Ocak"),
        "statistics_month_july": MessageLookupByLibrary.simpleMessage("Temmuz"),
        "statistics_month_june":
            MessageLookupByLibrary.simpleMessage("Haziran"),
        "statistics_month_march": MessageLookupByLibrary.simpleMessage("Mart"),
        "statistics_month_may": MessageLookupByLibrary.simpleMessage("Mayıs"),
        "statistics_month_november":
            MessageLookupByLibrary.simpleMessage("Kasım"),
        "statistics_month_october":
            MessageLookupByLibrary.simpleMessage("Ekim"),
        "statistics_month_september":
            MessageLookupByLibrary.simpleMessage("Eylül"),
        "statistics_time_practiced_this_month":
            MessageLookupByLibrary.simpleMessage("Bu ay çalışılan süre"),
        "statistics_time_practiced_this_year":
            MessageLookupByLibrary.simpleMessage("Bu yıl çalışılan süre"),
        "statistics_title":
            MessageLookupByLibrary.simpleMessage("İstatistikler"),
        "statistics_title_average_tempo":
            MessageLookupByLibrary.simpleMessage("Ortalama metronom hızı"),
        "statistics_title_last_week":
            MessageLookupByLibrary.simpleMessage("Geçen Hafta"),
        "statistics_title_this_week":
            MessageLookupByLibrary.simpleMessage("Bu Hafta"),
        "statistics_title_year": MessageLookupByLibrary.simpleMessage("Yıl"),
        "statistics_total_time_practiced":
            MessageLookupByLibrary.simpleMessage("Toplam çalışılan süre"),
        "statistics_txt_total": MessageLookupByLibrary.simpleMessage("Toplam"),
        "tagsEmpty": MessageLookupByLibrary.simpleMessage("Henüz etiket yok"),
        "tagsTitle": MessageLookupByLibrary.simpleMessage("Etiket"),
        "title": MessageLookupByLibrary.simpleMessage("Başlık"),
        "titleValidation":
            MessageLookupByLibrary.simpleMessage("Lütfen bir başlık girin"),
        "tuner_pitch_txt": MessageLookupByLibrary.simpleMessage("frekans"),
        "tuner_status_high": MessageLookupByLibrary.simpleMessage("tiz"),
        "tuner_status_in_tune": MessageLookupByLibrary.simpleMessage("akortlu"),
        "tuner_status_low": MessageLookupByLibrary.simpleMessage("pes"),
        "tuner_status_too_high":
            MessageLookupByLibrary.simpleMessage("çok tiz"),
        "tuner_status_too_low": MessageLookupByLibrary.simpleMessage("çok pes"),
        "wannaRemoveEntry": MessageLookupByLibrary.simpleMessage(
            "Bu girdiyi kaldırmak istiyor musunuz: "),
        "yes": MessageLookupByLibrary.simpleMessage("Evet"),
        "youHaventRecordedAnythingYet":
            MessageLookupByLibrary.simpleMessage("Henüz hiç kayıt yapmadın!")
      };
}
