// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a de_DE locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'de_DE';

  static String m0(day, hour, minute) => "${day} T, ${hour} S, ${minute} M";

  static String m1(hour, minute) => "${hour} S, ${minute} M";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "app_name": MessageLookupByLibrary.simpleMessage("Practice Suite"),
        "areYouSure": MessageLookupByLibrary.simpleMessage("Sind Sie sicher ?"),
        "cancel": MessageLookupByLibrary.simpleMessage("Abbrechen"),
        "date": MessageLookupByLibrary.simpleMessage("Datum"),
        "days": MessageLookupByLibrary.simpleMessage("Tage:"),
        "description":
            MessageLookupByLibrary.simpleMessage("Beschreibung (optional)"),
        "done": MessageLookupByLibrary.simpleMessage("Ok"),
        "duration": MessageLookupByLibrary.simpleMessage("Dauer (in Minuten)"),
        "durationValidation":
            MessageLookupByLibrary.simpleMessage("Bitte eine Dauer eingeben"),
        "duration_amount_validation": MessageLookupByLibrary.simpleMessage(
            "Nur eine Dauer zwischen 1 und 720 ist erlaubt."),
        "duration_format_validation": MessageLookupByLibrary.simpleMessage(
            "Die Dauer muss aus Zahlen bestehen"),
        "edit": MessageLookupByLibrary.simpleMessage("Bearbeiten"),
        "editLogEntry":
            MessageLookupByLibrary.simpleMessage("Log Eintrag bearbeiten"),
        "entryAddedSuccessfully": MessageLookupByLibrary.simpleMessage(
            "Eintrag erfolgreich eingefügt!"),
        "entryRemovedSuccessfully": MessageLookupByLibrary.simpleMessage(
            "Eintrag erfolgreich entfernt!"),
        "entryUpdatedSuccessfully": MessageLookupByLibrary.simpleMessage(
            "Eintrag erfolgreich aktualisiert!"),
        "error_message_microphone_denied": MessageLookupByLibrary.simpleMessage(
            "Mikrofonerlaubnis verweigert!"),
        "hours": MessageLookupByLibrary.simpleMessage("Stunden:"),
        "metronome_adagietto":
            MessageLookupByLibrary.simpleMessage("langsamer als andante"),
        "metronome_adagio": MessageLookupByLibrary.simpleMessage(
            "langsam und stattlich (wörtlich: \"in Ruhe\")"),
        "metronome_allegretto":
            MessageLookupByLibrary.simpleMessage("mäßig schnell"),
        "metronome_allegro":
            MessageLookupByLibrary.simpleMessage("schnell, zügig und hell"),
        "metronome_allegro_moderato": MessageLookupByLibrary.simpleMessage(
            "fast, aber nicht ganz Allegro"),
        "metronome_allegro_vivace":
            MessageLookupByLibrary.simpleMessage("sehr schnell"),
        "metronome_andante":
            MessageLookupByLibrary.simpleMessage("im Schritttempo"),
        "metronome_andante_moderato": MessageLookupByLibrary.simpleMessage(
            "zwischen Andante und Moderato"),
        "metronome_andantino":
            MessageLookupByLibrary.simpleMessage("etwas schneller als Andante"),
        "metronome_format_validation": MessageLookupByLibrary.simpleMessage(
            "Die Geschwindigkeit des Metronoms muss aus Zahlen bestehen"),
        "metronome_grave": MessageLookupByLibrary.simpleMessage("sehr langsam"),
        "metronome_larghetto":
            MessageLookupByLibrary.simpleMessage("recht breit gefächert"),
        "metronome_larghissimo":
            MessageLookupByLibrary.simpleMessage("sehr, sehr langsam"),
        "metronome_largo":
            MessageLookupByLibrary.simpleMessage("im Großen und Ganzen"),
        "metronome_lento": MessageLookupByLibrary.simpleMessage("langsam"),
        "metronome_limit_validation": MessageLookupByLibrary.simpleMessage(
            "Bitte wählen Sie eine Geschwindigkeit zwischen 24 und 230"),
        "metronome_marcia_moderato": MessageLookupByLibrary.simpleMessage(
            "mäßig, in der Art eines Marsches"),
        "metronome_markings_title": MessageLookupByLibrary.simpleMessage(
            "grundlegende Tempobezeichnungen"),
        "metronome_moderato": MessageLookupByLibrary.simpleMessage("mäßig"),
        "metronome_prestissimo":
            MessageLookupByLibrary.simpleMessage("noch schneller als Presto"),
        "metronome_presto":
            MessageLookupByLibrary.simpleMessage("sehr, sehr schnell"),
        "metronome_sound_name": MessageLookupByLibrary.simpleMessage("Ton"),
        "metronome_tap_to_set_tempo": MessageLookupByLibrary.simpleMessage(
            "Tippen um das Tempo einzustellen"),
        "metronome_triplet_warning": MessageLookupByLibrary.simpleMessage(
            "Um die Gesundheit Ihres Geräts zu schützen, wird die Höchstgeschwindigkeit für Triolen auf 197 bpm reduziert.\n"),
        "metronome_vivace":
            MessageLookupByLibrary.simpleMessage("lebendig und schnell"),
        "metronome_vivacissimo":
            MessageLookupByLibrary.simpleMessage("sehr schnell und lebendig"),
        "minutes": MessageLookupByLibrary.simpleMessage("Minuten:"),
        "minutesPracticed":
            MessageLookupByLibrary.simpleMessage("Minuten geübt"),
        "navBarMetronome": MessageLookupByLibrary.simpleMessage("Metronom"),
        "navBarRecording": MessageLookupByLibrary.simpleMessage("Aufnahme"),
        "navBarTuner": MessageLookupByLibrary.simpleMessage("Stimmgerät"),
        "newLogDialogTitle":
            MessageLookupByLibrary.simpleMessage("Neuer Log Eintrag"),
        "newRecording": MessageLookupByLibrary.simpleMessage("Neue Aufnahme"),
        "newTag": MessageLookupByLibrary.simpleMessage("Neuer Tag"),
        "no": MessageLookupByLibrary.simpleMessage("Nein"),
        "noLogEntryWarning": MessageLookupByLibrary.simpleMessage(
            "Noch keine Logeinträge! Zeit zum Üben!"),
        "note_text_a": MessageLookupByLibrary.simpleMessage("A"),
        "note_text_a_sharp": MessageLookupByLibrary.simpleMessage("A#"),
        "note_text_b": MessageLookupByLibrary.simpleMessage("B"),
        "note_text_c": MessageLookupByLibrary.simpleMessage("C"),
        "note_text_c_sharp": MessageLookupByLibrary.simpleMessage("C#"),
        "note_text_d": MessageLookupByLibrary.simpleMessage("D"),
        "note_text_d_sharp": MessageLookupByLibrary.simpleMessage("D#"),
        "note_text_e": MessageLookupByLibrary.simpleMessage("E"),
        "note_text_f": MessageLookupByLibrary.simpleMessage("F"),
        "note_text_f_sharp": MessageLookupByLibrary.simpleMessage("F#"),
        "note_text_g": MessageLookupByLibrary.simpleMessage("G"),
        "note_text_g_sharp": MessageLookupByLibrary.simpleMessage("G#"),
        "onboarding_app_intro": MessageLookupByLibrary.simpleMessage(
            "Practice Suite ist das ultimative Übungswerkzeug für Musiker aller Niveaus.Mit seinen umfassenden Funktionen können Sie im Takt und in der Melodie bleiben und Ihren Fortschritt in einer praktischen App verfolgen."),
        "onboarding_log_desc": MessageLookupByLibrary.simpleMessage(
            "Mit der integrierten Logfunktion können Sie Ihre Übungsstunden verfolgen, sich Ziele setzen und Ihre Fortschritte im Laufe der Zeit überwachen. "),
        "onboarding_log_title":
            MessageLookupByLibrary.simpleMessage("Übungs Log"),
        "onboarding_metronome_desc": MessageLookupByLibrary.simpleMessage(
            "Mit der Metronom-Funktion können Sie Ihr gewünschtes Tempo und die Taktart einstellen und so sicherstellen, dass Sie beim Üben im Takt und in der Tempo bleiben."),
        "onboarding_recorder_desc": MessageLookupByLibrary.simpleMessage(
            "Mit der Aufnahme-Funktion können Sie Ihre Übungssitzungen aufzeichnen und wiedergeben, so dass Sie Ihre Fortschritte verfolgen und Verbesserungsmöglichkeiten erkennen können."),
        "onboarding_skip": MessageLookupByLibrary.simpleMessage("Überspringen"),
        "onboarding_stats_desc": MessageLookupByLibrary.simpleMessage(
            "Die App bietet auch detaillierte Statistiken, so dass Sie sehen können, wie viel Zeit Sie mit dem Üben verbracht haben und wie Sie sich im Laufe der Zeit verbessert haben."),
        "onboarding_swiss_army_knife": MessageLookupByLibrary.simpleMessage(
            "Das Schweizer Taschenmesser für Musiker"),
        "onboarding_tuner_desc": MessageLookupByLibrary.simpleMessage(
            "Die Stimmgerät-Funktion nutzt das Mikrofon Ihres Geräts, um Ihr Instrument genau zu stimmen, damit Sie in perfekter Tonhöhe spielen können."),
        "optionalDescriptionText": MessageLookupByLibrary.simpleMessage(
            "Keine Beschreibung angegeben"),
        "practice_log_error_empty_tag":
            MessageLookupByLibrary.simpleMessage("Tag darf nicht leer sein"),
        "practice_log_error_no_tag_selected":
            MessageLookupByLibrary.simpleMessage(
                "Bitte mindestens einen Tag hinzufügen oder auswählen"),
        "practice_log_filter_allEntries":
            MessageLookupByLibrary.simpleMessage("alle Einträge"),
        "practice_log_filter_showAllEntries":
            MessageLookupByLibrary.simpleMessage("alle Einträge anzeigen"),
        "practice_log_filter_title":
            MessageLookupByLibrary.simpleMessage("Filter"),
        "practice_suite_intro_name":
            MessageLookupByLibrary.simpleMessage("Practice Suite!"),
        "recording_log_check_permissions": MessageLookupByLibrary.simpleMessage(
            "Bitte überprüfen Sie die Berechtigungen der Anwendung in den Systemeinstellungen"),
        "save": MessageLookupByLibrary.simpleMessage("speichern"),
        "settings_btn_licences":
            MessageLookupByLibrary.simpleMessage("Lizenzen"),
        "settings_built_with_love":
            MessageLookupByLibrary.simpleMessage("Mit 💙 gebaut von "),
        "settings_donation_txt": MessageLookupByLibrary.simpleMessage(
            "In die Entwicklung der Apps fließen unzählige Stunden. Wenn Ihnen die App gefällt und Sie die weitere Entwicklung unterstützen möchten, ziehen Sie bitte eine Spende in Betracht. Danke!"),
        "settings_follow_on":
            MessageLookupByLibrary.simpleMessage("Folgen Sie auf "),
        "settings_language_english":
            MessageLookupByLibrary.simpleMessage("Englisch"),
        "settings_language_german":
            MessageLookupByLibrary.simpleMessage("Deutsch"),
        "settings_language_system":
            MessageLookupByLibrary.simpleMessage("System"),
        "settings_language_turkish":
            MessageLookupByLibrary.simpleMessage("Türkisch"),
        "settings_theme_mode":
            MessageLookupByLibrary.simpleMessage("Theme-Modus"),
        "settings_title_about": MessageLookupByLibrary.simpleMessage("About"),
        "settings_title_donate":
            MessageLookupByLibrary.simpleMessage("Spenden"),
        "settings_title_email": MessageLookupByLibrary.simpleMessage("E-Mail"),
        "settings_title_help": MessageLookupByLibrary.simpleMessage("Hilfe"),
        "settings_title_issues":
            MessageLookupByLibrary.simpleMessage("Störungen"),
        "settings_title_language":
            MessageLookupByLibrary.simpleMessage("Sprache"),
        "settings_title_matrix_room":
            MessageLookupByLibrary.simpleMessage("Matrix-Raum"),
        "settings_title_source":
            MessageLookupByLibrary.simpleMessage("Quellcode"),
        "settings_title_txt":
            MessageLookupByLibrary.simpleMessage("Einstellungen"),
        "share_recording_text":
            MessageLookupByLibrary.simpleMessage("Seht euch das an!"),
        "share_recording_title":
            MessageLookupByLibrary.simpleMessage("Aufnahme freigeben"),
        "statistics_day_friday":
            MessageLookupByLibrary.simpleMessage("Freitag"),
        "statistics_day_monday": MessageLookupByLibrary.simpleMessage("Montag"),
        "statistics_day_saturday":
            MessageLookupByLibrary.simpleMessage("Samstag"),
        "statistics_day_sunday":
            MessageLookupByLibrary.simpleMessage("Sonntag"),
        "statistics_day_thursday":
            MessageLookupByLibrary.simpleMessage("Donnerstag"),
        "statistics_day_tuesday":
            MessageLookupByLibrary.simpleMessage("Dienstag"),
        "statistics_day_wednesday":
            MessageLookupByLibrary.simpleMessage("Mittwoch"),
        "statistics_days_hours_minutes": m0,
        "statistics_empty_warning": MessageLookupByLibrary.simpleMessage(
            "Üben Sie und fügen Sie einige Logeinträge hinzu, um Statistiken zu sehen!"),
        "statistics_hours_and_minutes": m1,
        "statistics_hours_practiced_last_week":
            MessageLookupByLibrary.simpleMessage(
                "Geübte Zeit in letzter Woche"),
        "statistics_hours_practiced_this_week":
            MessageLookupByLibrary.simpleMessage("Geübte Zeit in dieser Woche"),
        "statistics_month_april": MessageLookupByLibrary.simpleMessage("April"),
        "statistics_month_august":
            MessageLookupByLibrary.simpleMessage("August"),
        "statistics_month_december":
            MessageLookupByLibrary.simpleMessage("Dezember"),
        "statistics_month_february":
            MessageLookupByLibrary.simpleMessage("Februar"),
        "statistics_month_january":
            MessageLookupByLibrary.simpleMessage("Januar"),
        "statistics_month_july": MessageLookupByLibrary.simpleMessage("Juli"),
        "statistics_month_june": MessageLookupByLibrary.simpleMessage("Juni"),
        "statistics_month_march": MessageLookupByLibrary.simpleMessage("März"),
        "statistics_month_may": MessageLookupByLibrary.simpleMessage("Mai"),
        "statistics_month_november":
            MessageLookupByLibrary.simpleMessage("November"),
        "statistics_month_october":
            MessageLookupByLibrary.simpleMessage("Oktober"),
        "statistics_month_september":
            MessageLookupByLibrary.simpleMessage("September"),
        "statistics_time_practiced_this_month":
            MessageLookupByLibrary.simpleMessage("Geübte Zeit in diesem Monat"),
        "statistics_time_practiced_this_year":
            MessageLookupByLibrary.simpleMessage("Geübte Zeit in diesem Jahr"),
        "statistics_title": MessageLookupByLibrary.simpleMessage("Statistik"),
        "statistics_title_average_tempo": MessageLookupByLibrary.simpleMessage(
            "Durchschnittliche Geschwindigkeit des Metronoms"),
        "statistics_title_last_week":
            MessageLookupByLibrary.simpleMessage("Letzte Woche"),
        "statistics_title_this_week":
            MessageLookupByLibrary.simpleMessage("Diese Woche"),
        "statistics_title_year": MessageLookupByLibrary.simpleMessage("Jahr"),
        "statistics_total_time_practiced":
            MessageLookupByLibrary.simpleMessage("Gesamte Übungszeit"),
        "statistics_txt_total":
            MessageLookupByLibrary.simpleMessage("Insgesamt"),
        "tagsEmpty": MessageLookupByLibrary.simpleMessage("Noch keine Tags"),
        "tagsTitle": MessageLookupByLibrary.simpleMessage("Tags"),
        "title": MessageLookupByLibrary.simpleMessage("Titel"),
        "titleValidation":
            MessageLookupByLibrary.simpleMessage("Bitte einen Titel eingeben"),
        "tuner_pitch_txt": MessageLookupByLibrary.simpleMessage("kammerton"),
        "tuner_status_high": MessageLookupByLibrary.simpleMessage("hoch"),
        "tuner_status_in_tune":
            MessageLookupByLibrary.simpleMessage("gestimmt"),
        "tuner_status_low": MessageLookupByLibrary.simpleMessage("tief"),
        "tuner_status_too_high":
            MessageLookupByLibrary.simpleMessage("zu hoch"),
        "tuner_status_too_low": MessageLookupByLibrary.simpleMessage("zu tief"),
        "wannaRemoveEntry": MessageLookupByLibrary.simpleMessage(
            "Möchten Sie diesen Eintrag entfernen: "),
        "yes": MessageLookupByLibrary.simpleMessage("Ja"),
        "youHaventRecordedAnythingYet": MessageLookupByLibrary.simpleMessage(
            "Du hast noch nichts aufgenommen!")
      };
}
