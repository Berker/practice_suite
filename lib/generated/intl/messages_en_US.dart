// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  static String m0(day, hour, minute) => "${day} d, ${hour} h, ${minute} m";

  static String m1(hour, minute) => "${hour} h, ${minute} m";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "app_name": MessageLookupByLibrary.simpleMessage("Practice Suite"),
        "areYouSure": MessageLookupByLibrary.simpleMessage("Are you sure ?"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "date": MessageLookupByLibrary.simpleMessage("Date"),
        "days": MessageLookupByLibrary.simpleMessage("Days:"),
        "description":
            MessageLookupByLibrary.simpleMessage("Description (optional)"),
        "done": MessageLookupByLibrary.simpleMessage("Done"),
        "duration":
            MessageLookupByLibrary.simpleMessage("Duration (in minutes)"),
        "durationValidation":
            MessageLookupByLibrary.simpleMessage("Please enter a duration"),
        "duration_amount_validation": MessageLookupByLibrary.simpleMessage(
            "Only a duration between 1 and 720 is allowed"),
        "duration_format_validation": MessageLookupByLibrary.simpleMessage(
            "Duration must consist of numbers"),
        "edit": MessageLookupByLibrary.simpleMessage("Edit"),
        "editLogEntry": MessageLookupByLibrary.simpleMessage("Edit Log Entry"),
        "entryAddedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Entry added successfully!"),
        "entryRemovedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Entry removed successfully!"),
        "entryUpdatedSuccessfully":
            MessageLookupByLibrary.simpleMessage("Entry updated successfully!"),
        "error_message_microphone_denied": MessageLookupByLibrary.simpleMessage(
            "Microphone permission is denied!"),
        "hours": MessageLookupByLibrary.simpleMessage("Hours:"),
        "metronome_adagietto":
            MessageLookupByLibrary.simpleMessage("langsamer als Andante"),
        "metronome_adagio": MessageLookupByLibrary.simpleMessage(
            "slow and stately (literally, \"at ease\")"),
        "metronome_allegretto":
            MessageLookupByLibrary.simpleMessage("moderately fast"),
        "metronome_allegro":
            MessageLookupByLibrary.simpleMessage("fast, quickly and bright"),
        "metronome_allegro_moderato": MessageLookupByLibrary.simpleMessage(
            "close to but not quite allegro"),
        "metronome_allegro_vivace":
            MessageLookupByLibrary.simpleMessage("very fast"),
        "metronome_andante":
            MessageLookupByLibrary.simpleMessage("im Schritttempo"),
        "metronome_andante_moderato": MessageLookupByLibrary.simpleMessage(
            "between andante and moderato"),
        "metronome_andantino": MessageLookupByLibrary.simpleMessage(
            "slightly faster than andante"),
        "metronome_format_validation": MessageLookupByLibrary.simpleMessage(
            "Metronome speed must consist of numbers"),
        "metronome_grave": MessageLookupByLibrary.simpleMessage("very slow"),
        "metronome_larghetto":
            MessageLookupByLibrary.simpleMessage("rather broadly"),
        "metronome_larghissimo":
            MessageLookupByLibrary.simpleMessage("very, very slow"),
        "metronome_largo": MessageLookupByLibrary.simpleMessage("broadly"),
        "metronome_lento": MessageLookupByLibrary.simpleMessage("slowly"),
        "metronome_limit_validation": MessageLookupByLibrary.simpleMessage(
            "Please choose a speed between 24 and 230"),
        "metronome_marcia_moderato": MessageLookupByLibrary.simpleMessage(
            "moderately, in the manner of a march"),
        "metronome_markings_title":
            MessageLookupByLibrary.simpleMessage("Basic tempo markings"),
        "metronome_moderato":
            MessageLookupByLibrary.simpleMessage("moderately"),
        "metronome_prestissimo":
            MessageLookupByLibrary.simpleMessage("even faster than Presto"),
        "metronome_presto":
            MessageLookupByLibrary.simpleMessage("very, very fast"),
        "metronome_sound_name": MessageLookupByLibrary.simpleMessage("Sound"),
        "metronome_tap_to_set_tempo":
            MessageLookupByLibrary.simpleMessage("Tap to set tempo"),
        "metronome_triplet_warning": MessageLookupByLibrary.simpleMessage(
            "For the health of your device, maximum speed is reduced to 197 bpm for triplets"),
        "metronome_vivace":
            MessageLookupByLibrary.simpleMessage("lively and fast"),
        "metronome_vivacissimo":
            MessageLookupByLibrary.simpleMessage("very fast and lively"),
        "minutes": MessageLookupByLibrary.simpleMessage("Minutes:"),
        "minutesPracticed":
            MessageLookupByLibrary.simpleMessage("minutes practiced"),
        "navBarMetronome": MessageLookupByLibrary.simpleMessage("Metronome"),
        "navBarRecording": MessageLookupByLibrary.simpleMessage("Recorder"),
        "navBarTuner": MessageLookupByLibrary.simpleMessage("Tuner"),
        "navBarlog": MessageLookupByLibrary.simpleMessage("Log"),
        "newLogDialogTitle":
            MessageLookupByLibrary.simpleMessage("New Log Entry"),
        "newRecording": MessageLookupByLibrary.simpleMessage("New Recording"),
        "newTag": MessageLookupByLibrary.simpleMessage("New tag"),
        "no": MessageLookupByLibrary.simpleMessage("No"),
        "noLogEntryWarning": MessageLookupByLibrary.simpleMessage(
            "No log entries yet! Time to practice!"),
        "note_text_a": MessageLookupByLibrary.simpleMessage("A"),
        "note_text_a_sharp": MessageLookupByLibrary.simpleMessage("A#"),
        "note_text_b": MessageLookupByLibrary.simpleMessage("B"),
        "note_text_c": MessageLookupByLibrary.simpleMessage("C"),
        "note_text_c_sharp": MessageLookupByLibrary.simpleMessage("C#"),
        "note_text_d": MessageLookupByLibrary.simpleMessage("D"),
        "note_text_d_sharp": MessageLookupByLibrary.simpleMessage("D#"),
        "note_text_e": MessageLookupByLibrary.simpleMessage("E"),
        "note_text_f": MessageLookupByLibrary.simpleMessage("F"),
        "note_text_f_sharp": MessageLookupByLibrary.simpleMessage("F#"),
        "note_text_g": MessageLookupByLibrary.simpleMessage("G"),
        "note_text_g_sharp": MessageLookupByLibrary.simpleMessage("G#"),
        "onboarding_app_intro": MessageLookupByLibrary.simpleMessage(
            "Practice Suite is the ultimate music practice tool for musicians of all levels.With its comprehensive features, you can stay on beat, in tune, and track your progress in one convenient app."),
        "onboarding_log_desc": MessageLookupByLibrary.simpleMessage(
            "With the built-in Log feature, you can keep track of your practice sessions, set goals, and monitor your progress over time."),
        "onboarding_log_title":
            MessageLookupByLibrary.simpleMessage("Practice Log"),
        "onboarding_metronome_desc": MessageLookupByLibrary.simpleMessage(
            "The Metronome feature allows you to set your desired tempo and time signature, ensuring that you stay on beat and in time while practicing."),
        "onboarding_recorder_desc": MessageLookupByLibrary.simpleMessage(
            "The Recorder feature allows you to record your practice sessions and play them back, so you can listen to your progress and identify areas for improvement."),
        "onboarding_skip": MessageLookupByLibrary.simpleMessage("Skip"),
        "onboarding_stats_desc": MessageLookupByLibrary.simpleMessage(
            "The app also provides detailed statistics, so you can see how much time you\'ve spent practicing and how you\'ve improved over time."),
        "onboarding_swiss_army_knife": MessageLookupByLibrary.simpleMessage(
            "Introducing the Swiss army knife of musicians"),
        "onboarding_tuner_desc": MessageLookupByLibrary.simpleMessage(
            "The Tuner feature uses your device\'s microphone to accurately tune your instrument, helping you to play in perfect pitch."),
        "optionalDescriptionText":
            MessageLookupByLibrary.simpleMessage("No description given"),
        "practice_log_error_empty_tag":
            MessageLookupByLibrary.simpleMessage("Tag can\'t be empty"),
        "practice_log_error_no_tag_selected":
            MessageLookupByLibrary.simpleMessage(
                "Please add or select at least one tag"),
        "practice_log_filter_allEntries":
            MessageLookupByLibrary.simpleMessage("All entries"),
        "practice_log_filter_showAllEntries":
            MessageLookupByLibrary.simpleMessage("Show all entries"),
        "practice_log_filter_title":
            MessageLookupByLibrary.simpleMessage("Filter"),
        "practice_suite_intro_name":
            MessageLookupByLibrary.simpleMessage("Practice Suite!"),
        "recording_log_check_permissions": MessageLookupByLibrary.simpleMessage(
            "Please check app permissions in system settings"),
        "save": MessageLookupByLibrary.simpleMessage("save"),
        "settings_btn_licences":
            MessageLookupByLibrary.simpleMessage("Licences"),
        "settings_built_with_love":
            MessageLookupByLibrary.simpleMessage("Built with 💙 by "),
        "settings_donation_txt": MessageLookupByLibrary.simpleMessage(
            "Countless hours goes into developing the apps, if you like the app and want to support the continued development, please consider donating. Thanks!"),
        "settings_follow_on":
            MessageLookupByLibrary.simpleMessage("Follow on "),
        "settings_language_english":
            MessageLookupByLibrary.simpleMessage("English"),
        "settings_language_german":
            MessageLookupByLibrary.simpleMessage("German"),
        "settings_language_system":
            MessageLookupByLibrary.simpleMessage("System"),
        "settings_language_turkish":
            MessageLookupByLibrary.simpleMessage("Turkish"),
        "settings_theme_mode":
            MessageLookupByLibrary.simpleMessage("Theme Mode"),
        "settings_title_about": MessageLookupByLibrary.simpleMessage("About"),
        "settings_title_donate": MessageLookupByLibrary.simpleMessage("Donate"),
        "settings_title_email": MessageLookupByLibrary.simpleMessage("E-mail"),
        "settings_title_help": MessageLookupByLibrary.simpleMessage("Help"),
        "settings_title_issues": MessageLookupByLibrary.simpleMessage("Issues"),
        "settings_title_language":
            MessageLookupByLibrary.simpleMessage("Language"),
        "settings_title_matrix_room":
            MessageLookupByLibrary.simpleMessage("Matrix room"),
        "settings_title_source": MessageLookupByLibrary.simpleMessage("Source"),
        "settings_title_txt": MessageLookupByLibrary.simpleMessage("Settings"),
        "share_recording_text":
            MessageLookupByLibrary.simpleMessage("Check this out!"),
        "share_recording_title":
            MessageLookupByLibrary.simpleMessage("Share recording"),
        "statistics_day_friday": MessageLookupByLibrary.simpleMessage("Friday"),
        "statistics_day_monday": MessageLookupByLibrary.simpleMessage("Monday"),
        "statistics_day_saturday":
            MessageLookupByLibrary.simpleMessage("Saturday"),
        "statistics_day_sunday": MessageLookupByLibrary.simpleMessage("Sunday"),
        "statistics_day_thursday":
            MessageLookupByLibrary.simpleMessage("Thursday"),
        "statistics_day_tuesday":
            MessageLookupByLibrary.simpleMessage("Tuesday"),
        "statistics_day_wednesday":
            MessageLookupByLibrary.simpleMessage("Wednesday"),
        "statistics_days_hours_minutes": m0,
        "statistics_empty_warning": MessageLookupByLibrary.simpleMessage(
            "Practice and add some log entries to see statistics!"),
        "statistics_hours_and_minutes": m1,
        "statistics_hours_practiced_last_week":
            MessageLookupByLibrary.simpleMessage("Time practiced last week"),
        "statistics_hours_practiced_this_week":
            MessageLookupByLibrary.simpleMessage("Time practiced this week"),
        "statistics_month_april": MessageLookupByLibrary.simpleMessage("April"),
        "statistics_month_august":
            MessageLookupByLibrary.simpleMessage("August"),
        "statistics_month_december":
            MessageLookupByLibrary.simpleMessage("December"),
        "statistics_month_february":
            MessageLookupByLibrary.simpleMessage("February"),
        "statistics_month_january":
            MessageLookupByLibrary.simpleMessage("January"),
        "statistics_month_july": MessageLookupByLibrary.simpleMessage("July"),
        "statistics_month_june": MessageLookupByLibrary.simpleMessage("June"),
        "statistics_month_march": MessageLookupByLibrary.simpleMessage("March"),
        "statistics_month_may": MessageLookupByLibrary.simpleMessage("May"),
        "statistics_month_november":
            MessageLookupByLibrary.simpleMessage("November"),
        "statistics_month_october":
            MessageLookupByLibrary.simpleMessage("October"),
        "statistics_month_september":
            MessageLookupByLibrary.simpleMessage("September"),
        "statistics_time_practiced_this_month":
            MessageLookupByLibrary.simpleMessage("Time practiced this month"),
        "statistics_time_practiced_this_year":
            MessageLookupByLibrary.simpleMessage("Time practiced this year"),
        "statistics_title": MessageLookupByLibrary.simpleMessage("Statistics"),
        "statistics_title_average_tempo":
            MessageLookupByLibrary.simpleMessage("Average Metronome Speed"),
        "statistics_title_last_week":
            MessageLookupByLibrary.simpleMessage("Last Week"),
        "statistics_title_this_week":
            MessageLookupByLibrary.simpleMessage("This Week"),
        "statistics_title_year": MessageLookupByLibrary.simpleMessage("Year"),
        "statistics_total_time_practiced":
            MessageLookupByLibrary.simpleMessage("Total time practiced"),
        "statistics_txt_total": MessageLookupByLibrary.simpleMessage("Total"),
        "tagsEmpty": MessageLookupByLibrary.simpleMessage("No tags yet"),
        "tagsTitle": MessageLookupByLibrary.simpleMessage("Tags"),
        "title": MessageLookupByLibrary.simpleMessage("Title"),
        "titleValidation":
            MessageLookupByLibrary.simpleMessage("Please enter a title"),
        "tuner_pitch_txt": MessageLookupByLibrary.simpleMessage("pitch"),
        "tuner_status_high": MessageLookupByLibrary.simpleMessage("high"),
        "tuner_status_in_tune": MessageLookupByLibrary.simpleMessage("in tune"),
        "tuner_status_low": MessageLookupByLibrary.simpleMessage("low"),
        "tuner_status_too_high":
            MessageLookupByLibrary.simpleMessage("too high"),
        "tuner_status_too_low": MessageLookupByLibrary.simpleMessage("too low"),
        "wannaRemoveEntry":
            MessageLookupByLibrary.simpleMessage("Do you want to remove "),
        "yes": MessageLookupByLibrary.simpleMessage("Yes"),
        "youHaventRecordedAnythingYet": MessageLookupByLibrary.simpleMessage(
            "You have not recorded anything yet!")
      };
}
