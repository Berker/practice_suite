// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Practice Suite`
  String get app_name {
    return Intl.message(
      'Practice Suite',
      name: 'app_name',
      desc: '',
      args: [],
    );
  }

  /// `Log`
  String get navBarlog {
    return Intl.message(
      'Log',
      name: 'navBarlog',
      desc: '',
      args: [],
    );
  }

  /// `Recorder`
  String get navBarRecording {
    return Intl.message(
      'Recorder',
      name: 'navBarRecording',
      desc: '',
      args: [],
    );
  }

  /// `Metronome`
  String get navBarMetronome {
    return Intl.message(
      'Metronome',
      name: 'navBarMetronome',
      desc: '',
      args: [],
    );
  }

  /// `Tuner`
  String get navBarTuner {
    return Intl.message(
      'Tuner',
      name: 'navBarTuner',
      desc: '',
      args: [],
    );
  }

  /// `New Log Entry`
  String get newLogDialogTitle {
    return Intl.message(
      'New Log Entry',
      name: 'newLogDialogTitle',
      desc: '',
      args: [],
    );
  }

  /// `Title`
  String get title {
    return Intl.message(
      'Title',
      name: 'title',
      desc: '',
      args: [],
    );
  }

  /// `Duration (in minutes)`
  String get duration {
    return Intl.message(
      'Duration (in minutes)',
      name: 'duration',
      desc: '',
      args: [],
    );
  }

  /// `Description (optional)`
  String get description {
    return Intl.message(
      'Description (optional)',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `Date`
  String get date {
    return Intl.message(
      'Date',
      name: 'date',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a title`
  String get titleValidation {
    return Intl.message(
      'Please enter a title',
      name: 'titleValidation',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a duration`
  String get durationValidation {
    return Intl.message(
      'Please enter a duration',
      name: 'durationValidation',
      desc: '',
      args: [],
    );
  }

  /// `Done`
  String get done {
    return Intl.message(
      'Done',
      name: 'done',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Edit`
  String get edit {
    return Intl.message(
      'Edit',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `No description given`
  String get optionalDescriptionText {
    return Intl.message(
      'No description given',
      name: 'optionalDescriptionText',
      desc: '',
      args: [],
    );
  }

  /// `No log entries yet! Time to practice!`
  String get noLogEntryWarning {
    return Intl.message(
      'No log entries yet! Time to practice!',
      name: 'noLogEntryWarning',
      desc: '',
      args: [],
    );
  }

  /// `minutes practiced`
  String get minutesPracticed {
    return Intl.message(
      'minutes practiced',
      name: 'minutesPracticed',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure ?`
  String get areYouSure {
    return Intl.message(
      'Are you sure ?',
      name: 'areYouSure',
      desc: '',
      args: [],
    );
  }

  /// `Do you want to remove `
  String get wannaRemoveEntry {
    return Intl.message(
      'Do you want to remove ',
      name: 'wannaRemoveEntry',
      desc: '',
      args: [],
    );
  }

  /// `Hours:`
  String get hours {
    return Intl.message(
      'Hours:',
      name: 'hours',
      desc: '',
      args: [],
    );
  }

  /// `Minutes:`
  String get minutes {
    return Intl.message(
      'Minutes:',
      name: 'minutes',
      desc: '',
      args: [],
    );
  }

  /// `Days:`
  String get days {
    return Intl.message(
      'Days:',
      name: 'days',
      desc: '',
      args: [],
    );
  }

  /// `Yes`
  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `No`
  String get no {
    return Intl.message(
      'No',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `Entry added successfully!`
  String get entryAddedSuccessfully {
    return Intl.message(
      'Entry added successfully!',
      name: 'entryAddedSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `Entry removed successfully!`
  String get entryRemovedSuccessfully {
    return Intl.message(
      'Entry removed successfully!',
      name: 'entryRemovedSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `Entry updated successfully!`
  String get entryUpdatedSuccessfully {
    return Intl.message(
      'Entry updated successfully!',
      name: 'entryUpdatedSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `Edit Log Entry`
  String get editLogEntry {
    return Intl.message(
      'Edit Log Entry',
      name: 'editLogEntry',
      desc: '',
      args: [],
    );
  }

  /// `New Recording`
  String get newRecording {
    return Intl.message(
      'New Recording',
      name: 'newRecording',
      desc: '',
      args: [],
    );
  }

  /// `You have not recorded anything yet!`
  String get youHaventRecordedAnythingYet {
    return Intl.message(
      'You have not recorded anything yet!',
      name: 'youHaventRecordedAnythingYet',
      desc: '',
      args: [],
    );
  }

  /// `save`
  String get save {
    return Intl.message(
      'save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `very, very slow`
  String get metronome_larghissimo {
    return Intl.message(
      'very, very slow',
      name: 'metronome_larghissimo',
      desc: '',
      args: [],
    );
  }

  /// `very slow`
  String get metronome_grave {
    return Intl.message(
      'very slow',
      name: 'metronome_grave',
      desc: '',
      args: [],
    );
  }

  /// `broadly`
  String get metronome_largo {
    return Intl.message(
      'broadly',
      name: 'metronome_largo',
      desc: '',
      args: [],
    );
  }

  /// `slowly`
  String get metronome_lento {
    return Intl.message(
      'slowly',
      name: 'metronome_lento',
      desc: '',
      args: [],
    );
  }

  /// `rather broadly`
  String get metronome_larghetto {
    return Intl.message(
      'rather broadly',
      name: 'metronome_larghetto',
      desc: '',
      args: [],
    );
  }

  /// `slow and stately (literally, "at ease")`
  String get metronome_adagio {
    return Intl.message(
      'slow and stately (literally, "at ease")',
      name: 'metronome_adagio',
      desc: '',
      args: [],
    );
  }

  /// `langsamer als Andante`
  String get metronome_adagietto {
    return Intl.message(
      'langsamer als Andante',
      name: 'metronome_adagietto',
      desc: '',
      args: [],
    );
  }

  /// `im Schritttempo`
  String get metronome_andante {
    return Intl.message(
      'im Schritttempo',
      name: 'metronome_andante',
      desc: '',
      args: [],
    );
  }

  /// `slightly faster than andante`
  String get metronome_andantino {
    return Intl.message(
      'slightly faster than andante',
      name: 'metronome_andantino',
      desc: '',
      args: [],
    );
  }

  /// `moderately, in the manner of a march`
  String get metronome_marcia_moderato {
    return Intl.message(
      'moderately, in the manner of a march',
      name: 'metronome_marcia_moderato',
      desc: '',
      args: [],
    );
  }

  /// `between andante and moderato`
  String get metronome_andante_moderato {
    return Intl.message(
      'between andante and moderato',
      name: 'metronome_andante_moderato',
      desc: '',
      args: [],
    );
  }

  /// `moderately`
  String get metronome_moderato {
    return Intl.message(
      'moderately',
      name: 'metronome_moderato',
      desc: '',
      args: [],
    );
  }

  /// `moderately fast`
  String get metronome_allegretto {
    return Intl.message(
      'moderately fast',
      name: 'metronome_allegretto',
      desc: '',
      args: [],
    );
  }

  /// `close to but not quite allegro`
  String get metronome_allegro_moderato {
    return Intl.message(
      'close to but not quite allegro',
      name: 'metronome_allegro_moderato',
      desc: '',
      args: [],
    );
  }

  /// `fast, quickly and bright`
  String get metronome_allegro {
    return Intl.message(
      'fast, quickly and bright',
      name: 'metronome_allegro',
      desc: '',
      args: [],
    );
  }

  /// `lively and fast`
  String get metronome_vivace {
    return Intl.message(
      'lively and fast',
      name: 'metronome_vivace',
      desc: '',
      args: [],
    );
  }

  /// `very fast and lively`
  String get metronome_vivacissimo {
    return Intl.message(
      'very fast and lively',
      name: 'metronome_vivacissimo',
      desc: '',
      args: [],
    );
  }

  /// `very fast`
  String get metronome_allegro_vivace {
    return Intl.message(
      'very fast',
      name: 'metronome_allegro_vivace',
      desc: '',
      args: [],
    );
  }

  /// `very, very fast`
  String get metronome_presto {
    return Intl.message(
      'very, very fast',
      name: 'metronome_presto',
      desc: '',
      args: [],
    );
  }

  /// `even faster than Presto`
  String get metronome_prestissimo {
    return Intl.message(
      'even faster than Presto',
      name: 'metronome_prestissimo',
      desc: '',
      args: [],
    );
  }

  /// `Basic tempo markings`
  String get metronome_markings_title {
    return Intl.message(
      'Basic tempo markings',
      name: 'metronome_markings_title',
      desc: '',
      args: [],
    );
  }

  /// `For the health of your device, maximum speed is reduced to 197 bpm for triplets`
  String get metronome_triplet_warning {
    return Intl.message(
      'For the health of your device, maximum speed is reduced to 197 bpm for triplets',
      name: 'metronome_triplet_warning',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settings_title_txt {
    return Intl.message(
      'Settings',
      name: 'settings_title_txt',
      desc: '',
      args: [],
    );
  }

  /// `Share recording`
  String get share_recording_title {
    return Intl.message(
      'Share recording',
      name: 'share_recording_title',
      desc: '',
      args: [],
    );
  }

  /// `Check this out!`
  String get share_recording_text {
    return Intl.message(
      'Check this out!',
      name: 'share_recording_text',
      desc: '',
      args: [],
    );
  }

  /// `Please check app permissions in system settings`
  String get recording_log_check_permissions {
    return Intl.message(
      'Please check app permissions in system settings',
      name: 'recording_log_check_permissions',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get settings_title_about {
    return Intl.message(
      'About',
      name: 'settings_title_about',
      desc: '',
      args: [],
    );
  }

  /// `Donate`
  String get settings_title_donate {
    return Intl.message(
      'Donate',
      name: 'settings_title_donate',
      desc: '',
      args: [],
    );
  }

  /// `Countless hours goes into developing the apps, if you like the app and want to support the continued development, please consider donating. Thanks!`
  String get settings_donation_txt {
    return Intl.message(
      'Countless hours goes into developing the apps, if you like the app and want to support the continued development, please consider donating. Thanks!',
      name: 'settings_donation_txt',
      desc: '',
      args: [],
    );
  }

  /// `Help`
  String get settings_title_help {
    return Intl.message(
      'Help',
      name: 'settings_title_help',
      desc: '',
      args: [],
    );
  }

  /// `Theme Mode`
  String get settings_theme_mode {
    return Intl.message(
      'Theme Mode',
      name: 'settings_theme_mode',
      desc: '',
      args: [],
    );
  }

  /// `German`
  String get settings_language_german {
    return Intl.message(
      'German',
      name: 'settings_language_german',
      desc: '',
      args: [],
    );
  }

  /// `English`
  String get settings_language_english {
    return Intl.message(
      'English',
      name: 'settings_language_english',
      desc: '',
      args: [],
    );
  }

  /// `System`
  String get settings_language_system {
    return Intl.message(
      'System',
      name: 'settings_language_system',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get settings_title_language {
    return Intl.message(
      'Language',
      name: 'settings_title_language',
      desc: '',
      args: [],
    );
  }

  /// `too low`
  String get tuner_status_too_low {
    return Intl.message(
      'too low',
      name: 'tuner_status_too_low',
      desc: '',
      args: [],
    );
  }

  /// `low`
  String get tuner_status_low {
    return Intl.message(
      'low',
      name: 'tuner_status_low',
      desc: '',
      args: [],
    );
  }

  /// `in tune`
  String get tuner_status_in_tune {
    return Intl.message(
      'in tune',
      name: 'tuner_status_in_tune',
      desc: '',
      args: [],
    );
  }

  /// `high`
  String get tuner_status_high {
    return Intl.message(
      'high',
      name: 'tuner_status_high',
      desc: '',
      args: [],
    );
  }

  /// `too high`
  String get tuner_status_too_high {
    return Intl.message(
      'too high',
      name: 'tuner_status_too_high',
      desc: '',
      args: [],
    );
  }

  /// `pitch`
  String get tuner_pitch_txt {
    return Intl.message(
      'pitch',
      name: 'tuner_pitch_txt',
      desc: '',
      args: [],
    );
  }

  /// `Statistics`
  String get statistics_title {
    return Intl.message(
      'Statistics',
      name: 'statistics_title',
      desc: '',
      args: [],
    );
  }

  /// `Duration must consist of numbers`
  String get duration_format_validation {
    return Intl.message(
      'Duration must consist of numbers',
      name: 'duration_format_validation',
      desc: '',
      args: [],
    );
  }

  /// `Only a duration between 1 and 720 is allowed`
  String get duration_amount_validation {
    return Intl.message(
      'Only a duration between 1 and 720 is allowed',
      name: 'duration_amount_validation',
      desc: '',
      args: [],
    );
  }

  /// `Metronome speed must consist of numbers`
  String get metronome_format_validation {
    return Intl.message(
      'Metronome speed must consist of numbers',
      name: 'metronome_format_validation',
      desc: '',
      args: [],
    );
  }

  /// `Please choose a speed between 24 and 230`
  String get metronome_limit_validation {
    return Intl.message(
      'Please choose a speed between 24 and 230',
      name: 'metronome_limit_validation',
      desc: '',
      args: [],
    );
  }

  /// `Average Metronome Speed`
  String get statistics_title_average_tempo {
    return Intl.message(
      'Average Metronome Speed',
      name: 'statistics_title_average_tempo',
      desc: '',
      args: [],
    );
  }

  /// `Time practiced this week`
  String get statistics_hours_practiced_this_week {
    return Intl.message(
      'Time practiced this week',
      name: 'statistics_hours_practiced_this_week',
      desc: '',
      args: [],
    );
  }

  /// `Practice and add some log entries to see statistics!`
  String get statistics_empty_warning {
    return Intl.message(
      'Practice and add some log entries to see statistics!',
      name: 'statistics_empty_warning',
      desc: '',
      args: [],
    );
  }

  /// `{hour} h, {minute} m`
  String statistics_hours_and_minutes(Object hour, Object minute) {
    return Intl.message(
      '$hour h, $minute m',
      name: 'statistics_hours_and_minutes',
      desc: '',
      args: [hour, minute],
    );
  }

  /// `Time practiced this month`
  String get statistics_time_practiced_this_month {
    return Intl.message(
      'Time practiced this month',
      name: 'statistics_time_practiced_this_month',
      desc: '',
      args: [],
    );
  }

  /// `{day} d, {hour} h, {minute} m`
  String statistics_days_hours_minutes(Object day, Object hour, Object minute) {
    return Intl.message(
      '$day d, $hour h, $minute m',
      name: 'statistics_days_hours_minutes',
      desc: '',
      args: [day, hour, minute],
    );
  }

  /// `Time practiced last week`
  String get statistics_hours_practiced_last_week {
    return Intl.message(
      'Time practiced last week',
      name: 'statistics_hours_practiced_last_week',
      desc: '',
      args: [],
    );
  }

  /// `Time practiced this year`
  String get statistics_time_practiced_this_year {
    return Intl.message(
      'Time practiced this year',
      name: 'statistics_time_practiced_this_year',
      desc: '',
      args: [],
    );
  }

  /// `Total time practiced`
  String get statistics_total_time_practiced {
    return Intl.message(
      'Total time practiced',
      name: 'statistics_total_time_practiced',
      desc: '',
      args: [],
    );
  }

  /// `Last Week`
  String get statistics_title_last_week {
    return Intl.message(
      'Last Week',
      name: 'statistics_title_last_week',
      desc: '',
      args: [],
    );
  }

  /// `This Week`
  String get statistics_title_this_week {
    return Intl.message(
      'This Week',
      name: 'statistics_title_this_week',
      desc: '',
      args: [],
    );
  }

  /// `Total`
  String get statistics_txt_total {
    return Intl.message(
      'Total',
      name: 'statistics_txt_total',
      desc: '',
      args: [],
    );
  }

  /// `Monday`
  String get statistics_day_monday {
    return Intl.message(
      'Monday',
      name: 'statistics_day_monday',
      desc: '',
      args: [],
    );
  }

  /// `Tuesday`
  String get statistics_day_tuesday {
    return Intl.message(
      'Tuesday',
      name: 'statistics_day_tuesday',
      desc: '',
      args: [],
    );
  }

  /// `Wednesday`
  String get statistics_day_wednesday {
    return Intl.message(
      'Wednesday',
      name: 'statistics_day_wednesday',
      desc: '',
      args: [],
    );
  }

  /// `Thursday`
  String get statistics_day_thursday {
    return Intl.message(
      'Thursday',
      name: 'statistics_day_thursday',
      desc: '',
      args: [],
    );
  }

  /// `Friday`
  String get statistics_day_friday {
    return Intl.message(
      'Friday',
      name: 'statistics_day_friday',
      desc: '',
      args: [],
    );
  }

  /// `Saturday`
  String get statistics_day_saturday {
    return Intl.message(
      'Saturday',
      name: 'statistics_day_saturday',
      desc: '',
      args: [],
    );
  }

  /// `Sunday`
  String get statistics_day_sunday {
    return Intl.message(
      'Sunday',
      name: 'statistics_day_sunday',
      desc: '',
      args: [],
    );
  }

  /// `Year`
  String get statistics_title_year {
    return Intl.message(
      'Year',
      name: 'statistics_title_year',
      desc: '',
      args: [],
    );
  }

  /// `January`
  String get statistics_month_january {
    return Intl.message(
      'January',
      name: 'statistics_month_january',
      desc: '',
      args: [],
    );
  }

  /// `February`
  String get statistics_month_february {
    return Intl.message(
      'February',
      name: 'statistics_month_february',
      desc: '',
      args: [],
    );
  }

  /// `March`
  String get statistics_month_march {
    return Intl.message(
      'March',
      name: 'statistics_month_march',
      desc: '',
      args: [],
    );
  }

  /// `April`
  String get statistics_month_april {
    return Intl.message(
      'April',
      name: 'statistics_month_april',
      desc: '',
      args: [],
    );
  }

  /// `May`
  String get statistics_month_may {
    return Intl.message(
      'May',
      name: 'statistics_month_may',
      desc: '',
      args: [],
    );
  }

  /// `June`
  String get statistics_month_june {
    return Intl.message(
      'June',
      name: 'statistics_month_june',
      desc: '',
      args: [],
    );
  }

  /// `July`
  String get statistics_month_july {
    return Intl.message(
      'July',
      name: 'statistics_month_july',
      desc: '',
      args: [],
    );
  }

  /// `August`
  String get statistics_month_august {
    return Intl.message(
      'August',
      name: 'statistics_month_august',
      desc: '',
      args: [],
    );
  }

  /// `September`
  String get statistics_month_september {
    return Intl.message(
      'September',
      name: 'statistics_month_september',
      desc: '',
      args: [],
    );
  }

  /// `October`
  String get statistics_month_october {
    return Intl.message(
      'October',
      name: 'statistics_month_october',
      desc: '',
      args: [],
    );
  }

  /// `November`
  String get statistics_month_november {
    return Intl.message(
      'November',
      name: 'statistics_month_november',
      desc: '',
      args: [],
    );
  }

  /// `December`
  String get statistics_month_december {
    return Intl.message(
      'December',
      name: 'statistics_month_december',
      desc: '',
      args: [],
    );
  }

  /// `Show all entries`
  String get practice_log_filter_showAllEntries {
    return Intl.message(
      'Show all entries',
      name: 'practice_log_filter_showAllEntries',
      desc: '',
      args: [],
    );
  }

  /// `All entries`
  String get practice_log_filter_allEntries {
    return Intl.message(
      'All entries',
      name: 'practice_log_filter_allEntries',
      desc: '',
      args: [],
    );
  }

  /// `C`
  String get note_text_c {
    return Intl.message(
      'C',
      name: 'note_text_c',
      desc: '',
      args: [],
    );
  }

  /// `C#`
  String get note_text_c_sharp {
    return Intl.message(
      'C#',
      name: 'note_text_c_sharp',
      desc: '',
      args: [],
    );
  }

  /// `D`
  String get note_text_d {
    return Intl.message(
      'D',
      name: 'note_text_d',
      desc: '',
      args: [],
    );
  }

  /// `D#`
  String get note_text_d_sharp {
    return Intl.message(
      'D#',
      name: 'note_text_d_sharp',
      desc: '',
      args: [],
    );
  }

  /// `E`
  String get note_text_e {
    return Intl.message(
      'E',
      name: 'note_text_e',
      desc: '',
      args: [],
    );
  }

  /// `F`
  String get note_text_f {
    return Intl.message(
      'F',
      name: 'note_text_f',
      desc: '',
      args: [],
    );
  }

  /// `F#`
  String get note_text_f_sharp {
    return Intl.message(
      'F#',
      name: 'note_text_f_sharp',
      desc: '',
      args: [],
    );
  }

  /// `G`
  String get note_text_g {
    return Intl.message(
      'G',
      name: 'note_text_g',
      desc: '',
      args: [],
    );
  }

  /// `G#`
  String get note_text_g_sharp {
    return Intl.message(
      'G#',
      name: 'note_text_g_sharp',
      desc: '',
      args: [],
    );
  }

  /// `A`
  String get note_text_a {
    return Intl.message(
      'A',
      name: 'note_text_a',
      desc: '',
      args: [],
    );
  }

  /// `A#`
  String get note_text_a_sharp {
    return Intl.message(
      'A#',
      name: 'note_text_a_sharp',
      desc: '',
      args: [],
    );
  }

  /// `B`
  String get note_text_b {
    return Intl.message(
      'B',
      name: 'note_text_b',
      desc: '',
      args: [],
    );
  }

  /// `Turkish`
  String get settings_language_turkish {
    return Intl.message(
      'Turkish',
      name: 'settings_language_turkish',
      desc: '',
      args: [],
    );
  }

  /// `No tags yet`
  String get tagsEmpty {
    return Intl.message(
      'No tags yet',
      name: 'tagsEmpty',
      desc: '',
      args: [],
    );
  }

  /// `New tag`
  String get newTag {
    return Intl.message(
      'New tag',
      name: 'newTag',
      desc: '',
      args: [],
    );
  }

  /// `Tags`
  String get tagsTitle {
    return Intl.message(
      'Tags',
      name: 'tagsTitle',
      desc: '',
      args: [],
    );
  }

  /// `Introducing the Swiss army knife of musicians`
  String get onboarding_swiss_army_knife {
    return Intl.message(
      'Introducing the Swiss army knife of musicians',
      name: 'onboarding_swiss_army_knife',
      desc: '',
      args: [],
    );
  }

  /// `Practice Suite!`
  String get practice_suite_intro_name {
    return Intl.message(
      'Practice Suite!',
      name: 'practice_suite_intro_name',
      desc: '',
      args: [],
    );
  }

  /// `Practice Suite is the ultimate music practice tool for musicians of all levels.With its comprehensive features, you can stay on beat, in tune, and track your progress in one convenient app.`
  String get onboarding_app_intro {
    return Intl.message(
      'Practice Suite is the ultimate music practice tool for musicians of all levels.With its comprehensive features, you can stay on beat, in tune, and track your progress in one convenient app.',
      name: 'onboarding_app_intro',
      desc: '',
      args: [],
    );
  }

  /// `Practice Log`
  String get onboarding_log_title {
    return Intl.message(
      'Practice Log',
      name: 'onboarding_log_title',
      desc: '',
      args: [],
    );
  }

  /// `With the built-in Log feature, you can keep track of your practice sessions, set goals, and monitor your progress over time.`
  String get onboarding_log_desc {
    return Intl.message(
      'With the built-in Log feature, you can keep track of your practice sessions, set goals, and monitor your progress over time.',
      name: 'onboarding_log_desc',
      desc: '',
      args: [],
    );
  }

  /// `The Metronome feature allows you to set your desired tempo and time signature, ensuring that you stay on beat and in time while practicing.`
  String get onboarding_metronome_desc {
    return Intl.message(
      'The Metronome feature allows you to set your desired tempo and time signature, ensuring that you stay on beat and in time while practicing.',
      name: 'onboarding_metronome_desc',
      desc: '',
      args: [],
    );
  }

  /// `The Tuner feature uses your device's microphone to accurately tune your instrument, helping you to play in perfect pitch.`
  String get onboarding_tuner_desc {
    return Intl.message(
      'The Tuner feature uses your device\'s microphone to accurately tune your instrument, helping you to play in perfect pitch.',
      name: 'onboarding_tuner_desc',
      desc: '',
      args: [],
    );
  }

  /// `The Recorder feature allows you to record your practice sessions and play them back, so you can listen to your progress and identify areas for improvement.`
  String get onboarding_recorder_desc {
    return Intl.message(
      'The Recorder feature allows you to record your practice sessions and play them back, so you can listen to your progress and identify areas for improvement.',
      name: 'onboarding_recorder_desc',
      desc: '',
      args: [],
    );
  }

  /// `The app also provides detailed statistics, so you can see how much time you've spent practicing and how you've improved over time.`
  String get onboarding_stats_desc {
    return Intl.message(
      'The app also provides detailed statistics, so you can see how much time you\'ve spent practicing and how you\'ve improved over time.',
      name: 'onboarding_stats_desc',
      desc: '',
      args: [],
    );
  }

  /// `Skip`
  String get onboarding_skip {
    return Intl.message(
      'Skip',
      name: 'onboarding_skip',
      desc: '',
      args: [],
    );
  }

  /// `Filter`
  String get practice_log_filter_title {
    return Intl.message(
      'Filter',
      name: 'practice_log_filter_title',
      desc: '',
      args: [],
    );
  }

  /// `Tag can't be empty`
  String get practice_log_error_empty_tag {
    return Intl.message(
      'Tag can\'t be empty',
      name: 'practice_log_error_empty_tag',
      desc: '',
      args: [],
    );
  }

  /// `Microphone permission is denied!`
  String get error_message_microphone_denied {
    return Intl.message(
      'Microphone permission is denied!',
      name: 'error_message_microphone_denied',
      desc: '',
      args: [],
    );
  }

  /// `E-mail`
  String get settings_title_email {
    return Intl.message(
      'E-mail',
      name: 'settings_title_email',
      desc: '',
      args: [],
    );
  }

  /// `Matrix room`
  String get settings_title_matrix_room {
    return Intl.message(
      'Matrix room',
      name: 'settings_title_matrix_room',
      desc: '',
      args: [],
    );
  }

  /// `Sound`
  String get metronome_sound_name {
    return Intl.message(
      'Sound',
      name: 'metronome_sound_name',
      desc: '',
      args: [],
    );
  }

  /// `Issues`
  String get settings_title_issues {
    return Intl.message(
      'Issues',
      name: 'settings_title_issues',
      desc: '',
      args: [],
    );
  }

  /// `Source`
  String get settings_title_source {
    return Intl.message(
      'Source',
      name: 'settings_title_source',
      desc: '',
      args: [],
    );
  }

  /// `Follow on `
  String get settings_follow_on {
    return Intl.message(
      'Follow on ',
      name: 'settings_follow_on',
      desc: '',
      args: [],
    );
  }

  /// `Built with 💙 by `
  String get settings_built_with_love {
    return Intl.message(
      'Built with 💙 by ',
      name: 'settings_built_with_love',
      desc: '',
      args: [],
    );
  }

  /// `Tap to set tempo`
  String get metronome_tap_to_set_tempo {
    return Intl.message(
      'Tap to set tempo',
      name: 'metronome_tap_to_set_tempo',
      desc: '',
      args: [],
    );
  }

  /// `Please add or select at least one tag`
  String get practice_log_error_no_tag_selected {
    return Intl.message(
      'Please add or select at least one tag',
      name: 'practice_log_error_no_tag_selected',
      desc: '',
      args: [],
    );
  }

  /// `Licences`
  String get settings_btn_licences {
    return Intl.message(
      'Licences',
      name: 'settings_btn_licences',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      Locale.fromSubtags(languageCode: 'de', countryCode: 'DE'),
      Locale.fromSubtags(languageCode: 'tr', countryCode: 'TR'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
