import 'package:get_it/get_it.dart';
import 'package:practice_suite/data/api/sound_api.dart';
import 'package:practice_suite/generated/l10n.dart';
import 'package:practice_suite/logic/blocs/app_theme/app_theme_bloc.dart';
import 'package:practice_suite/logic/blocs/language/language_bloc.dart';
import 'package:practice_suite/logic/blocs/metronome/metronome_bloc.dart';
import 'package:practice_suite/logic/blocs/practice_log/practice_log_bloc.dart';
import 'package:practice_suite/logic/blocs/recording_log/recording_log_bloc.dart';
import 'package:practice_suite/logic/blocs/statistics/statistics_bloc.dart';
import 'package:practice_suite/logic/blocs/tuner/tuner_bloc.dart';
import 'package:practice_suite/services/metronome_service.dart';
import 'package:practice_suite/services/tuner/tuner_service.dart';

GetIt getIt = GetIt.instance;

void setupLocatorService() {
  getIt.registerLazySingleton(() => S());
  getIt.registerLazySingleton(() => PracticeLogBloc());
  getIt.registerLazySingleton(() => LanguageBloc());
  getIt.registerLazySingleton(() => MetronomeService());
  getIt.registerLazySingleton(() => MetronomeBloc());
  getIt.registerLazySingleton(() => TunerService());
  getIt.registerLazySingleton(() => TunerBloc());
  getIt.registerLazySingleton(() => AppThemeBloc());
  getIt.registerLazySingleton(() => SoundApi());
  getIt.registerLazySingleton(() => RecordingLogBloc());
  getIt.registerLazySingleton(() => StatisticsBloc());
}
