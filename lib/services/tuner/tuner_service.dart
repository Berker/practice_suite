import 'dart:typed_data';

import 'package:flutter_audio_capture/flutter_audio_capture.dart';
import 'package:flutter_sound/public/flutter_sound_recorder.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pitch_detector_dart/pitch_detector.dart';
import 'package:practice_suite/services/tuner/pitch_handler.dart';

import '../../generated/l10n.dart';
import '../../logic/blocs/recording_log/recording_log_bloc.dart';
import '../../logic/blocs/tuner/tuner_bloc.dart';
import '../locator_service.dart';

class TunerService {
  FlutterAudioCapture? _audioRecorder;
  double? audioSampleRate;
  PitchDetector? pitchDetectorDart;
  PitchHandler? pitchHandler;
  bool isTunerInit = false;
  bool isTunerOn = false;
  late double pitch;

  Future init() async {
    PermissionStatus status = await Permission.microphone.status;

    if (!status.isGranted) {
      final microphonePermissionStatus = await Permission.microphone.request();
      if (microphonePermissionStatus != PermissionStatus.granted) {
        getIt<RecordingLogBloc>().add(RecordingLogFailure(
            errorMessage: getIt<S>().recording_log_check_permissions));
        throw RecordingPermissionException(getIt<S>().error_message_microphone_denied);
      }
    }
    pitch = getIt<TunerBloc>().pitch;
    _audioRecorder = FlutterAudioCapture();
    await _audioRecorder!.init();
    audioSampleRate = 44100;
    pitchHandler = PitchHandler(newPitch: pitch);
    pitchDetectorDart = PitchDetector(audioSampleRate: audioSampleRate!, bufferSize: 2000);
    isTunerInit = true;
  }

  Future dispose() async {
    if (isTunerInit) {
      _audioRecorder = null;
      pitchDetectorDart = null;
      pitchHandler = null;
      isTunerInit = false;
    }
  }

  void changePitch(double newPitch) {
    dispose();
    pitch = newPitch;
    init();
  }

  Future<void> _startCapture() async {
    await _audioRecorder?.start(listener, onError,
        sampleRate: 44100, bufferSize: 3000);
    isTunerOn = true;
  }

  Future<void> _stopCapture() async {
    await _audioRecorder?.stop();
    isTunerOn = false;
  }

  Future<void> toggleTuner() async {
    if (isTunerOn) {
      _stopCapture();
    } else {
      _startCapture();
    }
  }

  void listener(dynamic obj) async {
    //Gets the audio sample
    var buffer = Float64List.fromList(obj.cast<double>());
    final List<double> audioSample = buffer.toList();

    //Uses pitch_detector_dart library to detect a pitch from the audio sample
    final result = await pitchDetectorDart?.getPitchFromFloatBuffer(audioSample);

    //If there is a pitch - evaluate it
    if (result != null) {

       if (result.pitched) {
         final handledPitchResult = pitchHandler?.handlePitch(result.pitch);
         //Updates the state with the result
         getIt<TunerBloc>().add(UpdateNoteTunerEvent(
             note: handledPitchResult!.note,
             pitchStatus: handledPitchResult.tuningStatus));
       }

    }
  }

  void onError(Object e) {
    print(e);
  }
}
