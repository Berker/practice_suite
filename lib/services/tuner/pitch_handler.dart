import 'dart:math';

import 'package:practice_suite/services/tuner/pitch_result.dart';
import 'package:practice_suite/services/tuner/tuning_status.dart';

import '../../generated/l10n.dart';
import '../locator_service.dart';

class PitchHandler {
  double newPitch;

  PitchHandler({required this.newPitch});

  final dynamic _minimumPitch = 13;
  final dynamic _maximumPitch = 8000;
  final dynamic _noteStrings = [
    getIt<S>().note_text_c,
    getIt<S>().note_text_c_sharp,
    getIt<S>().note_text_d,
    getIt<S>().note_text_d_sharp,
    getIt<S>().note_text_e,
    getIt<S>().note_text_f,
    getIt<S>().note_text_f_sharp,
    getIt<S>().note_text_g,
    getIt<S>().note_text_g_sharp,
    getIt<S>().note_text_a,
    getIt<S>().note_text_a_sharp,
    getIt<S>().note_text_b
  ];

  PitchResult handlePitch(double pitch) {
    if (_isPitchInRange(pitch)) {
      final noteLiteral = _noteFromPitch(pitch);
      final expectedFrequency = _frequencyFromNoteNumber(_midiFromPitch(pitch));
      final diff = _diffFromTargetedNote(pitch);
      final tuningStatus = _getTuningStatus(diff);
      final diffCents =
      _diffInCents(expectedFrequency, expectedFrequency - diff);

      return PitchResult(
          noteLiteral, tuningStatus, expectedFrequency, diff, diffCents);
    }

    return PitchResult("", TuningStatus.undefined, 0.00, 0.00, 0.00);
  }

  bool _isPitchInRange(double pitch) {
    return pitch > _minimumPitch && pitch < _maximumPitch;
  }

  String _noteFromPitch(double frequency) {
    final noteNum = 12.0 * (log((frequency / newPitch)) / log(2.0));
    return _noteStrings[
    ((noteNum.roundToDouble() + 69.0).toInt() % 12.0).toInt()];
  }

  double _diffFromTargetedNote(double pitch) {
    final targetPitch = _frequencyFromNoteNumber(_midiFromPitch(pitch));
    return targetPitch - pitch;
  }

  double _diffInCents(double expectedFrequency, double frequency) {
    return 1200.0 * log(expectedFrequency / frequency);
  }

  TuningStatus _getTuningStatus(double diff) {
    // TUNED
    if (diff >= -1.0 && diff <= 1.0) {
      return TuningStatus.tuned;
    }  else if (diff >= -3.0 && diff < -1.0) {
      return TuningStatus.tunedPlus;
    } else if (diff > 1.0 && diff <= 3.0) {
      return TuningStatus.tunedMinus;
    }
    // HIGH
    else if (diff >= -5.0 && diff < -3.0) {
      return TuningStatus.highMinus;
    } else if (diff >= -7.0 && diff < -5.0) {
      return TuningStatus.high;
    }  else if (diff >= -9.0 && diff < -7.0) {
      return TuningStatus.highPlus;
    }
    // TOO HIGH
    else if (diff >= -11.0 && diff < -9.0) {
      return TuningStatus.tooHighMinus;
    } else if (diff >= -13.0 && diff < -11.0) {
      return TuningStatus.tooHigh;
    }  else if (diff >= double.negativeInfinity && diff < -13.0) {
      return TuningStatus.tooHighPlus;
    }
    // LOW
    else if (diff > 3.0 && diff <= 5.0) {
      return TuningStatus.lowMinus;
    } else if (diff > 5.0 && diff <= 7.0) {
      return TuningStatus.low;
    } else if (diff > 7.0 && diff <= 9.0) {
      return TuningStatus.lowPlus;
    }
    // TOO LOW
    else if (diff > 9.0 && diff <= 11.0) {
      return TuningStatus.tooLowMinus;
    } else if (diff > 11.0 && diff <= 13.0) {
      return TuningStatus.tooLow;
    } else if (diff > 13.0 && diff <= double.infinity) {
      return TuningStatus.tooLowPlus;
    }
    // UNDEFINED
    else {
      return TuningStatus.undefined;
    }
  }

  // Original values
  // TuningStatus _getTuningStatus(double diff) {
  //   if (diff >= -0.3 && diff <= 0.3) {
  //     return TuningStatus.tuned;
  //   } else if (diff >= -1.0 && diff <= 0.0) {
  //     return TuningStatus.toohigh;
  //   } else if (diff > 0.0 && diff <= 1.0) {
  //     return TuningStatus.toolow;
  //   } else if (diff >= double.negativeInfinity && diff <= -1.0) {
  //     return TuningStatus.waytoohigh;
  //   } else {
  //     return TuningStatus.waytoolow;
  //   }
  // }

  int _midiFromPitch(double frequency) {
    final noteNum = 12.0 * (log((frequency / newPitch)) / log(2.0));
    return (noteNum.roundToDouble() + 69.0).toInt();
  }

  double _frequencyFromNoteNumber(int note) {
    final exp = (note - 69.0).toDouble() / 12.0;
    return (newPitch * pow(2.0, exp)).toDouble();
  }
}