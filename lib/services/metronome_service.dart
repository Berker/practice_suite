import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_sound/flutter_sound.dart';

List<String> metronomeSounds = [
  "sound-1.wav",
  "sound-2.wav",
  "sound-3.wav",
  "sound-4.wav",
  "sound-5.wav",
  "sound-6.wav",
  "sound-7.wav"
];

enum Notes {
  fourth(1),
  eighth(2),
  triplet(3);

  const Notes(this.value);

  final int value;
}

class MetronomeService {
  bool isMetronomeOn = false;

  Timer? _timer;
  late int _startTimestamp;
  late int _nextBeat;
  late Uint8List normalSoundByteData;
  late Uint8List accentSoundByteData;
  late FlutterSoundPlayer _audioPlayer;
  String normalSound = "sound-1.wav";
  String accentSound = "sound-1.wav";
  Notes selectedNote = Notes.fourth;

  final _sampleRate = 24000;

  // Will be computed from time signature
  int _accentSoundInterval = 1;

  Future<void> init() async {
    _audioPlayer = FlutterSoundPlayer();
    await _audioPlayer.openPlayer();
    // Load the sound byte data
    normalSoundByteData = await convertSoundToByteData(normalSound);
    accentSoundByteData = await convertSoundToByteData(accentSound);
  }

  Future<void> dispose() async {
    if (isMetronomeOn) {
      await off();
    }
    await _audioPlayer.closePlayer();
  }

  Future<Uint8List> convertSoundToByteData(String soundPath) async {
    return (await rootBundle.load('assets/sounds/$soundPath'))
        .buffer
        .asUint8List();
  }

  Future<void> off() async {
    if (isMetronomeOn) {
      isMetronomeOn = false;
      _timer?.cancel();
      await _audioPlayer.stopPlayer();
    }
  }

  Future<void> on(int tempo, Notes note) async {
    selectedNote = note;
    _accentSoundInterval = selectedNote.value;

    if (!isMetronomeOn) {
      isMetronomeOn = true;
      _startTimestamp = DateTime.now().millisecondsSinceEpoch;
      _nextBeat = 0;

      final intervalLengthSeconds = 60 / tempo;
      final intervalLengthSamples =
          (intervalLengthSeconds * _sampleRate).round();
      // Sound is played at 16 bits per sample, Uint8List uses 8 bit elements,
      // so each sample requires two elements in the Uint8List
      final intervalLengthUint8List = intervalLengthSamples * 2;

      // Generate the byte data in between sounds
      var gapByteData = List.generate(
          intervalLengthUint8List - normalSoundByteData.length, (i) => 0);

      // Transform Uint8List to food for the foodsink
      var food =
          FoodData(Uint8List.fromList(normalSoundByteData + gapByteData));
      var food2 =
          FoodData(Uint8List.fromList(accentSoundByteData + gapByteData));

      // Start the audio player
      await _audioPlayer.startPlayerFromStream(
          codec: Codec.pcm16, numChannels: 1, sampleRate: _sampleRate);
      // Without the delay, there will be crashes on Android
      await Future.delayed(const Duration(milliseconds: 150));
      // Feed the first beat
      _audioPlayer.foodSink?.add(food);

      _timer = Timer.periodic(const Duration(milliseconds: 10), (timer) {
        // Calculate the timestamp (in ms since UNIX epoch) until next beat
        var nextBeatTimestamp =
            _startTimestamp + (60000 / tempo).round() * _nextBeat;
        // If current timestamp is later than next beat timestamp,
        // queue the next beat
        if (DateTime.now().millisecondsSinceEpoch >= nextBeatTimestamp) {
          if (_nextBeat % _accentSoundInterval == _accentSoundInterval - 1) {
            // Queue the accent beat sound
            _audioPlayer.foodSink?.add(food2);
          } else {
            // Queue the normal beat sound
            _audioPlayer.foodSink?.add(food);
          }
          // Increment the beat counter
          _nextBeat++;
        }
      });
    }
  }

  Future<void> changeMetronomeSound(
      String soundName, String accentSoundName, int tempo) async {
    normalSound = soundName;
    accentSound = selectedNote == Notes.fourth ? soundName : accentSoundName;
    normalSoundByteData = await convertSoundToByteData(normalSound);
    accentSoundByteData = await convertSoundToByteData(accentSound);
    if (isMetronomeOn) {
      await off();
      await on(tempo, selectedNote);
    }
  }
}
